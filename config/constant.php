<?php
/**
 * Created by PhpStorm.
 * User: JoséLuis
 * Date: 16/02/2020
 * Time: 12:37
 */

return[
    'alert' => [
        'tittle' => [
            'change_estate' => '¡Cambiar estado!',
            'active_state' => '¡Activar registro!',
            'inactive_estate' => '¡Inactivar registro!',
            'delete' => '¡Eliminar registro!',
        ],
        'confirm'=> [
            'delete' => '¿Seguro que deseas eliminar este registro?',
            'state' => '¿Seguro que deseas cambiar el estado de este registro?',
            'state_active' => '¿Seguro que deseas activar este registro?',
            'state_inactive' => '¿Seguro que deseas inactivar este registro?',
            'state_active_client' => '¿Seguro que deseas activar este cliente?',
            'state_inactive_client' => '¿Seguro que deseas inactivar este cliente? Si lo inactivas ya no podrás hacer registro de venta a crédito con este cliente.',
            'state_active_user' => '¿Seguro que deseas activar este usuario?',
            'state_inactive_user' => '¿Seguro que deseas inactivar este usuario? Si lo inactivas, el usuario no podrá iniciar sesión en el sistema',
            'state_active_product' => '¿Seguro que deseas activar este producto?',
            'state_inactive_product' => '¿Seguro que deseas inactivar este producto? Si lo inactivas ya no podrás hacer registro de venta a crédito con este producto.',
        ],
        'success' => [
            'update' => 'Registro actualizado con éxito.',
            'delete' => 'Registro eliminado con éxito.',
            'state' => 'El estado fue actualizado con éxito',
        ],
        'error' => [
            'not_found' => 'El registro no existe.',
            'change_state_by_join' => 'El registro no puede ser inactivado ya que se encuentra relacionado a otra información del sistema.',
            'delete_records_all' => 'Debe existir al menos un registro para el correcto funcionamiento del sistema.',
        ],
        'button' => [
            'sure' => 'Si, estoy seguro',
            'cancel' => 'Cancelar',
            'accept' => 'Aceptar',
        ]
    ],

    'rol' => [
        'id' => [
            'admin' => 'ADMIN',
            'ventas' => 'VENTA',
        ],
        'description' => [
            'admin' => 'Administrador',
            'ventas' => 'Ventas',
        ],
    ]
];
