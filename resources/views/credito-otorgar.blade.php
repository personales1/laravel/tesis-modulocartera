@extends('app')
@section('title', 'Ventas a crédito')

<?php
$_GET['page'] = "credito";
$_GET['page-title'] = "Ventas a crédito";
$_GET['page-description'] = "Aquí se registra las ventas a crédito que se le hacen a los clientes.";
?>

@section('content-body')
    @if(count($clientes) == 0)
        <script>
            Swal.fire('¡Error!', "No existen clientes para realizar una venta, por favor proceda a crear o activar un cliente desde la opción Cliente/Catálogos/Cliente", 'error');
        </script>
    @endif

    @if(count($productos) == 0)
        <script>
            Swal.fire('¡Error!', "No existen productos disponibles para realizar una venta, por favor proceda a crear o activar un producto desde la opción Crédito/Catálogos/Productos", 'error');
        </script>
    @endif

    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Datos del Crédito</h5>
            <form class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="numFactura">Factura</label>
                        <input type="text" class="form-control" id="numFactura" placeholder="# Factura" value="" maxlength="16" required>
                        <div class="mensaje-error" id="mensaje-error-factura"></div>
                    </div>
                    <div class="col-md-9 mb-3">
                        <div class="row">
                            <div class="col-md-10">
                                <label for="clienteSeleccionado">* Cliente (Nombre - Identificación)</label> <label class="text-danger" id="clienteAdvertencia" style="visibility: hidden">CLIENTE PRESENTA DEUDAS</label>
                                <input type="text" class="form-control" id="clienteSeleccionado" placeholder="Selecciona cliente" required readonly>
                                <div class="mensaje-error" id="mensaje-error-cliente"></div>
                            </div>
                            <div class="col-md-2" style="margin-left:-25px; margin-top: 13px">
                                <label for=""></label>
                                <button class="form-control btn-shadow btn btn-alternate mt-2 btnModalCliente" type="button" data-toggle="modal" data-target=".bd-cliente-modal-lg">
                                    <i class="fa fa-user"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="creditReasonSelect">Motivo de crédito</label>
                        {{ Form::select('creditReason', $creditReason, null, ['class' => 'form-control', 'id' => 'creditReasonSelect']) }}
                        @error('creditReason')
                        <label class="mensaje-error" id="mensaje-error-creditreason">{{ $message }}</label>
                        <script>
                            $("#creditReasonSelect").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="creditDaySelect">Días de crédito</label>
                        {{ Form::select('creditDay', $creditDay, null, ['class' => 'form-control', 'id' => 'creditDaySelect']) }}
                        @error('creditDay')
                            <label class="mensaje-error" id="mensaje-error-creditday">{{ $message }}</label>
                            <script>
                                $("#creditDaySelect").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label for="observacionVenta">Observación</label>
                        <input type="text" class="form-control" id="observacionVenta" placeholder="Ingrese alguna observación" value="" required>
                        <div class="mensaje-error" id="mensaje-error-observacion"></div>
                    </div>
                </div>

                <div class="form-row mt-4">
                    <div class="col-md-12 mb-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <div class="form-row">
                                        <div class="col-md-11 mb-3">
                                            <h5 class="card-title">Selecciona producto</h5>
                                            <div class="mensaje-error" id="mensaje-error-producto"></div>
                                        </div>
                                        <div class="col-md-1 mb-3">
                                            <button class="btn btn-success btnModalProducto" type="button" data-toggle="modal" data-target=".bd-producto-modal-lg">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <table class="mb-0 table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Cod.</th>
                                                <th>Nombre</th>
                                                <th>Precio</th>
                                                <th>Cant.</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="rowProductoSeleccionado">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary btnGeneraVenta" type="button">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var ventaId = "";

            var clienteId = "";
            var clienteNombre;
            var clienteIdentificacion;

            var productoId;
            var productoNombre;
            var productoPrecio;
            var listProductosSeleccionado = [];

            let regexpSoloNumero = /^[0-9]+$/;

            $("#input-busqueda-prod").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTableProd tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $("#input-busqueda-client").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTableClient tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $(".rowCliente").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
                //$("#clienteAdvertencia").css({'visibility':  'hidden'});

                clienteId = $(this).data("id");
                clienteNombre = $(this).find(".nombresCliente").html();
                clienteIdentificacion = $(this).find(".identificacionCliente").html();

                $.ajax({
                    url: '/ventas/pago/pendiente/cliente/' + clienteId,
                    type: 'GET',
                    success: function(data) {
                        if(data == 1){
                            Swal.fire({
                                title: '¡Atención!',
                                text: 'El cliente ' + clienteNombre + ' tiene facturas pendiente de pago. ¿Desea seguir con el proceso de venta?',
                                icon: 'warning',
                                confirmButtonText: 'SI',
                                showCancelButton: true,
                                cancelButtonColor: '#d33',
                                cancelButtonText: 'NO',
                            }).then((result) => {
                                if (result.value){
                                    $("#clienteSeleccionado").val(clienteNombre + " - " + clienteIdentificacion);
                                    $("#clienteAdvertencia").css({'visibility':  'visible'});

                                    $("#input-busqueda-client").val("");
                                    $("#myTableClient tr").filter(function() {
                                        $(this).toggle($(this).text().toLowerCase().indexOf("") > -1)
                                    });
                                }
                                else{
                                    $("#clienteSeleccionado").val("");
                                    $("#clienteAdvertencia").css({'visibility':  'hidden'});
                                }
                            });
                        }
                        else{
                            $("#clienteSeleccionado").val(clienteNombre + " - " + clienteIdentificacion);
                            $("#clienteAdvertencia").css({'visibility':  'hidden'});
                        }

                        //$("#mensaje-error-cliente").html("");
                        //$("#clienteSeleccionado").removeClass("form-control-error");
                        $('#lightbox-loader').css({"visibility":"hidden"});
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        alert("Hubo un error.");
                        $('#lightbox-loader').css({"visibility":"hidden"});
                    }
                });
            });


            $(".rowProducto").on("click", function (e) {
                productoId = $(this).data("id");
                productoNombre = $(this).find(".nombreProducto").html();
                productoPrecio = $(this).find(".precioProducto").html();

                if($.inArray(productoId, listProductosSeleccionado) > -1){ //Devuelve -1 si no existe el id producto en el array listProductosSeleccionado
                    alert("El producto " + productoNombre + " ya ha sido seleccionado");
                }
                else{
                    listProductosSeleccionado.push(productoId);
                    var insertRowProducto =
                        '<tr>'+
                        '<th scope="row">' + productoId + '</th>'+
                        '<td>' + productoNombre + '</td>'+
                        '<td>' + productoPrecio + '</td>'+
                        '<td><input type="text" class="form-control cantidad-producto" id="" placeholder="" value="1" size="1" data-id="' + productoId + '" maxlength="5" required></td>'+
                        '<td><button class="btn-shadow btn btn-outline-link btn-danger btnQuitarProducto" type="button" data-placement="bottom" data-id="' + productoId + '"><i class="fa fa-times"></i></button></td>'+
                        '</tr>';

                    $('#rowProductoSeleccionado').append(insertRowProducto);
                    $("#mensaje-error-producto").html("");

                    $("#input-busqueda-prod").val("");
                    $("#myTableProd tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf("") > -1)
                    });
                }
            });


            $('#rowProductoSeleccionado').on("click", ".btnQuitarProducto", function (e) {
                let indice = $.inArray($(this).data("id"), listProductosSeleccionado); //Obtener el índice o posición donde se encuentra el id del producto seleccionado en el array.
                listProductosSeleccionado.splice(indice, 1); //Eliminar un elemento del array, el 1 indica la cantidad de elementos a eliminar.
                $(this).closest("tr").remove();
            });

            $('#rowProductoSeleccionado').on("input", ".cantidad-producto", function (e) {
                if(! regexpSoloNumero.test($(this).val())){
                    let cadena="";
                    let letter = "";

                    for(i=0; i < $(this).val().trim().length; i++){
                        letter = $(this).val().charAt(i);

                        if(regexpSoloNumero.test(letter)){
                            cadena += letter;
                        }
                    }

                    $(this).val(cadena);
                }
            });

            $('#rowProductoSeleccionado').on("blur", ".cantidad-producto", function (e) {
                if($(this).val().length === 0){
                    $(this).val(1)
                }
            });

            $("#numFactura").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $("#mensaje-error-factura").html("");
                        $(this).removeClass("form-control-error");
                    }
                },
                input: function () {
                    if(! regexpSoloNumero.test($(this).val())){
                        var cadena="";
                        var letter = "";

                        for(i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(regexpSoloNumero.test(letter)){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                }
            });


            $("#observacionVenta").on("keyup", function (e) {
                let cadena = $("#observacionVenta").val();

                if(cadena.length > 0){
                    $("#mensaje-error-observacion").html("");
                    $("#observacionVenta").removeClass("form-control-error");
                }
            });

            function validaForm() {
                let retorna = true;

                if($("#numFactura").val().trim() == ""){
                    $("#mensaje-error-factura").html("Ingrese un número de factura");
                    $("#numFactura").addClass("form-control-error");
                    retorna = false;
                }else{
                    $("#mensaje-error-factura").html("");
                    $("#numFactura").removeClass("form-control-error");
                }

                if(clienteId == ""){
                    $("#mensaje-error-cliente").html("Seleccione un cliente");
                    $("#clienteSeleccionado").addClass("form-control-error");
                    retorna = false;
                }else{
                    $("#mensaje-error-cliente").html("");
                    $("#clienteSeleccionado").removeClass("form-control-error");
                }

                if($("#observacionVenta").val().trim() == ""){
                    $("#mensaje-error-observacion").html("Ingresa una observación");
                    $("#observacionVenta").addClass("form-control-error");
                    retorna = false;
                }else{
                    $("#mensaje-error-observacion").html("");
                    $("#observacionVenta").removeClass("form-control-error");
                }

                if(listProductosSeleccionado.length == 0){
                    $("#mensaje-error-producto").html("Seleccione un producto");
                    retorna = false;
                }else{
                    $("#mensaje-error-producto").html("");
                }

                return retorna;
            }


            $('.btnGeneraVenta').on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});

                if(validaForm()){
                    $.ajax({
                        url: '/ventas/header',
                        type: 'POST',
                        data: {
                            _token: $("input[name='_token']").val(),
                            numFactura: $("#numFactura").val(),
                            clientId: clienteId,
                            creditReasonId: $("#creditReasonSelect").val(),
                            creditDaysId: $("#creditDaySelect").val(),
                            observacion: $("#observacionVenta").val()
                        },
                        success: function(data) {
                            ventaId = data;
                            enviaDetalles(0)
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                            alert("Hubo un error.");
                            $('#lightbox-loader').css({"visibility":"hidden"});
                        }
                    });
                }else{
                    $('#lightbox-loader').css({"visibility":"hidden"});
                }
            });

            function enviaDetalles(position){
                if(position < listProductosSeleccionado.length){
                    $.ajax({
                        url: '/ventas/detail',
                        type: 'POST',
                        data: {
                            _token: $("input[name='_token']").val(),
                            productId: listProductosSeleccionado[position],
                            cantidad: $('input[data-id="' + listProductosSeleccionado[position] + '"]').val(),
                            subtotal: "0",
                            iva: "0",
                            salesHeaderId: ventaId
                        },
                        success: function(data) {
                            position = position + 1;
                            enviaDetalles(position)
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                            alert("Hubo un error.");
                            $('#lightbox-loader').css({"visibility":"hidden"});
                        }
                    });
                }
                else{
                    $.ajax({
                        url: '/ventas/pago',
                        type: 'POST',
                        data: {
                            _token: $("input[name='_token']").val(),
                            salesHeaderId: ventaId,
                            creditDaysId: $("#creditDaySelect").val()
                        },
                        success: function(data) {
                            $('#lightbox-loader').css({"visibility":"hidden"});
                            $( ".btnGeneraVenta" ).prop( "disabled", true );
                            $( ".btnModalCliente" ).prop( "disabled", true );
                            $( ".btnModalProducto" ).prop( "disabled", true );
                            $( "#numFactura" ).prop( "disabled", true );
                            $( "#observacionVenta" ).prop( "disabled", true );
                            $( "#creditReasonSelect" ).prop( "disabled", true );
                            $( "#creditDaySelect" ).prop( "disabled", true );

                            showToastCustom("Crédito", "La venta de crédito fue realizada con éxito.");
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                            alert("Hubo un error.");
                            $('#lightbox-loader').css({"visibility":"hidden"});
                        }
                    });
                }
            }
        })
    </script>
@endsection

@include('include.modal-cliente')
@include('include.modal-producto')
