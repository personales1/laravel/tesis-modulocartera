@extends('app')
@section('title', 'Sector')

<?php
    $_GET['page'] = "ubicacion";
    $_GET['page-title'] = "Sector";
    if(isset($sectores)){
        $_GET['page-description'] = "Actualizar sector ".$sectores->nombre;

    }else{
        $_GET['page-description'] = "Registro de nuevos sectores.";
    }
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de sector</h5>
            <form id="myform" method="POST" action="{{ route('sector.create', ['id' => $sectores->id ?? '']) }}"  class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-5 mb-3">
                        <label for="sectorName">Nombre</label>
                        <input name="name" id="sectorName" type="text" class="form-control" placeholder="Nombre" value="{{ $sectores->nombre ?? old('name') }}" required>
                        @error('name')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#sectorName").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#sectorName").on("keyup", function (e) {
                let cadena = $("#sectorName").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#sectorName").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
