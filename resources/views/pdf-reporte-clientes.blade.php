@php
    use Carbon\Carbon;
    $fechaCurrent = Carbon::now()->startOfDay();
@endphp

<div class="card-body table-responsive">
    <h2 style="text-transform: uppercase; color: black; font-weight: bold;">Reporte de clientes</h2>
    <h3>Con fecha de {{ $fechaCurrent->format('d-m-Y') }}</h3>

    <table style="width: 100%; border: 1px solid #e9ecef; border-collapse: collapse;">
        <thead style="text-align: center;">
        <tr>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Cod.</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Nombres</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">C.I.</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Correo</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Teléfono</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Celular</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Dirección</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Referencia</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Crédito</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Estado</th>
        </tr>
        </thead>
        <tbody>
            @foreach($clientes as $cliente)
                @if($cliente->is_activo)
                    @php
                        $colorFondo = "#FFFFFF";
                        $colorTexto = "#000000";
                        $estado = "Activo";
                    @endphp
                @else
                    @php
                        $colorFondo = "#B81F44";
                        $colorTexto = "#FFFFFF";
                        $estado = "Inactivo";
                    @endphp
                @endif
                <tr>
                    <th style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $cliente->id }}</th>
                    <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $cliente->nombres }} {{ $cliente->apellidos }}</td>
                    <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $cliente->identificacion }}</td>
                    <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $cliente->email }}</td>
                    <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $cliente->telefono }}</td>
                    <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $cliente->celular }}</td>
                    <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $cliente->direccion }}</td>
                    <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $cliente->direccion_referencia }}</td>
                    <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 10px;">
                        @if($cliente->is_credito_aprobado)
                            <div>Aprobado</div>
                        @else
                            <div>No aprobado</div>
                        @endif
                    </td>
                    <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 10px;">{{ $estado }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
