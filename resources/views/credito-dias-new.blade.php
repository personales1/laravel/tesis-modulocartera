@extends('app')
@section('title', 'Días de crédito')

<?php
    $_GET['page'] = "credito";
    $_GET['page-title'] = "Días de crédito";
    if(isset($credits)){
        $_GET['page-description'] = "Actualizar días de crédito ".$credits->descripcion;

    }else{
        $_GET['page-description'] = "Registro de días de crédito, indicando el tiempo de crédito que tendrá los clientes.";
    }
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de días de crédito</h5>
            <form id="myform" method="POST" action="{{ route('credito.dias.create', ['id' => $credits->id ?? '']) }}" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-5 mb-3">
                        <label for="creditoDescription">Descripción</label>
                        <input name="description" id="creditoDescription" type="text" class="form-control" placeholder="Nombre" value="{{ $credits->descripcion ?? old('description') }}" required>
                        @error('description')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#creditoDescription").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>

                    <div class="col-md-3 mb-3">
                        <?php
                            if(isset($credits)){
                                $periodo = $credits->periodo;
                            }else{
                                $periodo = "";
                            }
                        ?>
                        <label for="creditoPeriodo" class="">Tiempo</label>
                        <select name="periodo" id="creditoPeriodo" class="form-control">
                            @if($periodo == 'días')
                                <option value="días" selected>días</option>
                            @else
                                <option value="días">días</option>
                            @endif

                            @if($periodo == 'semanas')
                                <option value="semanas" selected>semanas</option>
                            @else
                                <option value="semanas">semanas</option>
                            @endif

                            @if($periodo == 'meses')
                                <option value="meses" selected>meses</option>
                            @else
                                <option value="meses">meses</option>
                            @endif

                            @if($periodo == 'año')
                                <option value="año" selected>año</option>
                            @else
                                <option value="año">año</option>
                            @endif
                        </select>
                    </div>
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#creditoDescription").on("keyup", function (e) {
                let cadena = $("#creditoDescription").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#creditoDescription").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
