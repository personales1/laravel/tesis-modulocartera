@extends('app')
@section('title', 'Cantón')

<?php
    $_GET['page'] = "ubicacion";
    $_GET['page-title'] = "Cantón";
    if(isset($cantones)){
        $_GET['page-description'] = "Actualizar cantón ". $cantones->nombre;

    }else{
        $_GET['page-description'] = "Registro de nuevos cantones.";
    }
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de cantones</h5>
            <form id="myform" method="POST" action="{{ route('canton.create', ['id' => $cantones->id ?? '']) }}" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-5 mb-3">
                        <label for="cantonName">Nombre</label>
                        <input name="name" id="cantonName" type="text" class="form-control" placeholder="Nombre" value="{{ $cantones->nombre ?? old('name') }}" required>
                        @error('name')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#cantonName").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="cantonProvinceSelect">Provincia</label>
                        {{ Form::select('listProvince', $provincia, $cantones->province_id ?? null, ['class' => 'form-control', 'id' => 'cantonProvinceSelect']) }}
                        @error('listProvince')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#cantonProvinceSelect").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#cantonName").on("keyup", function (e) {
                let cadena = $("#cantonName").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#cantonName").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
