@extends('app')
@section('title', 'Reporte de ventas')

<?php
$_GET['page'] = "reporte";
$_GET['page-title'] = "Reporte de ventas";
$_GET['page-description'] = "Reporte que muestra las facturas de ventas que se han realizado durante un período de tiempo.";
?>

@section('content-body')
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <h5 class="card-title">Listado de reportes de ventas</h5>
                    <form class="needs-validation" novalidate>
                        @csrf
                        @include('include.botonera-mas-buscador')

                        <table class="mb-0 table table-bordered my-table">
                            <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Factura</th>
                                    <th>Motivo</th>
                                    <th>Observación</th>
                                    <th>Total</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                            @foreach($ventas as $venta)
                                <tr>
                                    <td>{{ $venta->client->nombres }} {{ $venta->client->apellidos }}</td>
                                    <td style="background: #a5d1f2; color: #000000;">{{ $venta->num_factura }}</td>
                                    <td>{{ $venta->creditReason->descripcion }}</td>
                                    <td>{{ $venta->observacion }}</td>
                                    <td style="background: #a0331e; color: #ffffff;"><b>${{ $venta->salesDetail->sum('total') }}</b></td>
                                    <td style="background: #cecece; color: #000000;">{{ $venta->created_at }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#input-busqueda").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $("#btnExportExcel").on("click", function (e) {
                $(this).prop("href", "{{route('export.excel.reporte.venta')}}");
            });

            $("#btnExportPdf").on("click", function (e) {
                $(this).prop("href", "{{route('export.pdf.reporte.venta')}}");
            });
        })
    </script>
@endsection
