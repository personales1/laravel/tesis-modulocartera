@php
    use Carbon\Carbon;
    $fechaCurrent = Carbon::now()->startOfDay();
@endphp

<div class="card-body table-responsive">
    <h2 style="text-transform: uppercase; color: black; font-weight: bold;">Reporte de ventas</h2>
    <h3>Con fecha de {{ $fechaCurrent->format('d-m-Y') }}</h3>

    <table style="width: 100%; border: 1px solid #e9ecef; border-collapse: collapse;">
        <thead style="text-align: center;">
        <tr>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Cliente</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Factura</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Motivo</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Observación</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Total</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Fecha</th>
        </tr>
        </thead>
        <tbody>
        @foreach($ventas as $venta)
            <tr>
                <th style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $venta->client->nombres }} {{ $venta->client->apellidos }}</th>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px; background: #a5d1f2; color: #000000;">{{ $venta->num_factura }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $venta->creditReason->descripcion }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $venta->observacion }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px; background: #a0331e; color: #ffffff;"><b>${{ $venta->salesDetail->sum('total') }}</b></td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px; background: #cecece; color: #000000;">{{ $venta->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
