@extends('app')
@section('title', 'Producto')

<?php
    $_GET['page'] = "credito";
    $_GET['page-title'] = "Producto";
    if(isset($productos)){
        $_GET['page-description'] = "Actualizar producto ". $productos->nombre;

    }else{
        $_GET['page-description'] = "Registro de nuevos productos.";
    }
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de productos</h5>
            <form id="myform" method="POST" action="{{ route('producto.create', ['id' => $productos->id ?? '']) }}" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-5 mb-3">
                        <label for="productName">Nombre</label>
                        <input name="name" id="productName" type="text" class="form-control" placeholder="Nombre" value="{{ $productos->nombre ?? old('name') }}" required>
                        @error('name')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#productName").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                    <div class="col-md-7 mb-3">
                        <label for="productDescription">Descripción</label>
                        <input name="description" id="productDescription" type="text" class="form-control" placeholder="Describe el producto" value="{{ $productos->descripcion ?? old('description') }}" required>
                        @error('description')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#productDescription").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="productPrice">Precio ($)</label>
                        <input name="price" id="productPrice" type="text" class="form-control" placeholder="Ingresa el precio en $" value="{{ $productos->precio ?? old('price') }}" required>
                        @error('price')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#productPrice").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#productName").on("keyup", function (e) {
                let cadena = $("#productName").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#productName").removeClass("form-control-error");
                }
            });

            $("#productDescription").on("keyup", function (e) {
                let cadena = $("#productDescription").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#productDescription").removeClass("form-control-error");
                }
            });

            $("#productPrice").on("keyup", function (e) {
                let cadena = $("#productPrice").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#productPrice").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
