@extends('app')
@section('title', 'Días de crédito')

<?php
$_GET['page'] = "credito";
$_GET['page-title'] = "Días de crédito";
$_GET['page-description'] = "Listado de días de crédito, indicando el tiempo de crédito que tendrá los clientes.";
?>

@section('content-body')
    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
        <li class="nav-item">
            <a class="nav-link active" href="{{ Request::root() }}/credito/dias/nuevo">
                <span>Nuevo</span>
            </a>
        </li>
    </ul>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <h5 class="card-title">Listado de días de crédito</h5>
                    <form class="needs-validation" novalidate>
                        @csrf
                        @include('include.botonera-mas-buscador')

                        <table class="mb-0 table table-bordered my-table">
                            <thead>
                            <tr>
                                <th>Descripción</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach($credits as $credit)
                                    <tr>
                                        <td>{{ $credit->descripcion }} {{ $credit->periodo }}</td>
                                        <td>
                                            <button class="btn-shadow btn btn-primary btnActualizar" type="button" data-toggle="tooltip" data-placement="bottom" data-id="{{ $credit->id }}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn-shadow btn btn-danger btnEliminar" type="button" data-toggle="tooltip" data-placement="bottom" data-id="{{ $credit->id }}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#input-busqueda").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $("#btnExportExcel").on("click", function (e) {
                $(this).prop("href", "{{route('export.excel.motivo.dias')}}");
            });

            $("#btnExportPdf").on("click", function (e) {
                $(this).prop("href", "{{route('export.pdf.motivo.dias')}}");
            });

            $(".btnActualizar").on("click", function (e) {
                location.href = '/credito/dias/' + $(this).data("id");
            });

            $(".btnEliminar").on("click", function (e) {
                Swal.fire({
                    title: '{{ config('constant.alert.tittle.delete') }}',
                    text: '{{ config('constant.alert.confirm.delete') }}',
                    icon: 'warning',
                    confirmButtonText: '{{ config('constant.alert.button.sure') }}',
                    showCancelButton: true,
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ config('constant.alert.button.cancel') }}',
                }).then((result) => {
                    if (result.value) {
                        $('#lightbox-loader').css({"visibility":"visible"});
                        let delete_id = $(this).data('id');
                        let row = this;

                        $.ajax({
                            url: '/credito/dias/' + delete_id,
                            type: 'DELETE',
                            data: {
                                _token: $("input[name='_token']").val(),
                                id: $(this).data("id")
                            },
                            success: function(data) {
                                let myJsonResponse = $.parseJSON(data); //convierte el response string en json

                                if(myJsonResponse.estado >= 400){
                                    Swal.fire('¡Error!', myJsonResponse.data.mensaje, 'error');
                                }else{
                                    $(row).closest("tr").remove();
                                    Swal.fire('¡Éxito!', myJsonResponse.data.mensaje, 'success');
                                }

                                $('#lightbox-loader').css({"visibility":"hidden"});
                            },
                            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                                console.log(JSON.stringify(jqXHR));
                                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                                alert("Hubo un error.");
                                $('#lightbox-loader').css({"visibility":"hidden"});
                            }
                        });
                    }
                });
            })
        })
    </script>
@endsection
