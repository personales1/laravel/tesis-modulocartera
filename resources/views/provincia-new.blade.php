@extends('app')
@section('title', 'Provincia')

<?php
    $_GET['page'] = "ubicacion";
    $_GET['page-title'] = "Provincia";
    if(isset($provincias)){
        $_GET['page-description'] = "Actualizar provincia ".$provincias->nombre;

    }else{
        $_GET['page-description'] = "Registro de nuevas provincias.";
    }
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de provincias</h5>
            <form id="myform" method="POST" action="{{ route('provincia.create', ['id' => $provincias->id ?? '']) }}" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-5 mb-3">
                        <label for="provinceName">Nombre</label>
                        <input name="name" id="provinceName" type="text" class="form-control" placeholder="Nombre" value="{{ $provincias->nombre ?? old('name') }}" required>
                        @error('name')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#provinceName").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#provinceName").on("keyup", function (e) {
                let cadena = $("#provinceName").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#provinceName").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
