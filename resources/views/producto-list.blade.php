@extends('app')
@section('title', 'Producto')

<?php
$_GET['page'] = "credito";
$_GET['page-title'] = "Producto";
$_GET['page-description'] = "Listado de productos.";
?>

@section('content-body')
    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
        <li class="nav-item">
            <a class="nav-link active" href="{{ Request::root() }}/producto/nuevo">
                <span>Nuevo</span>
            </a>
        </li>
    </ul>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <h5 class="card-title">Listado de productos</h5>
                    <form class="needs-validation" novalidate>
                        @csrf
                        @include('include.botonera-mas-buscador')

                        <table class="mb-0 table table-bordered my-table">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Precio</th>
                                @if(\Illuminate\Support\Facades\Auth::user()->id_rol == config('constant.rol.id.admin'))
                                    <th>Estado</th>
                                    <th></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach($productos as $producto)
                                    <tr>
                                        <td>{{ $producto->nombre }}</td>
                                        <td>{{ $producto->descripcion }}</td>
                                        <td>${{ $producto->precio }}</td>
                                        @if(\Illuminate\Support\Facades\Auth::user()->id_rol == config('constant.rol.id.admin'))
                                            <td class="estado">
                                                @if($producto->is_activo)
                                                    @php
                                                        $claseBoton = "btn-success";
                                                        $estado = "Activo";
                                                    @endphp
                                                @else
                                                    @php
                                                        $claseBoton = "btn-danger";
                                                        $estado = "Inactivo";
                                                    @endphp
                                                @endif

                                                <button class="btn-shadow btn btnEstado {{ $claseBoton }}" type="button" data-toggle="tooltip" data-placement="bottom" data-id="{{ $producto->id }}" data-state="{{ $producto->is_activo }}">
                                                    {{ $estado }}
                                                </button>
                                            </td>
                                            <td>
                                                <button class="btn-shadow btn btn-primary btnActualizar" type="button" data-toggle="tooltip" data-placement="bottom" data-id="{{ $producto->id }}">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#input-busqueda").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $("#btnExportExcel").on("click", function (e) {
                $(this).prop("href", "{{route('export.excel.productos')}}");
            });

            $("#btnExportPdf").on("click", function (e) {
                $(this).prop("href", "{{route('export.pdf.productos')}}");
            });

            $(".btnActualizar").on("click", function (e) {
                location.href = '/producto/' + $(this).data("id");
            });

            $(".btnEstado").on("click", function (e) {
                let dataId = $(this).data("id");
                let state_id = $(this).data('state');

                let tituloalert = "";
                let mensaje = "";

                if(state_id === 0){
                    tituloalert = '{{ config('constant.alert.tittle.active_state') }}';
                    mensaje = '{{ config('constant.alert.confirm.state_active_product') }}';
                }else{
                    tituloalert = '{{ config('constant.alert.tittle.inactive_estate') }}';
                    mensaje = '{{ config('constant.alert.confirm.state_inactive_product') }}';
                }

                Swal.fire({
                    title: tituloalert,
                    text: mensaje,
                    icon: 'warning',
                    confirmButtonText: '{{ config('constant.alert.button.sure') }}',
                    showCancelButton: true,
                    cancelButtonColor: '#d33',
                    cancelButtonText: '{{ config('constant.alert.button.cancel') }}',
                }).then((result) => {
                    if (result.value) {
                        $('#lightbox-loader').css({"visibility":"visible"});

                        $.ajax({
                            url: '/producto/estado/' + dataId,
                            type: 'POST',
                            data: {
                                _token: $("input[name='_token']").val(),
                            },
                            success: function(data) {
                                $('#lightbox-loader').css({"visibility":"hidden"});
                                let myJsonResponse = $.parseJSON(data); //convierte el response string en json

                                if(myJsonResponse.estado === 200){
                                    $(".estado").find("[data-id='" + dataId + "']").text(myJsonResponse.data.user_estado);
                                    $(".estado").find("[data-id='" + dataId + "']").removeClass("btn-success");
                                    $(".estado").find("[data-id='" + dataId + "']").removeClass("btn-danger");
                                    $(".estado").find("[data-id='" + dataId + "']").addClass(myJsonResponse.data.user_clase);

                                    if(state_id === 0){
                                        $(".estado").find("[data-id='" + dataId + "']").data("state", 1);
                                    }else{
                                        $(".estado").find("[data-id='" + dataId + "']").data("state", 0);
                                    }

                                    Swal.fire('¡Éxito!', myJsonResponse.data.mensaje, 'success')
                                }
                                else{
                                    Swal.fire('¡Error!', myJsonResponse.data.mensaje, 'error')
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                                console.log(JSON.stringify(jqXHR));
                                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                                alert("Hubo un error.");
                                $('#lightbox-loader').css({"visibility":"hidden"});
                            }
                        });
                    }
                })
            });
        })
    </script>
@endsection
