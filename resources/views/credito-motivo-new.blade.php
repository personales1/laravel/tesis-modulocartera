@extends('app')
@section('title', 'Motivo de crédito')

<?php
    $_GET['page'] = "credito";
    $_GET['page-title'] = "Motivo de crédito";
    if(isset($credits)){
        $_GET['page-description'] = "Actualizar motivo de crédito ".$credits->descripcion;

    }else{
        $_GET['page-description'] = "Registro de motivos de crédito para indicar porque se le da crédito a un cliente.";
    }
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de motivos de crédito</h5>
            <form id="myform" method="POST" action="{{ route('credito.motivo.create', ['id' => $credits->id ?? '']) }}" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label for="creditoDescription">Descripción</label>
                        <input name="description" id="creditoDescription" type="text" class="form-control" placeholder="Descripción" value="{{ $credits->descripcion ?? old('description') }}" required>
                        @error('description')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#creditoDescription").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#creditoDescription").on("keyup", function (e) {
                let cadena = $("#creditoDescription").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#creditoDescription").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
