@php
    use Carbon\Carbon;
    $fechaCurrent = Carbon::now()->startOfDay();
@endphp

<script>
    let listDatos = [];
    let listLabels = [];
    let listColors = [];
    let listTotalPagos = [];
</script>
@foreach($pagosConsolidadosPorCalificacion as $rowPagos)
    <script>
        listDatos.push('{{ $rowPagos->porcentaje }}');
        listLabels.push('{{ $rowPagos->estadoPago->ponderacion_texto }}');
        listColors.push('{{ $rowPagos->estadoPago->ponderacion_color_background }}');
        listTotalPagos.push('{{ $rowPagos->total }}');
    </script>
@endforeach

<div>
    <div style="display: flex; flex-wrap: wrap; margin-right: -15px; margin-left: -15px;">
        <div style="flex: 0 0 100%; max-width: 100%">
            <div style="margin-bottom: 30px !important; box-shadow: 0 0.46875rem 2.1875rem rgba(4,9,20,0.03),0 0.9375rem 1.40625rem rgba(4,9,20,0.03),0 0.25rem 0.53125rem rgba(4,9,20,0.05),0 0.125rem 0.1875rem rgba(4,9,20,0.03); border-width: 0; transition: all .2s;">
                <div style="display:flex; align-items: center; border-bottom-width: 1px; padding-top: 0; padding-bottom: 0; padding-right: .625rem; height: 3.5rem;">
                    <h5><b>{{ $totalNumeroDePagos }} pagos registrados</b></h5>
                </div>
                <div style="flex: 1 1 auto; padding: 1.25rem;">
                    <div>
                        <div id="ddd">
                            <canvas id="graphBarChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="{{ asset('assets/scripts/main.js') }}"></script>
<script>
    graphBarHorizontalChart();
    graphBarChart();

    $(document).ready(function () {
        $("#ddd").html("dsdsd")
    });

    function graphBarChart(){
        let ctx = document.getElementById('graphBarChart').getContext('2d');

        var data = {
            datasets: [{
                data: listTotalPagos,
                backgroundColor: listColors,
                label: 'Número de pagos',
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: listLabels,
        };

        var opciones = {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Total de pagos',
                        fontColor: "#000000",
                        fontSize: "20"
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 3
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Calidad crediticia',
                        fontColor: "#000000",
                        fontSize: "20"
                    }
                }]
            }
        };

        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: opciones
        });
    }

    function graphBarHorizontalChart(){
        let ctx = document.getElementById('graphBarHorizontalChart').getContext('2d');

        var data = {
            datasets: [{
                data: listTotalPagos,
                backgroundColor: listColors,
                label: 'Número de pagos',
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: listLabels,
        };

        var opciones = {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Total de pagos',
                        fontColor: "#000000",
                        fontSize: "20"
                    },
                    ticks: {
                        beginAtZero: true,
                        stepSize: 3
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Calidad crediticia',
                        fontColor: "#000000",
                        fontSize: "20"
                    }
                }]
            }
        };

        var myBarChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: data,
            options: opciones
        });
    }
</script>
