@extends('app')
@section('title', 'Perfil de usuario')

<?php
$_GET['page'] = "usuario";
$_GET['page-title'] = "Usuario";
if(isset($user)){
    $_GET['page-description'] = "DatFos del perfil del usuario ". $user->userProfile->nombres . " " . $user->userProfile->apellidos;

}else{
    $_GET['page-description'] = "Datos de perfi de usuario.";
}
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de usuario</h5>
            <form id="myform" method="POST" action="{{ route('usuario.create', ['id' => $users->id ?? '']) }}" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="cantonName">* Nombres</label>
                        <input name="name" id="operadorNombre" type="text" class="form-control" placeholder="Nombres" value="{{ $users->userProfile->nombres ?? old('name') }}" required>
                        @error('name')
                        <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                        <script>
                            $("#operadorNombre").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="operadorApellido">* Apellidos</label>
                        <input name="apellido" id="operadorApellido" type="text" class="form-control" placeholder="Apellidos" value="{{ $users->userProfile->apellidos ?? old('apellido') }}" required>
                        @error('apellido')
                        <label class="mensaje-error" id="mensaje-error-apellido">{{ $message }}</label>
                        <script>
                            $("#operadorApellido").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="rolSelect">* Rol</label>
                        {{ Form::select('rol', $rol, $users->id_rol ?? null, ['class' => 'form-control', 'id' => 'rolSelect']) }}
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-5 mb-3">
                        <label for="operadorCorreo">* Correo (este será su usuario)</label>
                        <input name="correo" id="operadorCorreo" type="text" class="form-control" placeholder="Correo electrónico" value="{{ $users->email ?? old('correo') }}" required>
                        <label class="mensaje-error d-none" id="mensaje-error-correo"></label>
                        @error('correo')
                        <script>
                            $("#mensaje-error-correo").addClass("d-flex");
                            $("#mensaje-error-correo").removeClass("d-none");
                            $("#mensaje-error-correo").html("{{ $message }}");
                            $("#operadorCorreo").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="operadorIdentificacion">* Identificación</label>
                        <input name="identificación" id="operadorIdentificacion" type="text" class="form-control" placeholder="Identificación" value="{{ $users->userProfile->identificacion ?? old('identificación') }}" maxlength="10" required>
                        @error('identificación')
                        <label class="mensaje-error" id="mensaje-error-identificacion">{{ $message }}</label>
                        <script>
                            $("#operadorIdentificacion").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="operadorTelefono">* Teléfono</label>
                        <input name="teléfono" id="operadorTelefono" type="text" class="form-control" placeholder="Teléfono" value="{{ $users->userProfile->telefono ?? old('teléfono') }}" maxlength="10" required>
                        @error('teléfono')
                        <label class="mensaje-error" id="mensaje-error-telefono">{{ $message }}</label>
                        <script>
                            $("#operadorTelefono").addClass("form-control-error");
                        </script>
                        @enderror
                    </div>

                    @if(! isset($users->password))
                        <div class="col-md-3 mb-3">
                            <label for="operadorPassword">* Contraseña</label>
                            <input name="password" id="operadorPassword" type="password" class="form-control" placeholder="Contraseña" value="{{ $users->password ?? old('password') }}" required>
                            @error('password')
                            <label class="mensaje-error" id="mensaje-error-password">{{ $message }}</label>
                            <script>
                                $("#operadorPassword").addClass("form-control-error");
                            </script>
                            @enderror
                        </div>
                    @endif
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var regexpSoloLetra = /^[a-zA-Z áéíóúÁÉÍÓÚñÑ]+$/;
            var regexpSoloNumero = /^[0-9]+$/;
            var regexpEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#operadorNombre").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $('#mensaje-error-name').hide();
                        $(this).removeClass("form-control-error");
                    }
                },
                input: function () {
                    if(! regexpSoloLetra.test($(this).val())){
                        let cadena="";
                        let letter = "";

                        for(let i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(regexpSoloLetra.test(letter)){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                }
            });

            $("#operadorApellido").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $('#mensaje-error-apellido').hide();
                        $(this).removeClass("form-control-error");
                    }
                },
                input: function () {
                    if(! regexpSoloLetra.test($(this).val())){
                        let cadena="";
                        let letter = "";

                        for(let i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(regexpSoloLetra.test(letter)){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                }
            });

            $("#operadorCorreo").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $("#mensaje-error-correo").addClass("d-none");
                        $("#mensaje-error-correo").removeClass("d-flex");
                        $(this).removeClass("form-control-error");
                    }
                },
                blur: function () {
                    if(! regexpEmail.test($(this).val())){
                        $("#mensaje-error-correo").addClass("d-flex");
                        $("#mensaje-error-correo").removeClass("d-none");
                        $("#mensaje-error-correo").html("La dirección email no tiene un formato válido");
                        $(this).addClass("form-control-error");
                    }
                }
            });

            $("#operadorIdentificacion").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $('#mensaje-error-identificacion').hide();
                        $(this).removeClass("form-control-error");
                    }
                },
                input: function () {
                    if(! regexpSoloNumero.test($(this).val())){
                        let cadena="";
                        let letter = "";

                        for(let i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(regexpSoloNumero.test(letter)){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                }
            });

            $("#operadorTelefono").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $('#mensaje-error-telefono').hide();
                        $(this).removeClass("form-control-error");
                    }
                },
                input: function () {
                    if(! regexpSoloNumero.test($(this).val())){
                        let cadena="";
                        let letter = "";

                        for(let i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(regexpSoloNumero.test(letter)){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                }
            });

            $("#operadorPassword").on("keyup", function (e) {
                let cadena = $("#operadorPassword").val();

                if(cadena.length > 0){
                    $('#mensaje-error-password').hide();
                    $("#operadorPassword").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
