@extends('app')
@section('title', 'Clientes')

<?php
$_GET['page'] = "cliente";
$_GET['page-title'] = "Clientes";
$_GET['page-description'] = "Registro de nuevo cliente.";
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de cliente</h5>
            <form id="myform" method="POST" action="{{ route('cliente.create', ['id' => $clientes->id ?? '']) }}" class="needs-validation" enctype="multipart/form-data" novalidate>
                @csrf
                @include('include.form-cliente')
                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>
@endsection
