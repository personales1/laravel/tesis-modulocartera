@extends('app')
@section('title', 'Aprobar crédito')

<?php
$_GET['page'] = "credito";
$_GET['page-title'] = "Aprobar crédito";
$_GET['page-description'] = "Listado de clientes por aprobar crédito";
?>

@section('content-body')
    <script>
        let mensajeExitoso = sessionStorage.getItem("mensaje_exitoso");
        if(mensajeExitoso !== ""){
            Swal.fire('¡Éxito!', mensajeExitoso, 'success');
            sessionStorage.removeItem("mensaje_exitoso");
        }
    </script>
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <h5 class="card-title">Clientes con crédito por aprobar</h5>
                    <form class="needs-validation" novalidate>
                        @csrf
                        @include('include.buscador')

                        <table class="mb-0 table table-bordered my-table">
                            <thead>
                            <tr>
                                <th>Nombres</th>
                                <th>Identificación</th>
                                <th>Teléfono</th>
                                <th>Email</th>
                                <th>Promedio ingreso mensual</th>
                                <th>Promedio gasto mensual</th>
                                <th>Aprobar</th>
                                <th class="d-none"></th>
                            </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach($clientes as $cliente)
                                    <tr>
                                        <td>{{ $cliente->nombres }} {{ $cliente->apellidos }}</td>
                                        <td>{{ $cliente->identificacion }}</td>
                                        <td>@if(empty($cliente->telefono)) {{ $cliente->celular }} @else {{ $cliente->telefono }} / {{ $cliente->celular }} @endif</td>
                                        <td>{{ $cliente->email }}</td>
                                        <td style="background: #c1e7bb; color: #000000;">{{ $cliente->clientWork->ingreso_promedio_mensual }}</td>
                                        <td style="background: #a0331e; color: #ffffff;">{{ $cliente->clientWork->gastos_promedio_mensual }}</td>
                                        <td>
                                            <input class="checkCliente" type="checkbox" data-id="{{ $cliente->id }}">
                                        </td>
                                        <td class="d-none">
                                            <button class="btn-shadow btn btn-primary btnVerCliente" type="button" data-toggle="tooltip" title="Ver cliente" data-placement="bottom" data-id="{{ $cliente->id }}">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <button class="btn btn-primary mt-4 btnAprobarCredito" type="button">Aprobar crédito</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var listClienteId = [];
            var pos = 0;

            $("#input-busqueda").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $(".btnAprobarCredito").on("click", function (e) {
                listClienteId = [];

                $("input:checkbox:checked").each(
                    function () {
                        listClienteId.push($(this).data("id"));
                    }
                );

                if(listClienteId.length > 0){
                    Swal.fire({
                        title: "¡Confirmación!",
                        text: "¿Seguro que deseas aprobarle el crédito a los clientes seleccionados?",
                        icon: 'question',
                        confirmButtonText: '<?php echo e(config('constant.alert.button.sure')); ?>',
                        showCancelButton: true,
                        cancelButtonColor: '#d33',
                        cancelButtonText: '<?php echo e(config('constant.alert.button.cancel')); ?>',
                    }).then((result) => {
                        if (result.value) {
                            aprobarCredito(0);
                        }
                    });
                }
                else{
                    Swal.fire('¡Alerta!', "No hay clientes seleccionados para aprobar crédito", 'warning');
                }
            });

            function aprobarCredito(position){
                $('#lightbox-loader').css({"visibility":"visible"});

                if(position < listClienteId.length){
                    $.ajax({
                        url: '/credito/aprueba/cliente',
                        type: 'POST',
                        data: {
                            _token: $("input[name='_token']").val(),
                            clientId: listClienteId[position]
                        },
                        success: function(data) {
                            pos = pos + 1;
                            aprobarCredito(pos);
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(jqXHR);
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                            alert("Hubo un error.");
                            $('#lightbox-loader').css({"visibility":"hidden"});
                            pos = 0;
                            location.reload();
                        }
                    });
                }else{
                    $('#lightbox-loader').css({"visibility":"hidden"});
                    pos = 0;

                    //Si tenía clientes seleccionados, recarga la página al finalizar.
                    if(listClienteId.length > 0){
                        sessionStorage.setItem("mensaje_exitoso", "La aprobación del crédito se ha realizado con éxito");
                        location.reload();
                    }
                }
            }
        })
    </script>
@endsection



