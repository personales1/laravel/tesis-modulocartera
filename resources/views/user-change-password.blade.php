@extends('app')
@section('title', 'Cambio de contraseña')

<?php
$_GET['page'] = "usuario";
$_GET['page-title'] = "Usuario";
$_GET['page-description'] = "Formulario para cambiar contraseña.";
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Cambio de contraseña</h5>
            <form>
                @csrf
                <div class="form-row">
                    <div class="col-md-12 mb-3">
                        <label for="passwordCurrent">* Contraseña actual</label>
                        <input name="passwordCurrent" id="passwordCurrent" type="text" class="form-control" placeholder="Contraseña actual" required>
                        <label class="mensaje-error" id="mensaje-error-password-current" style="display: none;"></label>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="passwordNew">* Nueva contraseña</label>
                        <input name="passwordNew" id="passwordNew" type="text" class="form-control" placeholder="Nueva contraseña" required>
                        <label class="mensaje-error" id="mensaje-error-password-new" style="display: none;"></label>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="passwordConfirm">* Confirmar contraseña</label>
                        <input name="passwordConfirm" id="passwordConfirm" type="text" class="form-control" placeholder="Confirmar contraseña" required>
                        <label class="mensaje-error" id="mensaje-error-password-new-confirm" style="display: none;"></label>
                    </div>
                    <div class="col-md-12 text-center">
                        <label class="mensaje-error fsize-1" id="mensaje-error-general" style="display: none"></label>
                    </div>
                </div>

                <button class="btn btn-primary btnCambiar" type="button">Cambiar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(".btnCambiar").on("click", function (e) {
                $('#mensaje-error-general').css({"display":"none"});
                $('#mensaje-error-general').html("");

                if(formValido()){
                    $('#lightbox-loader').css({"visibility":"visible"});

                    $.ajax({
                        url:"{{ route('usuario-change-password') }}",
                        type: 'POST',
                        data: {
                            _token: $("input[name='_token']").val(),
                            passwordCurrent: $("#passwordCurrent").val().trim(),
                            passwordNew: $("#passwordNew").val().trim(),
                            passwordConfirm: $("#passwordConfirm").val().trim()
                        },
                        success: function(data) {
                            $('#lightbox-loader').css({"visibility":"hidden"});
                            let myJsonResponse = $.parseJSON(data); //convierte el response string en json

                            if(myJsonResponse.estado >= 400){
                                if(myJsonResponse.estado === 401){
                                    $('#mensaje-error-general').css({"display":"flex"});
                                    $('#mensaje-error-general').html(myJsonResponse.data.mensaje);
                                }
                                else{
                                    $('#mensaje-error-general').css({"display":"flex"});
                                    $('#mensaje-error-general').html(myJsonResponse.data.mensaje);
                                }
                            }
                            else{
                                $('#mensaje-error-general').css({"display":"none"});
                                $('#mensaje-error-general').html("");

                                $("#passwordCurrent").val("");
                                $("#passwordNew").val("");
                                $("#passwordConfirm").val("");

                                $("#passwordCurrent").removeClass("form-control-error");
                                $("#passwordNew").removeClass("form-control-error");
                                $("#passwordConfirm").removeClass("form-control-error");

                                Swal.fire('¡Éxito!', myJsonResponse.data.mensaje, 'success');
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                            alert("Hubo un error.");
                            $('#lightbox-loader').css({"visibility":"hidden"});
                        }
                    });
                }
            });

            $("#passwordCurrent").on("keyup", function (e) {
                let passwordCurrent = $("#passwordCurrent").val();

                if(passwordCurrent.length >= 0){
                    $('#mensaje-error-password-current').hide();
                    $("#passwordCurrent").removeClass("form-control-error");
                }
            });

            $("#passwordNew").on("keyup", function (e) {
                let passwordNew = $("#passwordNew").val().trim();

                if(passwordNew.length > 0 && passwordNew.length < 5){
                    $('#mensaje-error-password-new').css({"display":"flex"});
                    $('#mensaje-error-password-new').html("La contraseña es muy corta, al menos debe tener 5 dígitos.");
                    $("#passwordNew").addClass("form-control-error");
                }else{
                    $('#mensaje-error-password-new').hide();
                    $("#passwordNew").removeClass("form-control-error");

                    if(passwordNew === $("#passwordConfirm").val().trim()){
                        $('#mensaje-error-password-new-confirm').hide();
                        $("#passwordConfirm").removeClass("form-control-error");
                    }
                }
            });

            $("#passwordConfirm").on("keyup", function (e) {
                let passwordConfirm = $("#passwordConfirm").val().trim();

                if(passwordConfirm.length > 0 && passwordConfirm.length < 5){
                    $('#mensaje-error-password-new-confirm').css({"display":"flex"});
                    $('#mensaje-error-password-new-confirm').html("La contraseña es muy corta, al menos debe tener 5 dígitos.");
                    $("#passwordConfirm").addClass("form-control-error");
                }else{
                    $('#mensaje-error-password-new-confirm').hide();
                    $("#passwordConfirm").removeClass("form-control-error");
                }
            });

            function formValido(){
                let retorna = true;

                if($("#passwordCurrent").val().trim() === ""){
                    $('#mensaje-error-password-current').css({"display":"flex"});
                    $('#mensaje-error-password-current').html("Campo obligatorio");
                    $("#passwordCurrent").addClass("form-control-error");
                    retorna = false;
                }

                if($("#passwordNew").val().trim() === ""){
                    $('#mensaje-error-password-new').css({"display":"flex"});
                    $('#mensaje-error-password-new').html("Campo obligatorio");
                    $("#passwordNew").addClass("form-control-error");
                    retorna = false;
                }
                else{
                    if($("#passwordNew").val().trim().length < 5){
                        $('#mensaje-error-password-new').css({"display":"flex"});
                        $('#mensaje-error-password-new').html("La contraseña es muy corta, al menos debe tener 5 dígitos.");
                        $("#passwordNew").addClass("form-control-error");
                        retorna = false;
                    }
                }

                if($("#passwordConfirm").val().trim() === ""){
                    $('#mensaje-error-password-new-confirm').css({"display":"flex"});
                    $('#mensaje-error-password-new-confirm').html("Campo obligatorio");
                    $("#passwordConfirm").addClass("form-control-error");
                    retorna = false;
                }
                else{
                    if($("#passwordConfirm").val().trim().length < 5){
                        $('#mensaje-error-password-new-confirm').css({"display":"flex"});
                        $('#mensaje-error-password-new-confirm').html("La contraseña es muy corta, al menos debe tener 5 dígitos.");
                        $("#passwordConfirm").addClass("form-control-error");
                        retorna = false;
                    }
                    else{
                        if($("#passwordNew").val().trim() !== "" && ($("#passwordNew").val().trim() !== $("#passwordConfirm").val().trim())){
                            $('#mensaje-error-password-new-confirm').css({"display":"flex"});
                            $('#mensaje-error-password-new-confirm').html("Las contraseñas ingresadas no coinciden.");
                            $("#passwordConfirm").addClass("form-control-error");
                            retorna = false;
                        }
                    }
                }

                return retorna;
            }
        });
    </script>
@endsection
