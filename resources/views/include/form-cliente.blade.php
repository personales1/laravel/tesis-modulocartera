<div class="card-body">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a data-toggle="tab" href="#tab-eg10-0" class="nav-link show active">Datos personales &nbsp;&nbsp;&nbsp;&nbsp;<img src="{{ asset('assets/images/iconobligatorio.png') }}" width="20" id="alertDatosPersonales" style="display: none"></a></li>
        <li class="nav-item"><a data-toggle="tab" href="#tab-eg10-1" class="nav-link show">Datos del trabajo &nbsp;&nbsp;&nbsp;&nbsp;<img src="{{ asset('assets/images/iconobligatorio.png') }}" width="20" id="alertDatosTrabajo" style="display: none"></a></li>
        <li class="nav-item"><a data-toggle="tab" href="#tab-eg10-2" class="nav-link show">Documentos &nbsp;&nbsp;&nbsp;&nbsp;<img src="{{ asset('assets/images/iconobligatorio.png') }}" width="20" id="alertDatosDocumentos" style="display: none"></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane show active" id="tab-eg10-0" role="tabpanel">
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="name">* Nombres</label>
                    <input name="name" id="name" type="text" class="form-control" placeholder="Nombres" value="{{ $clientes->nombres ?? old('name') }}" required>
                    <label class="mensaje-error" id="mensaje-error-name" style="display: none"></label>
                    @error('name')
                        <script>
                            $("#mensaje-error-name").css({"display":"flex"});
                            $("#mensaje-error-name").html('{{ $message }}');
                            $("#name").addClass("form-control-error");
                        </script>
                    @enderror
                </div>

                <div class="col-md-6 mb-3">
                    <label for="lastName">* Apellidos</label>
                    <input name="apellido" id="lastName" type="text" class="form-control" placeholder="Apellidos" value="{{ $clientes->apellidos ?? old('apellido') }}" required>
                    <label class="mensaje-error" id="mensaje-error-lastName" style="display: none"></label>
                    @error('apellido')
                        <script>
                            $("#mensaje-error-lastName").css({"display":"flex"});
                            $("#mensaje-error-lastName").html('{{ $message }}');
                            $("#lastName").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="dni">* Identificación</label>
                    <input name="identificación" id="dni" type="text" class="form-control" placeholder="Identificación" value="{{ $clientes->identificacion ?? old('identificación') }}" required maxlength="10">
                    <label class="mensaje-error" id="mensaje-error-dni" style="display: none"></label>
                    @error('identificación')
                        <script>
                            $("#mensaje-error-dni").css({"display":"flex"});
                            $("#mensaje-error-dni").html('{{ $message }}');
                            $("#dni").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
                <div class="col-md-2 mb-3">
                    <label for="fechaNacimiento">* Fecha de nacimiento</label>
                    <input name="fechaNacimiento" id="fechaNacimiento" type="text" class="form-control datepicker" placeholder="Fecha" value="{{ $clientes->fec_nac ?? old('fechaNacimiento') }}" required>
                    <label class="mensaje-error" id="mensaje-error-fecnac" style="display: none"></label>
                    @error('fechaNacimiento')
                        <script>
                            $("#mensaje-error-fecnac").css({"display":"flex"});
                            $("#mensaje-error-fecnac").html('{{ $message }}');
                            $("#fechaNacimiento").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
                <div class="col-md-1 mb-3" style="display: none">
                    <label for="edad">Edad</label>
                    <input name="edad" id="edad" type="text" class="form-control" placeholder="Edad" value="{{ old('edad') }}" readonly>
                </div>
                <div class="col-md-5 mb-3">
                    <label for="correo">* Correo electrónico</label>
                    <input name="correo" id="correo" type="text" class="form-control" placeholder="Correo electrónico" value="{{ $clientes->email ?? old('correo') }}" required>
                    <label class="mensaje-error" id="mensaje-error-correo" style="display: none"></label>
                    @error('correo')
                        <script>
                            $("#mensaje-error-correo").css({"display":"flex"});
                            $("#mensaje-error-correo").html('{{ $message }}');
                            $("#correo").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="telefono">Teléfono</label>
                    <input name="telefono" id="telefono" type="text" class="form-control" placeholder="Teléfono" value="{{ $clientes->telefono ?? old('telefono') }}" maxlength="10">
                </div>
                <div class="col-md-3 mb-3">
                    <label for="celular">* Celular</label>
                    <input name="celular" id="celular" type="text" class="form-control" placeholder="Celular" value="{{ $clientes->celular ?? old('celular') }}" required maxlength="10">
                    <label class="mensaje-error" id="mensaje-error-celular" style="display: none"></label>
                    @error('celular')
                        <script>
                            $("#mensaje-error-celular").css({"display":"flex"});
                            $("#mensaje-error-celular").html('{{ $message }}');
                            $("#celular").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
                <div class="col-md-3 mb-3">
                    <label for="sexoSelect">Sexo</label>
                    <select name="sexo" id="sexoSelect" class="form-control">
                        <option value="MASCULINO" @if(isset($clientes) && $clientes->sexo == "MASCULINO") selected @endif>Masculino</option>
                        <option value="FEMENINO" @if(isset($clientes) && $clientes->sexo == "FEMENINO") selected @endif>Femenino</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <label for="direccion">* Dirección</label>
                    <input name="dirección" id="direccion" type="text" class="form-control" placeholder="Dirección" value="{{ $clientes->direccion ?? old('dirección') }}" required>
                    <label class="mensaje-error" id="mensaje-error-direccion" style="display: none"></label>
                    @error('dirección')
                        <script>
                            $("#mensaje-error-direccion").css({"display":"flex"});
                            $("#mensaje-error-direccion").html('{{ $message }}');
                            $("#direccion").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <label for="referencia">* Referencia dirección</label>
                    <input name="referencia" id="referencia" type="text" class="form-control" placeholder="Referencia dirección" value="{{ $clientes->direccion_referencia ?? old('referencia') }}" required>
                    <label class="mensaje-error" id="mensaje-error-referencia" style="display: none"></label>
                    @error('referencia')
                        <script>
                            $("#mensaje-error-referencia").css({"display":"flex"});
                            $("#mensaje-error-referencia").html('{{ $message }}');
                            $("#referencia").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="provinceSelect">* Provincia</label>
                    {{ Form::select('provincia', $provincia, $clientes->province_id ?? null, ['class' => 'form-control', 'id' => 'provinceSelect']) }}
                    @error('provincia')
                        <label class="mensaje-error" id="mensaje-error-provincia">{{ $message }}</label>
                        <script>
                            $("#provinceSelect").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
                <div class="col-md-3 mb-3">
                    <label for="cantonSelect">* Cantón</label>
                    {{ Form::select('canton', [''], $clientes->canton_id ?? null, ['class' => 'form-control', 'id' => 'cantonSelect']) }}
                    @error('canton')
                        <label class="mensaje-error" id="mensaje-error-canton">{{ $message }}</label>
                        <script>
                            $("#cantonSelect").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
                <div class="col-md-3 mb-3">
                    <label for="parroquiaSelect">* Parroquia</label>
                    {{ Form::select('parroquia', [''], $clientes->parroquia_id ?? null, ['class' => 'form-control', 'id' => 'parroquiaSelect']) }}
                    @error('parroquia')
                        <label class="mensaje-error" id="mensaje-error-parroquia">{{ $message }}</label>
                        <script>
                            $("#parroquiaSelect").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
                <div class="col-md-3 mb-3">
                    <label for="clienteSectorSelect">* Sector</label>
                    {{ Form::select('sector', $sector, $clientes->sector_id ?? null, ['class' => 'form-control', 'id' => 'clienteSectorSelect']) }}
                    @error('listSector')
                        <label class="mensaje-error" id="mensaje-error-sector">{{ $message }}</label>
                        <script>
                            $("#clienteSectorSelect").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
            </div>
        </div>






        <div class="tab-pane show" id="tab-eg10-1" role="tabpanel">
            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <label for="empresa">* Empresa donde labora</label>
                    <input name="empresa" id="empresa" type="text" class="form-control" placeholder="Nombre de empresa" value="{{ $clientes->clientWork->empresa ?? old('empresa') }}" required>
                    <label class="mensaje-error" id="mensaje-error-empresa" style="display: none"></label>
                    @error('empresa')
                        <script>
                            $("#mensaje-error-empresa").css({"display":"flex"});
                            $("#mensaje-error-empresa").html('{{ $message }}');
                            $("#empresa").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="fechaIngreso">* Fecha de ingreso</label>
                    <input name="fechaIngreso" id="fechaIngreso" type="text" class="form-control datepickerIngreso" placeholder="Fecha" value="{{ $clientes->clientWork->fec_ing ?? old('fechaIngreso') }}" required>
                    <label class="mensaje-error" id="mensaje-error-fechaing" style="display: none"></label>
                    @error('fechaIngreso')
                        <script>
                            $("#mensaje-error-fechaing").css({"display":"flex"});
                            $("#mensaje-error-fechaing").html('{{ $message }}');
                            $("#fechaIngreso").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
                <div class="col-md-3 mb-3">
                    <label for="cargoSelect">Cargo</label>
                    {{ Form::select('cargo', $cargo, $clientes->clientWork->cargo_id ?? null , ['class' => 'form-control', 'id' => 'cargoSelect']) }}
                </div>
                <div class="col-md-5 mb-3">
                    <label for="cargoDescripcion">Describe cargo</label>
                    <input name="cargoDescripcion" id="cargoDescripcion" type="text" class="form-control" placeholder="Si escoge 'Otro', describe cargo" value="{{ old('cargoDescripcion') }}" readonly>
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="areaSelect">Área</label>
                    {{ Form::select('area', $area, $clientes->clientWork->area_id ?? null, ['class' => 'form-control', 'id' => 'areaSelect']) }}
                </div>
                <div class="col-md-5 mb-3">
                    <label for="areaDescripcion">Describe área</label>
                    <input name="areaDescripcion" id="areaDescripcion" type="text" class="form-control" placeholder="Si escoge 'Otro', describe área" value="{{ old('areaDescripcion') }}" readonly>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="telefonoTrabajo">Teléfono</label>
                    <input name="telefonoTrabajo" id="telefonoTrabajo" type="text" class="form-control" placeholder="Teléfono" value="{{ $clientes->clientWork->telefono ?? old('telefonoTrabajo') }}" maxlength="10">
                </div>
                <div class="col-md-1 mb-3">
                    <label for="extension">Ext.</label>
                    <input name="extension" id="extension" type="text" class="form-control" placeholder="Ext." value="{{ $clientes->clientWork->extension ?? old('extension') }}" maxlength="4">
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <label for="direccionTrabajo">* Dirección</label>
                    <input name="direccionTrabajo" id="direccionTrabajo" type="text" class="form-control" placeholder="Dirección" value="{{ $clientes->clientWork->direccion ?? old('direccionTrabajo') }}" required>
                    <label class="mensaje-error" id="mensaje-error-direcciontrabajo" style="display: none"></label>
                    @error('direccionTrabajo')
                        <script>
                            $("#mensaje-error-direcciontrabajo").css({"display":"flex"});
                            $("#mensaje-error-direcciontrabajo").html('{{ $message }}');
                            $("#direccionTrabajo").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <label for="referenciaTrabajo">* Referencia dirección</label>
                    <input name="referenciaTrabajo" id="referenciaTrabajo" type="text" class="form-control" placeholder="Referencia dirección" value="{{ $clientes->clientWork->direccion_referencia ?? old('referenciaTrabajo') }}" required>
                    <label class="mensaje-error" id="mensaje-error-referenciatrabajo" style="display: none"></label>
                    @error('referenciaTrabajo')
                        <script>
                            $("#mensaje-error-referenciatrabajo").css({"display":"flex"});
                            $("#mensaje-error-referenciatrabajo").html('{{ $message }}');
                            $("#referenciaTrabajo").addClass("form-control-error");
                        </script>
                    @enderror
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="periodoIngreso" class="">Período ingreso</label>
                    <select name="periodoIngreso" id="periodoIngreso" class="form-control">
                        <option value="DIARIO" @if(isset($clientes) && $clientes->clientWork->ingreso_periodo == "DIARIO") selected @endif>Diario</option>
                        <option value="SEMANAL" @if(isset($clientes) && $clientes->clientWork->ingreso_periodo == "SEMANAL") selected @endif>Semanal</option>
                        <option value="QUINCENAL" @if(isset($clientes) && $clientes->clientWork->ingreso_periodo == "QUINCENAL") selected @endif>Quincenal</option>
                        <option value="MENSUAL" @if(isset($clientes) && $clientes->clientWork->ingreso_periodo == "MENSUAL") selected @endif>Mensual</option>
                    </select>
                </div>

                <div class="col-md-4 mb-3">
                    <label for="promedioIngreso" class="">Promedio ingresos mensuales</label>
                    <select name="promedioIngreso" id="promedioIngreso" class="form-control">
                        <option value="< $398" @if(isset($clientes) && $clientes->clientWork->ingreso_promedio_mensual == "< $398") selected @endif>< $398</option>
                        <option value="$398 - $449" @if(isset($clientes) && $clientes->clientWork->ingreso_promedio_mensual == "$398 - $449") selected @endif>$398 - $449</option>
                        <option value="$450 - $599" @if(isset($clientes) && $clientes->clientWork->ingreso_promedio_mensual == "$450 - $599") selected @endif>$450 - $599</option>
                        <option value="$600 - $799" @if(isset($clientes) && $clientes->clientWork->ingreso_promedio_mensual == "$600 - $799") selected @endif>$600 - $799</option>
                        <option value="$800 - $999" @if(isset($clientes) && $clientes->clientWork->ingreso_promedio_mensual == "$800 - $999") selected @endif>$800 - $999</option>
                        <option value="$1000 - $1499" @if(isset($clientes) && $clientes->clientWork->ingreso_promedio_mensual == "$1000 - $1499") selected @endif>$1000 - $1499</option>
                        <option value="$1499 - $1999" @if(isset($clientes) && $clientes->clientWork->ingreso_promedio_mensual == "$1499 - $1999") selected @endif>$1499 - $1999</option>
                        <option value="> $2000" @if(isset($clientes) && $clientes->clientWork->ingreso_promedio_mensual == "> $2000") selected @endif>> $2000</option>
                    </select>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="promedioGasto" class="">Promedio gastos mensuales</label>
                    <select name="promedioGasto" id="promedioGasto" class="form-control">
                        <option value="< $398" @if(isset($clientes) && $clientes->clientWork->gastos_promedio_mensual == "< $398") selected @endif>< $398</option>
                        <option value="$398 - $449" @if(isset($clientes) && $clientes->clientWork->gastos_promedio_mensual == "$398 - $449") selected @endif>$398 - $449</option>
                        <option value="$450 - $599" @if(isset($clientes) && $clientes->clientWork->gastos_promedio_mensual == "$450 - $599") selected @endif>$450 - $599</option>
                        <option value="$600 - $799" @if(isset($clientes) && $clientes->clientWork->gastos_promedio_mensual == "$600 - $799") selected @endif>$600 - $799</option>
                        <option value="$800 - $999" @if(isset($clientes) && $clientes->clientWork->gastos_promedio_mensual == "$800 - $999") selected @endif>$800 - $999</option>
                        <option value="$1000 - $1499" @if(isset($clientes) && $clientes->clientWork->gastos_promedio_mensual == "$1000 - $1499") selected @endif>$1000 - $1499</option>
                        <option value="$1499 - $1999" @if(isset($clientes) && $clientes->clientWork->gastos_promedio_mensual == "$1499 - $1999") selected @endif>$1499 - $1999</option>
                        <option value="> $2000" @if(isset($clientes) && $clientes->clientWork->gastos_promedio_mensual == "> $2000") selected @endif>> $2000</option>
                    </select>
                </div>
            </div>
        </div>





        <div class="tab-pane show " id="tab-eg10-2" role="tabpanel">
            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <h5 class="card-title">Documentos de identificación</h5>
                </div>

                <div class="col-md-4 mb-3 uploader">
                    <input name="fileIdentificacion1" id="file-identificacion-1" type="file" class="form-control-file file-cliente" accept="image/*" data-image-id="identificacion1">
                    <label for="file-identificacion-1">
                        @if(isset($imageIdentificacion) && isset($imageIdentificacion[0]))
                            <img class="image-preview " src="http://tesis.modulocartera/image_system/clientes/documentos/identificacion/{{ $clientes->identificacion }}/{{ $imageIdentificacion[0]->foto }}" alt="Preview" id="identificacion1">
                        @else
                            <img class="image-preview hidden" src="#" alt="Preview" id="identificacion1">

                            <div class="contenedor-select-image" id="contenedor-select-identificacion1">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <div>Selecciona una imagen</div>
                            </div>
                        @endif
                    </label>
                </div>

                <div class="col-md-4 mb-3 uploader">
                    <input name="fileIdentificacion2" id="file-identificacion-2" type="file" class="form-control-file file-cliente" accept="image/*" data-image-id="identificacion2">
                    <label for="file-identificacion-2">
                        @if(isset($imageIdentificacion) && isset($imageIdentificacion[1]))
                            <img class="image-preview " src="http://tesis.modulocartera/image_system/clientes/documentos/identificacion/{{ $clientes->identificacion }}/{{ $imageIdentificacion[1]->foto }}" alt="Preview" id="identificacion2">
                        @else
                            <img class="image-preview hidden" src="#" alt="Preview" id="identificacion2">

                            <div class="contenedor-select-image" id="contenedor-select-identificacion2">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <div>Selecciona una imagen</div>
                            </div>
                        @endif
                    </label>
                </div>

                <div class="col-md-4 mb-3 uploader">
                    <input name="fileIdentificacion3" id="file-identificacion-3" type="file" class="form-control-file file-cliente" accept="image/*" data-image-id="identificacion3">
                    <label for="file-identificacion-3">
                        @if(isset($imageIdentificacion) && isset($imageIdentificacion[2]))
                            <img class="image-preview " src="http://tesis.modulocartera/image_system/clientes/documentos/identificacion/{{ $clientes->identificacion }}/{{ $imageIdentificacion[2]->foto }}" alt="Preview" id="identificacion3">
                        @else
                            <img class="image-preview hidden" src="#" alt="Preview" id="identificacion3">

                            <div class="contenedor-select-image" id="contenedor-select-identificacion3">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <div>Selecciona una imagen</div>
                            </div>
                        @endif
                    </label>
                </div>
            </div>

            <hr>

            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <h5 class="card-title">Roles de pago</h5>
                </div>

                <div class="col-md-4 mb-3 uploader">
                    <input name="fileRoles1" id="file-roles-1" type="file" class="form-control-file file-cliente" accept="image/*" data-image-id="roles1">
                    <label for="file-roles-1">
                        @if(isset($imageRoles) && isset($imageRoles[0]))
                            <img class="image-preview " src="http://tesis.modulocartera/image_system/clientes/documentos/roles/{{ $clientes->identificacion }}/{{ $imageRoles[0]->foto }}" alt="Preview" id="roles1">
                        @else
                            <img class="image-preview hidden" src="#" alt="Preview" id="roles1">

                            <div class="contenedor-select-image" id="contenedor-select-roles1">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <div>Selecciona una imagen</div>
                            </div>
                        @endif
                    </label>
                </div>

                <div class="col-md-4 mb-3 uploader">
                    <input name="fileRoles2" id="file-roles-2" type="file" class="form-control-file file-cliente" accept="image/*" data-image-id="roles2">
                    <label for="file-roles-2">
                        @if(isset($imageRoles) && isset($imageRoles[1]))
                            <img class="image-preview " src="http://tesis.modulocartera/image_system/clientes/documentos/roles/{{ $clientes->identificacion }}/{{ $imageRoles[1]->foto }}" alt="Preview" id="roles2">
                        @else
                            <img class="image-preview hidden" src="#" alt="Preview" id="roles2">

                            <div class="contenedor-select-image" id="contenedor-select-roles2">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <div>Selecciona una imagen</div>
                            </div>
                        @endif
                    </label>
                </div>

                <div class="col-md-4 mb-3 uploader">
                    <input name="fileRoles3" id="file-roles-3" type="file" class="form-control-file file-cliente" accept="image/*" data-image-id="roles3">
                    <label for="file-roles-3">
                        @if(isset($imageRoles) && isset($imageRoles[2]))
                            <img class="image-preview " src="http://tesis.modulocartera/image_system/clientes/documentos/roles/{{ $clientes->identificacion }}/{{ $imageRoles[2]->foto }}" alt="Preview" id="roles3">
                        @else
                            <img class="image-preview hidden" src="#" alt="Preview" id="roles3">

                            <div class="contenedor-select-image" id="contenedor-select-roles3">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <div>Selecciona una imagen</div>
                            </div>
                        @endif
                    </label>
                </div>
            </div>

            <hr>
            
            <div class="form-row">
                <div class="col-md-12 mb-3">
                    <h5 class="card-title">Otros documentos</h5>
                </div>

                <div class="col-md-4 mb-3 uploader">
                    <input name="fileOtros1" id="file-otros-1" type="file" class="form-control-file file-cliente" accept="image/*" data-image-id="otros1">
                    <label for="file-otros-1">
                        @if(isset($imageOtros) && isset($imageOtros[0]))
                            <img class="image-preview " src="http://tesis.modulocartera/image_system/clientes/documentos/otros/{{ $clientes->identificacion }}/{{ $imageOtros[0]->foto }}" alt="Preview" id="otros1">
                        @else
                            <img class="image-preview hidden" src="#" alt="Preview" id="otros1">

                            <div class="contenedor-select-image" id="contenedor-select-otros1">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <div>Selecciona una imagen</div>
                            </div>
                        @endif
                    </label>
                </div>

                <div class="col-md-4 mb-3 uploader">
                    <input name="fileOtros2" id="file-otros-2" type="file" class="form-control-file file-cliente" accept="image/*" data-image-id="otros2">
                    <label for="file-otros-2">
                        @if(isset($imageOtros) && isset($imageOtros[1]))
                            <img class="image-preview " src="http://tesis.modulocartera/image_system/clientes/documentos/otros/{{ $clientes->identificacion }}/{{ $imageOtros[1]->foto }}" alt="Preview" id="otros2">
                        @else
                            <img class="image-preview hidden" src="#" alt="Preview" id="otros2">

                            <div class="contenedor-select-image" id="contenedor-select-otros2">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <div>Selecciona una imagen</div>
                            </div>
                        @endif
                    </label>
                </div>

                <div class="col-md-4 mb-3 uploader">
                    <input name="fileOtros3" id="file-otros-3" type="file" class="form-control-file file-cliente" accept="image/*" data-image-id="otros3">
                    <label for="file-otros-3">
                        @if(isset($imageOtros) && isset($imageOtros[2]))
                            <img class="image-preview " src="http://tesis.modulocartera/image_system/clientes/documentos/otros/{{ $clientes->identificacion }}/{{ $imageOtros[2]->foto }}" alt="Preview" id="otros3">
                        @else
                            <img class="image-preview hidden" src="#" alt="Preview" id="otros3">

                            <div class="contenedor-select-image" id="contenedor-select-otros3">
                                <i class="fa fa-upload" aria-hidden="true"></i>
                                <div>Selecciona una imagen</div>
                            </div>
                        @endif
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true
    });

    $('.datepickerIngreso').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true
    });

    $(document).ready(function() {
        var isDatosPersonales = false;
        var isDatosTrabajo = false;
        var isDatosDocumentos = false;

        var regexpSoloLetra = /^[a-zA-Z áéíóúÁÉÍÓÚñÑ]+$/;
        var regexpSoloNumero = /^[0-9]+$/;
        var regexpEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var provincia = $("#provinceSelect").val();
        getCanton(provincia);

        $("#name").on({
            keyup: function () {
                if($(this).val().length > 0){
                    $('#mensaje-error-name').hide();
                    $(this).removeClass("form-control-error");
                }
            },
            input: function () {
                if(! regexpSoloLetra.test($(this).val())){
                    var cadena="";
                    var letter = "";

                    for(i=0; i < $(this).val().trim().length; i++){
                        letter = $(this).val().charAt(i);

                        if(regexpSoloLetra.test(letter)){
                            cadena += letter;
                        }
                    }

                    $(this).val(cadena);
                }
            }
        });

        $("#lastName").on({
            keyup: function () {
                if($(this).val().length > 0){
                    $('#mensaje-error-lastName').hide();
                    $(this).removeClass("form-control-error");
                }
            },
            input: function () {
                if(! regexpSoloLetra.test($(this).val())){
                    var cadena="";
                    var letter = "";

                    for(i=0; i < $(this).val().trim().length; i++){
                        letter = $(this).val().charAt(i);

                        if(regexpSoloLetra.test(letter)){
                            cadena += letter;
                        }
                    }

                    $(this).val(cadena);
                }
            }
        });

        $("#dni").on({
            keyup: function () {
                if($(this).val().length > 0){
                    $('#mensaje-error-dni').hide();
                    $(this).removeClass("form-control-error");
                }
            },
            input: function () {
                if(! regexpSoloNumero.test($(this).val())){
                    var cadena="";
                    var letter = "";

                    for(i=0; i < $(this).val().trim().length; i++){
                        letter = $(this).val().charAt(i);

                        if(regexpSoloNumero.test(letter)){
                            cadena += letter;
                        }
                    }

                    $(this).val(cadena);
                }
            }
        });

        $("#fechaNacimiento").on("change", function (e) {
            if($("#fechaNacimiento").val().length > 0){
                $('#mensaje-error-fecnac').hide();
                $("#fechaNacimiento").removeClass("form-control-error");
            }
        });

        $("#correo").on("keyup", function (e) {
            if($("#correo").val().length > 0){
                $('#mensaje-error-correo').hide();
                $("#correo").removeClass("form-control-error");
            }
        });

        $("#telefono").on({
            input: function () {
                if(! regexpSoloNumero.test($(this).val())){
                    var cadena="";
                    var letter = "";

                    for(i=0; i < $(this).val().trim().length; i++){
                        letter = $(this).val().charAt(i);

                        if(regexpSoloNumero.test(letter)){
                            cadena += letter;
                        }
                    }

                    $(this).val(cadena);
                }
            }
        });

        $("#celular").on({
            keyup: function () {
                if($(this).val().length > 0){
                    $('#mensaje-error-celular').hide();
                    $(this).removeClass("form-control-error");
                }
            },
            input: function () {
                if(! regexpSoloNumero.test($(this).val())){
                    var cadena="";
                    var letter = "";

                    for(i=0; i < $(this).val().trim().length; i++){
                        letter = $(this).val().charAt(i);

                        if(regexpSoloNumero.test(letter)){
                            cadena += letter;
                        }
                    }

                    $(this).val(cadena);
                }
            }
        });

        $("#direccion").on("keyup", function (e) {
            if($("#direccion").val().length > 0){
                $('#mensaje-error-direccion').hide();
                $("#direccion").removeClass("form-control-error");
            }
        });

        $("#referencia").on("keyup", function (e) {
            if($("#referencia").val().length > 0){
                $('#mensaje-error-referencia').hide();
                $("#referencia").removeClass("form-control-error");
            }
        });

        $("#provinceSelect").change(function () {
            $('#mensaje-error-canton').hide();
            $('#cantonSelect').removeClass("form-control-error");
            provincia = $(this).val();
            getCanton(provincia);
        });

        $("#cantonSelect").change(function () {
            $('#mensaje-error-parroquia').hide();
            $('#parroquiaSelect').removeClass("form-control-error");
            var canton = $(this).val();
            getParroquia(canton);
        });

        $("#empresa").on("keyup", function (e) {
            if($("#empresa").val().length > 0){
                $('#mensaje-error-empresa').hide();
                $("#empresa").removeClass("form-control-error");
            }
        });

        $("#fechaIngreso").on("change", function (e) {
            if($("#fechaIngreso").val().length > 0){
                $('#mensaje-error-fechaing').hide();
                $("#fechaIngreso").removeClass("form-control-error");
            }
        });

        $("#cargoSelect").change(function () {
            if($(this).val() === "Z-OTRO"){
                $("#cargoDescripcion").prop("readonly", false);
            }
            else{
                $("#cargoDescripcion").prop("readonly", true);
                $("#cargoDescripcion").val("");
            }
        });

        $("#areaSelect").change(function () {
            if($(this).val() === "Z-OTRO"){
                $("#areaDescripcion").prop("readonly", false);
            }
            else{
                $("#areaDescripcion").prop("readonly", true);
                $("#areaDescripcion").val("");
            }
        });

        $("#telefonoTrabajo").on({
            input: function () {
                if(! regexpSoloNumero.test($(this).val())){
                    var cadena="";
                    var letter = "";

                    for(i=0; i < $(this).val().trim().length; i++){
                        letter = $(this).val().charAt(i);

                        if(regexpSoloNumero.test(letter)){
                            cadena += letter;
                        }
                    }

                    $(this).val(cadena);
                }
            }
        });

        $("#extension").on({
            input: function () {
                if(! regexpSoloNumero.test($(this).val())){
                    var cadena="";
                    var letter = "";

                    for(i=0; i < $(this).val().trim().length; i++){
                        letter = $(this).val().charAt(i);

                        if(regexpSoloNumero.test(letter)){
                            cadena += letter;
                        }
                    }

                    $(this).val(cadena);
                }
            }
        });

        $("#direccionTrabajo").on("keyup", function (e) {
            if($("#direccionTrabajo").val().length > 0){
                $('#mensaje-error-direcciontrabajo').hide();
                $("#direccionTrabajo").removeClass("form-control-error");
            }
        });

        $("#referenciaTrabajo").on("keyup", function (e) {
            if($("#referenciaTrabajo").val().length > 0){
                $('#mensaje-error-referenciatrabajo').hide();
                $("#referenciaTrabajo").removeClass("form-control-error");
            }
        });

        $(".file-cliente").on("change", function () {
            idInputFile = $(this).data("imageId");

            if (this.files && this.files[0]){
                var reader = new FileReader();

                reader.onload = function (e) {
                    $("#" + idInputFile).attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
                $("#" + idInputFile).removeClass('hidden');
                $("#contenedor-select-" + idInputFile).hide();
            }
        });

        function getCanton(idProvincia){
            $.ajax({
                url:"{{ route('canton.find.by.province') }}",
                type: 'POST',
                data: {
                    _token: $("input[name='_token']").val(),
                    provinciaId: idProvincia
                },
                success: function(data) {
                    console.log(data);
                    $('#cantonSelect').html(data);

                    var canton = $("#cantonSelect").val();
                    getParroquia(canton);
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error en getCanton: " + textStatus + ' : ' + errorThrown);
                    alert("Hubo un error.");
                }
            });
        }

        function getParroquia(idCanton){
            $.ajax({
                url:"{{ route('parroquia.find.by.canton') }}",
                type: 'POST',
                data: {
                    _token: $("input[name='_token']").val(),
                    cantonId: idCanton
                },
                success: function(data) {
                    console.log(data);
                    $('#parroquiaSelect').html(data);
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error en getParroquia: " + textStatus + ' : ' + errorThrown);
                    alert("Hubo un error.");
                }
            });
        }

        function validaCedula(cedula){
            var cad = cedula;
            var total = 0;
            var longitud = cad.length;
            var longcheck = longitud - 1;

            if(longitud == 10){
                for(i = 0; i < longcheck; i++){
                    if (i%2 === 0) {
                        var aux = cad.charAt(i) * 2;
                        if (aux > 9) aux -= 9;
                        total += aux;
                    } else {
                        total += parseInt(cad.charAt(i));
                    }
                }

                total = total % 10 ? 10 - total % 10 : 0;

                if (cad.charAt(longitud-1) == total) {
                    console.log('la cedula:' + cedula + ' es correcta');
                    return true;
                }else{
                    console.log('la cedula:' + cedula + ' es incorrecta');
                    return false;
                }
            }else{
                return false;
            }
        }

        function isFormValido(){
            var retorna = true;
            isDatosPersonales = false;
            isDatosTrabajo = false;
            isDatosDocumentos = false;

            if($("#name").val().trim() == ""){
                $("#mensaje-error-name").css({"display":"flex"});
                $("#mensaje-error-name").html("Campo obligatorio");
                $("#name").addClass("form-control-error");
                isDatosPersonales = true;
                retorna = false;
            }else{
                $("#mensaje-error-name").html("");
                $("#name").removeClass("form-control-error");
            }

            if($("#lastName").val().trim() == ""){
                $("#mensaje-error-lastName").css({"display":"flex"});
                $("#mensaje-error-lastName").html("Campo obligatorio");
                $("#lastName").addClass("form-control-error");
                isDatosPersonales = true;
                retorna = false;
            }else{
                $("#mensaje-error-lastName").html("");
                $("#lastName").removeClass("form-control-error");
            }

            if($("#dni").val().trim() == ""){
                $("#mensaje-error-dni").css({"display":"flex"});
                $("#mensaje-error-dni").html("Campo obligatorio");
                $("#dni").addClass("form-control-error");
                isDatosPersonales = true;
                retorna = false;
            }else{
                if(! validaCedula($("#dni").val().trim())){
                    $("#mensaje-error-dni").css({"display":"flex"});
                    $('#mensaje-error-dni').text("El número de identificación es inválido");
                    $("#dni").addClass("form-control-error");
                    isDatosPersonales = true;
                    retorna = false;
                }else{
                    $("#mensaje-error-dni").html("");
                    $("#dni").removeClass("form-control-error");
                }
            }

            if($("#fechaNacimiento").val().trim() == ""){
                $("#mensaje-error-fecnac").css({"display":"flex"});
                $("#mensaje-error-fecnac").html("Campo obligatorio");
                $("#fechaNacimiento").addClass("form-control-error");
                isDatosPersonales = true;
                retorna = false;
            }else{
                $("#mensaje-error-fecnac").html("");
                $("#fechaNacimiento").removeClass("form-control-error");
            }

            if($("#correo").val().trim() == ""){
                $("#mensaje-error-correo").css({"display":"flex"});
                $("#mensaje-error-correo").html("Campo obligatorio");
                $("#correo").addClass("form-control-error");
                isDatosPersonales = true;
                retorna = false;
            }else{
                if(! regexpEmail.test($("#correo").val().trim())){
                    $("#mensaje-error-correo").css({"display":"flex"});
                    $("#mensaje-error-correo").html("La dirección email no tiene un formato válido");
                    $("#correo").addClass("form-control-error");
                    retorna = false;
                }else{
                    $("#mensaje-error-correo").html("");
                    $("#correo").removeClass("form-control-error");
                }
            }

            if($("#celular").val().trim() == ""){
                $("#mensaje-error-celular").css({"display":"flex"});
                $("#mensaje-error-celular").html("Campo obligatorio");
                $("#celular").addClass("form-control-error");
                isDatosPersonales = true;
                retorna = false;
            }else{
                $("#mensaje-error-celular").html("");
                $("#celular").removeClass("form-control-error");
            }

            if($("#direccion").val().trim() == ""){
                $("#mensaje-error-direccion").css({"display":"flex"});
                $("#mensaje-error-direccion").html("Campo obligatorio");
                $("#direccion").addClass("form-control-error");
                isDatosPersonales = true;
                retorna = false;
            }else{
                $("#mensaje-error-direccion").html("");
                $("#direccion").removeClass("form-control-error");
            }

            if($("#referencia").val().trim() == ""){
                $("#mensaje-error-referencia").css({"display":"flex"});
                $("#mensaje-error-referencia").html("Campo obligatorio");
                $("#referencia").addClass("form-control-error");
                isDatosPersonales = true;
                retorna = false;
            }else{
                $("#mensaje-error-referencia").html("");
                $("#referencia").removeClass("form-control-error");
            }

            if($("#empresa").val().trim() == ""){
                $("#mensaje-error-empresa").css({"display":"flex"});
                $("#mensaje-error-empresa").html("Campo obligatorio");
                $("#empresa").addClass("form-control-error");
                isDatosTrabajo = true;
                retorna = false;
            }else{
                $("#mensaje-error-empresa").html("");
                $("#empresa").removeClass("form-control-error");
            }

            if($("#fechaIngreso").val().trim() == ""){
                $("#mensaje-error-fechaing").css({"display":"flex"});
                $("#mensaje-error-fechaing").html("Campo obligatorio");
                $("#fechaIngreso").addClass("form-control-error");
                isDatosTrabajo = true;
                retorna = false;
            }else{
                $("#mensaje-error-fechaing").html("");
                $("#fechaIngreso").removeClass("form-control-error");
            }

            if($("#direccionTrabajo").val().trim() == ""){
                $("#mensaje-error-direcciontrabajo").css({"display":"flex"});
                $("#mensaje-error-direcciontrabajo").html("Campo obligatorio");
                $("#direccionTrabajo").addClass("form-control-error");
                isDatosTrabajo = true;
                retorna = false;
            }else{
                $("#mensaje-error-direcciontrabajo").html("");
                $("#direccionTrabajo").removeClass("form-control-error");
            }

            if($("#referenciaTrabajo").val().trim() == ""){
                $("#mensaje-error-referenciatrabajo").css({"display":"flex"});
                $("#mensaje-error-referenciatrabajo").html("Campo obligatorio");
                $("#referenciaTrabajo").addClass("form-control-error");
                isDatosTrabajo = true;
                retorna = false;
            }else{
                $("#mensaje-error-referenciatrabajo").html("");
                $("#referenciaTrabajo").removeClass("form-control-error");
            }

            return retorna
        }

        $(".btnGuardar").on("click", function (e) {
            $('#lightbox-loader').css({"visibility":"visible"});

            if(! isFormValido()){
                if(isDatosPersonales){
                    $("#alertDatosPersonales").css({"display":"flex"});
                }else{
                    $("#alertDatosPersonales").css({"display":"none"});
                }

                if(isDatosTrabajo){
                    $("#alertDatosTrabajo").css({"display":"flex"});
                }else{
                    $("#alertDatosTrabajo").css({"display":"none"});
                }

                if(isDatosDocumentos){
                    $("#alertDatosDocumentos").css({"display":"flex"});
                }else{
                    $("#alertDatosDocumentos").css({"display":"none"});
                }


                var hoy = new Date();
                var cumpleanos = new Date($("#fechaNacimiento").val().trim());
                console.log(hoy);
                console.log(cumpleanos);


                $('#lightbox-loader').css({"visibility":"hidden"});
                createToastError("Error", "Faltan campos obligatorios por ingresar");
                e.preventDefault();
            }
        })
    });
</script>
