<!-- Large modal -->
<div class="modal fade" id="modalCobrar" role="dialog" tabindex="-1" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle">Cobro de factura</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="form-row">
                    <div class="col-8">
                        <div class="form-row">
                            <div class="col-md-3 mb-3">
                                <div class="font-weight-bold">Fecha de pago:</div>
                                <div id="facturaFechaPago"></div>
                            </div>
                            <div class="col-md-9 mb-3">
                                <div class="font-weight-bold">Cliente:</div>
                                <div id="facturaCliente"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3 mb-3">
                                <div class="font-weight-bold">Identificación:</div>
                                <div id="facturaIdentificacion"></div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <div class="font-weight-bold">Correo:</div>
                                <div id="facturaCorreo"></div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="font-weight-bold">Motivo:</div>
                                <div id="facturaMotivo"></div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <div class="font-weight-bold">Observación:</div>
                                <div id="facturaObservacion"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="font-weight-bold" style="font-size: 40px; text-align: right" id="facturaTotalPago"></div>
                    </div>
                </div>


                <div class="form-row">
                    <div class="col-lg-5 m-auto">
                        <label for="valorCobrar" class="font-weight-bold" style="font-size: 20px">Valor a cobrar</label>

                        <div class="form-row">
                            <div class="col-1">
                                <div style="font-size: 32px; margin-top: -6px">$</div>
                            </div>

                            <div class="col-11">
                                <input class="form-control" name="valorCobrar" id="valorCobrar" type="text" style="font-size: 32px; text-align: center" required>
                                <label class="mensaje-error" id="mensaje-error-valor-factura"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary btnCobrar">Cobrar factura</button>
            </div>
        </div>
    </div>
</div>
