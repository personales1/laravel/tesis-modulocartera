<!-- Large modal -->
<div class="modal fade bd-cliente-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Clientes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="mt-3 mr-3 ml-3">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="search-wrapper active w-100">
                            <div class="input-holder w-100">
                                <input type="text" class="search-input" id="input-busqueda-client" placeholder="Búsqueda por todos los campos">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card-body table-responsive">
                        <table class="mb-0 table table-bordered my-table">
                            <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Identificación</th>
                                    <th>Correo</th>
                                    <th>Celular</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="myTableClient">
                                @foreach($clientes as $cliente)
                                    <tr class="rowCliente" data-id="{{ $cliente->id }}" data-dismiss="modal">
                                        <td class="nombresCliente">{{ $cliente->nombres }} {{ $cliente->apellidos }}</td>
                                        <td class="identificacionCliente">{{ $cliente->identificacion }}</td>
                                        <td>{{ $cliente->email }}</td>
                                        <td>{{ $cliente->celular }}</td>
                                        <td>
                                            <button class="btn-shadow btn btn-primary" type="button" data-toggle="tooltip" data-placement="bottom">
                                                <i class="fa fa-check"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
