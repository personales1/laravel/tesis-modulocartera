<div class="main-card card" style="margin-top: -12px">
    <div class="card-body">
        <h5 class="card-title">Filtrar por</h5>
        <form>
            @csrf
            <div class="form-row">
                <div class="col-6 col-sm-5">
                    Fecha inicio (día/mes/año)
                    <input name="fechaInicio" id="fechaInicio" type="text" class="form-control datepicker" readonly>
                </div>
                <div class="col-6 col-sm-5">
                    Fecha fin (día/mes/año)
                    <input name="fechaFin" id="fechaFin" type="text" class="form-control datepicker" readonly>
                </div>

                <button class="btn btn-alternate btnBuscar mt-2 mt-sm-4 col-12 col-sm-2" type="button">Buscar</button>
            </div>
        </form>
    </div>
</div>
