<div class="app-sidebar sidebar-shadow">
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <!--
                <li class="app-sidebar__heading">Dashboards</li>
                <li>
                    <a href="index.html" class="mm-active">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        Dashboard Example 1
                    </a>
                </li>
                -->

                <li class="app-sidebar__heading">Cliente</li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-server"></i>
                        Catálogos
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="/clientes" id="menu-cliente">
                                <i class="metismenu-icon"></i>
                                Cliente
                            </a>
                        </li>

                        @if(\Illuminate\Support\Facades\Auth::user()->id_rol == config('constant.rol.id.admin'))
                            <li>
                                <a href="/provincia" id="menu-provincia">
                                    <i class="metismenu-icon"></i>
                                    Provincia
                                </a>
                            </li>
                            <li>
                                <a href="/canton" id="menu-canton">
                                    <i class="metismenu-icon"></i>
                                    Cantón
                                </a>
                            </li>
                            <li>
                                <a href="/parroquia" id="menu-parroquia">
                                    <i class="metismenu-icon"></i>
                                    Parroquia
                                </a>
                            </li>
                            <li>
                                <a href="/sector" id="menu-sector">
                                    <i class="metismenu-icon"></i>
                                    Sector
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>








                <li class="app-sidebar__heading">Crédito</li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-server"></i>
                        Catálogos
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        @if(\Illuminate\Support\Facades\Auth::user()->id_rol == config('constant.rol.id.admin'))
                            <li>
                                <a href="/credito/motivo">
                                    <i class="metismenu-icon"></i>
                                    Motivos de crédito
                                </a>
                            </li>
                            <li>
                                <a href="/credito/dias">
                                    <i class="metismenu-icon"></i>
                                    Días de crédito
                                </a>
                            </li>
                        @endif
                        <li>
                            <a href="/producto">
                                <i class="metismenu-icon"></i>
                                Productos
                            </a>
                        </li>
                    </ul>
                </li>
                @if(\Illuminate\Support\Facades\Auth::user()->id_rol == config('constant.rol.id.admin'))
                    <li>
                        <a href="/credito/aprueba">
                            <i class="metismenu-icon pe-7s-like2"></i>
                            Aprobar crédito
                        </a>
                    </li>
                @endif
                <li>
                    <a href="/credito/otorgar">
                        <i class="metismenu-icon pe-7s-credit"></i>
                        Ventas a crédito
                    </a>
                </li>
                <li>
                    <a href="/ventas/cobros">
                        <i class="metismenu-icon pe-7s-cash"></i>
                        Pago de facturas
                    </a>
                </li>





                <li class="app-sidebar__heading">Reportes</li>
                <li>
                    <a href="/reporte/historial/crediticio">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        Historial crediticio
                    </a>
                </li>
                <li>
                    <a href="/reporte/ventas">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        Ventas
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-graph"></i>
                        Gráficos
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="/reporte/grafico/calidad-pagos">
                                <i class="metismenu-icon"></i>
                                Calidad crediticia por pagos
                            </a>
                        </li>
                        <li>
                            <a href="/reporte/grafico/ventas-pagos">
                                <i class="metismenu-icon"></i>
                                Ventas - Pagos
                            </a>
                        </li>
                    </ul>
                </li>





                @if(\Illuminate\Support\Facades\Auth::user()->id_rol == config('constant.rol.id.admin'))
                    <li class="app-sidebar__heading">Parámetros</li>
                    <!--
                    <li>
                        <a href="">
                            <i class="metismenu-icon pe-7s-settings"></i>
                            Parámetros de empresa
                        </a>
                    </li>
                    -->
                    <li>
                        <a href="/usuario">
                            <i class="metismenu-icon pe-7s-user"></i>
                            Usuarios
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>
