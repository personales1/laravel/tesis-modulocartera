<div class="row">
    <div class="col-md-6 mb-3">
        <div class="search-wrapper active w-100">
            <div class="input-holder w-100">
                <input type="text" class="search-input" id="input-busqueda" placeholder="Búsqueda por todos los campos">
            </div>
        </div>
    </div>

    <div class="col-md-6 mb-3">
        <div class="float-right">
            <a class="btnExportReport" id="btnExportExcel"><img src="{{asset('assets/images/iconexcel.png')}}" width="40"></a>
            <a class="btnExportReport" id="btnExportPdf"><img src="{{asset('assets/images/iconpdf.png')}}" width="40"></a>
        </div>
    </div>
</div>
