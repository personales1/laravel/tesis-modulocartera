<!doctype html>
<html lang="en">
<head>
    <link  href="{{asset('style.css?v=0')}}" rel="stylesheet">
    <link  href="{{asset('mystylecustom.css?v=0')}}" rel="stylesheet">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        @include('include.mail-header')
        @include('include.mail-title')

        <div style="color: black" class="text-center fsize-2 mt-3">
            <span class="font-weight-bold">{{ strtoupper($usuario->userProfile->nombres) }} {{ strtoupper($usuario->userProfile->apellidos) }}</span> su cuenta de usuario ha sido creada y se le ha generado la siguiente contraseña temporal: <b>{{ $usuario->password }}</b>.
            <br>Recuerde que esta es solo una contraseña temporal, cámbiela al ingresar al sistema.
        </div>
    </div>
</body>
</html>
