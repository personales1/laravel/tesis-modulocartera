<!doctype html>
<html lang="en">
<head>
    <link  href="{{asset('style.css?v=0')}}" rel="stylesheet">
    <link  href="{{asset('mystylecustom.css?v=0')}}" rel="stylesheet">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        {{--
        @include('include.mail-header')
        @include('include.mail-title')
        --}}

        <div style="color: black" class="text-center fsize-2 mt-3">
            {{ strtoupper($cliente->nombres) }} {{ strtoupper($cliente->apellidos) }}, <b>SEICARSE</b> le da la bienvenida por formar parte de nuestra selecta cartera de clientes.
            <br>Nuestro departamento de ventas analizará sus datos ingresados y se le notificará cuando se le haya dado el visto bueno para que pueda realizar compras a crédito con nosotros.
        </div>
    </div>
</body>
</html>
