<!doctype html>
<html lang="en">
<head>
    <link  href="{{asset('style.css?v=0')}}" rel="stylesheet">
    <link  href="{{asset('mystylecustom.css?v=0')}}" rel="stylesheet">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        {{--
        @include('include.mail-header')
        @include('include.mail-title')
        --}}

        <div style="color: black" class="text-center fsize-2 mt-3">
            Estimado cliente {{ strtoupper($cliente->nombres) }} {{ strtoupper($cliente->apellidos) }}, usted ha realizado una compra a crédito con <b>SEICARSE</b>.
            <br>Los datos de la venta son:
        </div>
        <br>

        <div class="table-responsive">
            <table style="width:100%; border: 1px solid black; border-collapse: collapse;">
                <thead style="text-align: center;">
                    <tr>
                        <th style="border: 1px solid black;">Producto</th>
                        <th style="border: 1px solid black;">Cantidad</th>
                        <th style="border: 1px solid black;">Precio</th>
                        <th style="border: 1px solid black;">Total</th>
                    </tr>
                </thead>
                <tbody style="text-align: center;">
                @foreach($salesDetail as $detalleVenta)
                    <tr>
                        <td style="border: 1px solid black;">{{ $detalleVenta->product->nombre }}</td>
                        <td style="border: 1px solid black;">{{ $detalleVenta->cantidad }}</td>
                        <td style="border: 1px solid black;">${{ $detalleVenta->product->precio }}</td>
                        <td style="border: 1px solid black;">${{ $detalleVenta->total }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <br>

        <div style="font-size: 16px">
            <b>Total venta: ${{ $registroPago->total }}</b>
        </div>
    </div>
</body>
</html>
