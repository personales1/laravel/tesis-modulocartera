<!doctype html>
<html lang="en">
<head>
    <link  href="{{asset('style.css?v=0')}}" rel="stylesheet">
    <link  href="{{asset('mystylecustom.css?v=0')}}" rel="stylesheet">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        {{--
        @include('include.mail-header')
        @include('include.mail-title')
        --}}

        <div style="color: black" class="text-center fsize-2 mt-3">
            Estimado cliente {{ strtoupper($registroPago->client->nombres) }} {{ strtoupper($registroPago->client->apellidos) }}, se ha registrado un pago de ${{ $registroPago->valor_pago }} correspondiente a la factura #{{ $registroPago->salesHeader->num_factura }}.
            <br>
            Del total de su factura ${{ $totalPago->total }}, <b>saldo pendiente
                de ${{ $registroPago->saldo }}</b>
            <br><br>
            SEICARSE le agradece su pago.
        </div>
    </div>
</body>
</html>
