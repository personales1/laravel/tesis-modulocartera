<!doctype html>
<html lang="en">
<head>
    <link  href="{{asset('style.css?v=0')}}" rel="stylesheet">
    <link  href="{{asset('mystylecustom.css?v=0')}}" rel="stylesheet">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        {{--
        @include('include.mail-header')
        @include('include.mail-title')
        --}}

        <div style="color: black" class="text-center fsize-2 mt-3">
            {{ strtoupper($cliente->nombres) }} {{ strtoupper($cliente->apellidos) }}, <b>SEICARSE</b> le ha aprobado el crédito para realizar compras.
            <br>Desde este momento puede acercarse a realizar sus compras a crédito.
        </div>
    </div>
</body>
</html>
