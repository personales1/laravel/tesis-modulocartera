@extends('app')
@section('title', 'Gráficos de ventas y pagos')

<?php
$_GET['page'] = "graficos";
$_GET['page-title'] = "Análisis de ventas y pagos";
$_GET['page-description'] = "Se muestra gráficos del total de las ventas y de los pagos, agrupados por mes.";
?>

<script>
    let listVentas = [];
    let listPagos = [];
    let listMes = [];
</script>

@foreach(json_decode($ventas_pagos) as $vp)
    <script>
        listVentas.push('{{ $vp->ventas }}');
        listPagos.push('{{ $vp->pagos }}');
        listMes.push('{{ $vp->mes_nombre }}');
    </script>
@endforeach

@section('content-body')
    @include('include.filtro-fecha')

    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav" id="nav-tabs">
        <li class="nav-item">
            <a role="tab" class="nav-link show active" id="tab-0" data-toggle="tab" onclick="tab1()" aria-selected="true">
                <span>Gráfico de barras</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link show" id="tab-1" data-toggle="tab" onclick="tab2()" aria-selected="false">
                <span>Gráfico de líneas</span>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="tab-content-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="card-header">
                            <div class="btn-actions-pane-left">
                                <div class="nav">
                                    <a data-toggle="tab" id="tabVertical" onclick="tabVertical()" class="btn-pill btn-wide active btn btn-outline-alternate btn-sm">Vertical</a>
                                    <a data-toggle="tab" id="tabHorizontal" onclick="tabHorizontal()" class="btn-pill btn-wide mr-1 ml-1  btn btn-outline-alternate btn-sm">Horizontal</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div id="tab1-vertical">
                                    <canvas id="graphBarChart"></canvas>
                                </div>
                                <div style="display: none" id="tab1-horizontal">
                                    <canvas id="graphBarHorizontalChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div style="display: none" id="tab-content-1">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <div class="tab-content">
                                <div id="tab1-vertical">
                                    <canvas id="graphLineChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('assets/scripts/main.js') }}"></script>
    <script>
        $("#fechaInicio").val('{{ $dateInicio->isoFormat('DD/MM/YYYY') }}');
        $("#fechaFin").val('{{ $dateFin->isoFormat('DD/MM/YYYY') }}');

        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
        });

        graphBarChart();

        function tab1(){
            if(listVentas.length > 0 || listPagos.length > 0){
                $("#tab-content-0").css({"display":"block"});
                $("#tab-content-1").css({"display":"none"});

                $("#tabVertical").addClass("active");
                $("#tabHorizontal").removeClass("active");
                tabVertical();
            }
            else{
                $("#tab-content-0").css({"display":"none"});
            }
        }

        function tab2(){
            if(listVentas.length > 0 || listPagos.length > 0){
                $("#tab-content-0").css({"display":"none"});
                $("#tab-content-1").css({"display":"block"});
                graphLineChart();
            }
            else{
                $("#tab-content-1").css({"display":"none"});
            }
        }

        function tabVertical(){
            $("#tab1-vertical").css({"display":"block"});
            $("#tab1-horizontal").css({"display":"none"});
            graphBarChart();
        }

        function tabHorizontal(){
            $("#tab1-vertical").css({"display":"none"});
            $("#tab1-horizontal").css({"display":"block"});
            graphBarHorizontalChart();
        }

        function graphBarChart(){
            let ctx = document.getElementById('graphBarChart').getContext('2d');

            var data = {
                datasets: [
                    {
                        data: listVentas,
                        backgroundColor: '#FF8000',
                        label: 'Ingresos ($) en ventas',
                    },
                    {
                        data: listPagos,
                        backgroundColor: '#008AAA',
                        label: 'Ingresos ($) en pagos',
                    }
                ],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: listMes,
            };

            var opciones = {
                responsive: true,
                legend: {
                    display: true
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Ingresos ($) por ventas/pagos',
                            fontColor: "#000000",
                            fontSize: "20"
                        },
                        ticks: {
                            beginAtZero: true,
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Meses',
                            fontColor: "#000000",
                            fontSize: "20"
                        }
                    }]
                },
                animation: {
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font= "15px Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = '#000000';

                        this.data.datasets.forEach(function (dataset) {
                            var meta = dataset._meta[Object.keys(dataset._meta)[0]];

                            meta.data.forEach(function(bar, index) {
                                var data = "$ " + dataset.data[index];
                                ctx.fillText(data, bar._model.x, bar._model.y - 2);
                            });
                        });
                    }
                }
            };

            var myBarChart = new Chart(ctx, {
                type: 'bar',
                data: data,
                options: opciones
            });

            scrollToGraph()
        }

        function graphBarHorizontalChart(){
            let ctx = document.getElementById('graphBarHorizontalChart').getContext('2d');

            var data = {
                datasets: [
                    {
                        data: listVentas,
                        backgroundColor: '#FF8000',
                        label: 'Ingresos ($) en ventas',
                    },
                    {
                        data: listPagos,
                        backgroundColor: '#008AAA',
                        label: 'Ingresos ($) en pagos',
                    }
                ],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: listMes,
            };

            var opciones = {
                responsive: true,
                legend: {
                    display: true
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Ventas - Pagos ($)',
                            fontColor: "#000000",
                            fontSize: "20"
                        },
                        ticks: {
                            beginAtZero: true,
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Meses',
                            fontColor: "#000000",
                            fontSize: "20"
                        }
                    }]
                },
                animation: {
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font= "13px Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = '#000000';

                        this.data.datasets.forEach(function (dataset) {
                            var meta = dataset._meta[Object.keys(dataset._meta)[0]];

                            meta.data.forEach(function(bar, index) {
                                var data = "$ " + dataset.data[index];
                                ctx.fillText(data, bar._model.x + 30, bar._model.y);
                            });
                        });
                    }
                }
            };

            var myBarChart = new Chart(ctx, {
                type: 'horizontalBar',
                data: data,
                options: opciones
            });

            scrollToGraph()
        }

        function graphLineChart(){
            let ctx = document.getElementById('graphLineChart').getContext('2d');

            var data = {
                datasets: [
                    {
                        data: listVentas,
                        backgroundColor: '#FF8000',
                        borderColor: '#FF8000',
                        pointBorderWidth: "5",
                        label: 'Ventas realizadas',
                        fill: false,
                    },
                    {
                        data: listPagos,
                        backgroundColor: '#008AAA',
                        borderColor: '#008AAA',
                        pointBorderWidth: "5",
                        label: 'Pagos registrados',
                        fill: false,
                    }
                ],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: listMes,
            };

            var opciones = {
                responsive: true,
                legend: {
                    display: true
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Ventas - Pagos ($)',
                            fontColor: "#000000",
                            fontSize: "20"
                        },
                        ticks: {
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Meses',
                            fontColor: "#000000",
                            fontSize: "20"
                        }
                    }]
                },
            };

            var myBarChart = new Chart(ctx, {
                type: 'line',
                data: data,
                options: opciones
            });

            scrollToGraph()
        }

        $(".btnBuscar").on("click", function (e) {
            $('#lightbox-loader').css({"visibility":"visible"});

            $.ajax({
                url:"{{ route('grafico.ventas.pagos.porfecha') }}",
                type: "POST",
                data: {
                    _token: $("input[name='_token']").val(),
                    fechaInicio: $('#fechaInicio').val(),
                    fechaFin: $('#fechaFin').val(),
                },
                success: function (data) {
                    let myJsonResponse = $.parseJSON(data); //convierte el response string en json

                    listVentas = [];
                    listPagos = [];
                    listMes = [];

                    console.log(myJsonResponse);
                    console.log(myJsonResponse.ventas_pagos.length);
                    console.log(typeof myJsonResponse.ventas_pagos);

                    for(var i=0; i < myJsonResponse.ventas_pagos.length; i++){
                        listVentas.push(myJsonResponse.ventas_pagos[i].ventas);
                        listPagos.push(myJsonResponse.ventas_pagos[i].pagos);
                        listMes.push(myJsonResponse.ventas_pagos[i].mes_nombre);
                    }

                    console.log(listVentas);
                    console.log(listPagos);
                    console.log(listMes);

                    $("#tab-content-1").css({"display":"none"});
                    $("#tab-0").addClass("active");
                    $("#tab-1").removeClass("active");
                    tab1();

                    $('#lightbox-loader').css({"visibility":"hidden"});
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    alert("Hubo un error.");
                    $('#lightbox-loader').css({"visibility":"hidden"});
                }
            })
        });

        function scrollToGraph(){
            $('html, body').animate({
                scrollTop: $("#nav-tabs").offset().top
            }, 1000);
        }
    </script>
@endsection
