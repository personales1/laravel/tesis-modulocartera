@extends('app')
@section('title', 'Gráficos calidad crediticia')

<?php
$_GET['page'] = "graficos";
$_GET['page-title'] = "Análisis de calidad crediticia por pagos";
$_GET['page-description'] = "Se muestra gráficos del análisis de la calidad crediticia que ha tenido los pagos realizados por los clientes.";
?>

<script>
    let listDatos = [];
    let listLabels = [];
    let listColors = [];
    let listTotalPagos = [];
</script>

@foreach($pagosConsolidadosPorCalificacion as $rowPagos)
    <script>
        listDatos.push('{{ $rowPagos->porcentaje }}');
        listLabels.push('{{ $rowPagos->estadoPago->ponderacion_texto }}');
        listColors.push('{{ $rowPagos->estadoPago->ponderacion_color_background }}');
        listTotalPagos.push('{{ $rowPagos->total }}');
    </script>
@endforeach

@section('content-body')
    @include('include.filtro-fecha')

    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav" id="nav-tabs">
        <li class="nav-item">
            <a role="tab" class="nav-link show active" id="tab-0" data-toggle="tab" onclick="tab1()" aria-selected="true">
                <span>Porcentajes</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link show" id="tab-1" data-toggle="tab" onclick="tab2()" aria-selected="false">
                <span>Totales</span>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="tab-content-0">
            <div class="row">
                <div class="col-md-6">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <!--<h5 class="card-title">Pie Chart</h5>-->
                            <canvas id="graphPieChart"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <!--<h5 class="card-title">Doughnut</h5>-->
                            <canvas id="graphDoughnutChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div style="display: none;" id="tab-content-1">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="card-header">
                            <h5><b>{{ $totalNumeroDePagos }} pagos registrados</b></h5>

                            <div class="btn-actions-pane-right">
                                <div class="nav">
                                    <a data-toggle="tab" id="tabVertical" onclick="tab1Vertical()" class="btn-pill btn-wide active btn btn-outline-alternate btn-sm">Vertical</a>
                                    <a data-toggle="tab" id="tabHorizontal" onclick="tab1Horizontal()" class="btn-pill btn-wide mr-1 ml-1 btn btn-outline-alternate btn-sm">Horizontal</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div id="tab1-vertical">
                                    <canvas id="graphBarChart"></canvas>
                                </div>
                                <div style="display: none;" id="tab1-horizontal">
                                    <canvas id="graphBarHorizontalChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="{{ asset('assets/scripts/main.js') }}"></script>
    <script>
        $("#fechaInicio").val('{{ $dateInicio->isoFormat('DD/MM/YYYY') }}');
        $("#fechaFin").val('{{ $dateFin->isoFormat('DD/MM/YYYY') }}');

        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
        });

        graphPieChart();
        graphDoughnout();

        function tab1(){
            if(listDatos.length > 0){
                $("#tab-content-0").css({"display":"block"});
                $("#tab-content-1").css({"display":"none"});

                graphPieChart();
                graphDoughnout();
            }
            else{
                $("#tab-content-0").css({"display":"none"});
            }
        }

        function tab2(){
            if(listTotalPagos.length > 0){
                $("#tab-content-0").css({"display":"none"});
                $("#tab-content-1").css({"display":"block"});

                $("#tabVertical").addClass("active");
                $("#tabHorizontal").removeClass("active");
                tab1Vertical()
            }
            else{
                $("#tab-content-1").css({"display":"none"});
            }
        }

        function tab1Vertical(){
            $("#tab1-vertical").css({"display":"block"});
            $("#tab1-horizontal").css({"display":"none"});
            graphBarChart();
        }

        function tab1Horizontal(){
            $("#tab1-vertical").css({"display":"none"});
            $("#tab1-horizontal").css({"display":"block"});
            graphBarHorizontalChart();
        }

        function graphPieChart(){
            let ctx = document.getElementById('graphPieChart').getContext('2d');

            let data = {
                datasets: [{
                    data: listDatos,
                    backgroundColor: listColors
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: listLabels,
            };

            let opciones = {
                tooltips: {
                    callbacks: {
                        // this callback is used to create the tooltip label
                        label: function(tooltipItem, data) {
                            // get the data label and data value to display
                            // convert the data value to local string so it uses a comma seperated number
                            let dataLabel = data.labels[tooltipItem.index];
                            let value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString() + "%";

                            // return the text to display on the tooltip
                            return dataLabel + value;
                        }
                    }
                },
                animation: {
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font= "10px Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = '#fff';

                        this.data.datasets.forEach(function (dataset) {
                            for (let i = 0; i < dataset.data.length; i++) {
                                let model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle)/2;

                                let x = mid_radius * Math.cos(mid_angle);
                                let y = mid_radius * Math.sin(mid_angle);

                                let val = dataset.data[i] + "%";

                                if(dataset.data[i] > 20) {
                                    ctx.fillText(val, model.x + x, model.y + y);
                                }
                            }
                        });
                    }
                }
            };

            let myPieChart = new Chart(ctx, {
                type: 'pie',
                data: data,
                options: opciones
            });

            scrollToGraph()
        }

        function graphDoughnout(){
            let ctx = document.getElementById('graphDoughnutChart').getContext('2d');

            var data = {
                datasets: [{
                    data: listDatos,
                    backgroundColor: listColors,
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: listLabels,
            };

            let opciones = {
                tooltips: {
                    callbacks: {
                        // this callback is used to create the tooltip label
                        label: function(tooltipItem, data) {
                            // get the data label and data value to display
                            // convert the data value to local string so it uses a comma seperated number
                            let dataLabel = data.labels[tooltipItem.index];
                            let value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString() + "%";

                            // return the text to display on the tooltip
                            return dataLabel + value;
                        }
                    }
                },
                animation: {
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font= "10px Arial";
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = '#fff';

                        this.data.datasets.forEach(function (dataset) {
                            for (let i = 0; i < dataset.data.length; i++) {
                                let model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle)/2;

                                let x = mid_radius * Math.cos(mid_angle);
                                let y = mid_radius * Math.sin(mid_angle);

                                let val = dataset.data[i] + "%";

                                if(dataset.data[i] > 10) {
                                    ctx.fillText(val, model.x + x, model.y + y);
                                }
                            }
                        });
                    }
                }
            };

            let myDoughnoutChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: opciones
            });

            scrollToGraph()
        }

        function graphBarChart(){
            let ctx = document.getElementById('graphBarChart').getContext('2d');

            var data = {
                datasets: [{
                    data: listTotalPagos,
                    backgroundColor: listColors,
                    label: listLabels,
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: listLabels,
            };

            var opciones = {
                responsive: true,
                legend: {
                    display: true
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Total de pagos',
                            fontColor: "#000000",
                            fontSize: "20"
                        },
                        ticks: {
                            beginAtZero: true,
                            stepSize: 3
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Calidad crediticia',
                            fontColor: "#000000",
                            fontSize: "20"
                        }
                    }]
                }
            };

            var myBarChart = new Chart(ctx, {
                type: 'bar',
                data: data,
                options: opciones
            });

            scrollToGraph()
        }

        function graphBarHorizontalChart(){
            let ctx = document.getElementById('graphBarHorizontalChart').getContext('2d');

            var data = {
                datasets: [{
                    data: listTotalPagos,
                    backgroundColor: listColors,
                    label: listLabels,
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: listLabels,
            };

            var opciones = {
                responsive: true,
                legend: {
                    display: true
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Total de pagos',
                            fontColor: "#000000",
                            fontSize: "20"
                        },
                        ticks: {
                            beginAtZero: true,
                            stepSize: 3
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Calidad crediticia',
                            fontColor: "#000000",
                            fontSize: "20"
                        }
                    }]
                }
            };

            var myBarChart = new Chart(ctx, {
                type: 'horizontalBar',
                data: data,
                options: opciones
            });

            scrollToGraph()
        }

        $(".btnBuscar").on("click", function (e) {
            $('#lightbox-loader').css({"visibility":"visible"});

            $.ajax({
                url:"{{ route('grafico.calidad.crediticia.porfecha') }}",
                type: "POST",
                data: {
                    _token: $("input[name='_token']").val(),
                    fechaInicio: $('#fechaInicio').val(),
                    fechaFin: $('#fechaFin').val(),
                },
                success: function (data) {
                    let myJsonResponse = $.parseJSON(data); //convierte el response string en json

                    listDatos = [];
                    listLabels = [];
                    listColors = [];
                    listTotalPagos = [];

                    var ponderacionTexto = "";
                    var ponderacionBackground = "";
                    for(var i=0; i<myJsonResponse.pagoConsolidadoPorCalificacion.length; i++){
                        listDatos.push(myJsonResponse.pagoConsolidadoPorCalificacion[i].porcentaje.toString());
                        listTotalPagos.push(myJsonResponse.pagoConsolidadoPorCalificacion[i].total);

                        ponderacionTexto = "";
                        ponderacionBackground = "";
                        for(var j=0; j<myJsonResponse.estadosPagos.length; j++ ){
                            var estadoPago = myJsonResponse.estadosPagos[j].id.toString();

                            if(estadoPago === myJsonResponse.pagoConsolidadoPorCalificacion[i].estado_pago_id.toString()){
                                ponderacionTexto = myJsonResponse.estadosPagos[j].ponderacion_texto;
                                ponderacionBackground = myJsonResponse.estadosPagos[j].ponderacion_color_background;
                            }
                        }

                        listLabels.push(ponderacionTexto);
                        listColors.push(ponderacionBackground);
                    }

                    if(listTotalPagos.length < 1){
                        $("#tab-content-1").css({"display":"none"});
                    }

                    $("#tab-0").addClass("active");
                    $("#tab-1").removeClass("active");
                    tab1();

                    $('#lightbox-loader').css({"visibility":"hidden"});
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    alert("Hubo un error.");
                    $('#lightbox-loader').css({"visibility":"hidden"});
                }
            })
        });

        function scrollToGraph(){
            $('html, body').animate({
                scrollTop: $("#nav-tabs").offset().top
            }, 1000);
        }
    </script>
@endsection
