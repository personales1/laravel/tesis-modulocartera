@php
    use Carbon\Carbon;
    $fechaCurrent = Carbon::now()->startOfDay();
@endphp

<div class="card-body table-responsive">
    <h2 style="text-transform: uppercase; color: black; font-weight: bold;">Historial crediticio</h2>
    <h3>Reporte con fecha de {{ $fechaCurrent->format('d-m-Y') }}</h3>

    <table style="width: 100%; border: 1px solid #e9ecef; border-collapse: collapse;">
        <thead style="text-align: center;">
        <tr>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; font-size: 13px;">Cliente</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; font-size: 13px;">Identificación</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; font-size: 13px;">Email</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; font-size: 13px;">Calidad crediticia</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; font-size: 13px;">Calificación crediticia</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; font-size: 13px;">Descripción de la calificación</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; font-size: 13px;">Pagos pendiente</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pagos as $pago)
            @php $ponderacionSuma = 0 @endphp

            @foreach($pago as $p)
                @php $ponderacionSuma += $p->estadoPago->ponderacion @endphp
            @endforeach

            @php
                $ponderacionPromedio = $ponderacionSuma/count($pago);
                $ponderacionPromedio = round($ponderacionPromedio);

                $estadoPago = \App\EstadoPago::where('ponderacion', $ponderacionPromedio)->first();
                $numeroPagosPendientes = \App\RegistroPago::where([['client_id', $pago[0]->client_id], ['is_pagado', 0]])->count();
            @endphp
            <tr style="background-color: {{ $estadoPago->ponderacion_color_background }}; color: {{ $estadoPago->ponderacion_color_texto }}">
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $pago[0]->client->nombres }} {{ $pago[0]->client->apellidos }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $pago[0]->client->identificacion }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $pago[0]->client->email }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $estadoPago->ponderacion_texto }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $estadoPago->ponderacion_calificacion }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $estadoPago->descripcion }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $numeroPagosPendientes }} pagos</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
