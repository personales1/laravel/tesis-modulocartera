@extends('app')
@section('title', 'Creación de usuario')

<?php
$_GET['page'] = "usuario";
$_GET['page-title'] = "Usuario";
if(isset($users)){
    $_GET['page-description'] = "Actualizar datos del usuario ". $users->userProfile->nombres . " " . $users->userProfile->apellidos;

}else{
    $_GET['page-description'] = "Registro de nuevos usuarios.";
}
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de usuario</h5>
            {{--<form id="myform" method="POST" action="{{ route('usuario.create', ['id' => $users->id ?? '']) }}" class="needs-validation" novalidate>--}}
            <form id="myform" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                        <label for="cantonName">* Nombres</label>
                        <input name="name" id="operadorNombre" type="text" class="form-control" placeholder="Nombres" value="{{ $users->userProfile->nombres ?? "" }}" required>
                        <label class="mensaje-error d-none" id="mensaje-error-name"></label>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="operadorApellido">* Apellidos</label>
                        <input name="apellido" id="operadorApellido" type="text" class="form-control" placeholder="Apellidos" value="{{ $users->userProfile->apellidos ?? "" }}" required>
                        <label class="mensaje-error d-none" id="mensaje-error-apellido"></label>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="rolSelect">* Rol</label>
                        {{ Form::select('rol', $rol, $users->id_rol ?? null, ['class' => 'form-control', 'id' => 'rolSelect']) }}
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-5 mb-3">
                        <label for="operadorCorreo">* Correo (este será su usuario)</label>
                        <input name="correo" id="operadorCorreo" type="text" class="form-control" placeholder="Correo electrónico" value="{{ $users->email ?? old('correo') }}" required>
                        <label class="mensaje-error d-none" id="mensaje-error-correo"></label>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="operadorIdentificacion">* Identificación</label>
                        <input name="identificación" id="operadorIdentificacion" type="text" class="form-control" placeholder="Identificación" value="{{ $users->userProfile->identificacion ?? old('identificación') }}" maxlength="10" required>
                        <label class="mensaje-error d-none" id="mensaje-error-identificacion"></label>
                    </div>
                    <div class="col-md-2 mb-3">
                        <label for="operadorTelefono">* Teléfono</label>
                        <input name="teléfono" id="operadorTelefono" type="text" class="form-control" placeholder="Teléfono" value="{{ $users->userProfile->telefono ?? old('teléfono') }}" maxlength="10" required>
                        <label class="mensaje-error d-none" id="mensaje-error-telefono"></label>
                    </div>

                    @if(! isset($users->password))
                        <div class="col-md-3 mb-3">
                            <label for="operadorPassword">* Contraseña</label>
                            <input name="password" id="operadorPassword" type="password" class="form-control" placeholder="Contraseña" value="{{ $users->password ?? old('password') }}" required>
                            <label class="mensaje-error d-none" id="mensaje-error-password"></label>
                        </div>
                    @endif
                </div>

                <button class="btn btn-primary btnGuardar" type="button">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var regexpSoloLetra = /^[a-zA-Z áéíóúÁÉÍÓÚñÑ]+$/;
            var regexpSoloNumero = /^[0-9]+$/;
            var regexpEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            function validaCedula(cedula){
                var cad = cedula;
                var total = 0;
                var longitud = cad.length;
                var longcheck = longitud - 1;

                if(longitud == 10){
                    for(i = 0; i < longcheck; i++){
                        if (i%2 === 0) {
                            var aux = cad.charAt(i) * 2;
                            if (aux > 9) aux -= 9;
                            total += aux;
                        } else {
                            total += parseInt(cad.charAt(i));
                        }
                    }

                    total = total % 10 ? 10 - total % 10 : 0;

                    if (cad.charAt(longitud-1) == total) {
                        console.log('la cedula:' + cedula + ' es correcta');
                        return true;
                    }else{
                        console.log('la cedula:' + cedula + ' es incorrecta');
                        return false;
                    }
                }else{
                    return false;
                }
            }

            function validaForm(){
                let retorna = true;

                if($("#operadorNombre").val().trim() === ""){
                    $("#mensaje-error-name").html("Ingrese al menos un nombre");
                    $("#mensaje-error-name").removeClass("d-none");
                    $("#operadorNombre").addClass("form-control-error");
                    retorna = false;
                }else{
                    if($("#operadorNombre").val().trim().length < 3){
                        $("#mensaje-error-name").html("Nombre tiene pocos caracteres");
                        $("#mensaje-error-name").removeClass("d-none");
                        $("#operadorNombre").addClass("form-control-error");
                        retorna = false;
                    }else{
                        $("#mensaje-error-name").html("");
                        $("#mensaje-error-name").addClass("d-none");
                        $("#operadorNombre").removeClass("form-control-error");
                    }
                }

                if($("#operadorApellido").val().trim() === ""){
                    $("#mensaje-error-apellido").html("Ingrese al menos un apellido");
                    $("#mensaje-error-apellido").removeClass("d-none");
                    $("#operadorApellido").addClass("form-control-error");
                    retorna = false;
                }else{
                    if($("#operadorApellido").val().trim().length < 3){
                        $("#mensaje-error-apellido").html("Apellido tiene pocos caracteres");
                        $("#mensaje-error-apellido").removeClass("d-none");
                        $("#operadorApellido").addClass("form-control-error");
                        retorna = false;
                    }else{
                        $("#mensaje-error-apellido").html("");
                        $("#mensaje-error-apellido").addClass("d-none");
                        $("#operadorApellido").removeClass("form-control-error");
                    }
                }

                if($("#operadorCorreo").val().trim() === ""){
                    $("#mensaje-error-correo").html("Ingrese un correo electrónico");
                    $("#mensaje-error-correo").removeClass("d-none");
                    $("#operadorCorreo").addClass("form-control-error");
                    retorna = false;
                }else{
                    if(! regexpEmail.test($("#operadorCorreo").val())){
                        $("#mensaje-error-correo").html("La dirección de correo no tiene un formato válido");
                        $("#mensaje-error-correo").removeClass("d-none");
                        $("#operadorCorreo").addClass("form-control-error");
                        retorna = false;
                    }else{
                        $("#mensaje-error-correo").html("");
                        $("#mensaje-error-correo").addClass("d-none");
                        $("#operadorCorreo").removeClass("form-control-error");
                    }
                }

                if($("#operadorIdentificacion").val().trim() === ""){
                    $("#mensaje-error-identificacion").html("Ingrese el número de identificación");
                    $("#mensaje-error-identificacion").removeClass("d-none");
                    $("#operadorIdentificacion").addClass("form-control-error");
                    retorna = false;
                }else{
                    if($("#operadorIdentificacion").val().trim().length < 10){
                        $("#mensaje-error-identificacion").html("Identificación debe tener mínimo 10 caracteres");
                        $("#mensaje-error-identificacion").removeClass("d-none");
                        $("#operadorIdentificacion").addClass("form-control-error");
                        retorna = false;
                    }else if(! validaCedula($("#operadorIdentificacion").val().trim())){
                        $("#mensaje-error-identificacion").html("El número de identificación es inválido");
                        $("#mensaje-error-identificacion").removeClass("d-none");
                        $("#operadorIdentificacion").addClass("form-control-error");
                        retorna = false;
                    }else{
                        $("#mensaje-error-identificacion").html("");
                        $("#mensaje-error-identificacion").addClass("d-none");
                        $("#operadorIdentificacion").removeClass("form-control-error");
                    }
                }

                if($("#operadorTelefono").val().trim() === ""){
                    $("#mensaje-error-telefono").html("Ingrese el número de teléfono");
                    $("#mensaje-error-telefono").removeClass("d-none");
                    $("#operadorTelefono").addClass("form-control-error");
                    retorna = false;
                }else{
                    if($("#operadorTelefono").val().trim().length < 7){
                        $("#mensaje-error-telefono").html("Teléfono debe tener mínimo 7 caracteres");
                        $("#mensaje-error-telefono").removeClass("d-none");
                        $("#operadorTelefono").addClass("form-control-error");
                        retorna = false;
                    }else{
                        $("#mensaje-error-telefono").html("");
                        $("#mensaje-error-telefono").addClass("d-none");
                        $("#operadorTelefono").removeClass("form-control-error");
                    }
                }

                if($("#operadorPassword").length > 0){ //Valida si el elemento html existe
                    if($("#operadorPassword").val().trim() === ""){
                        $("#mensaje-error-password").html("Ingrese la contraseña");
                        $("#mensaje-error-password").removeClass("d-none");
                        $("#operadorPassword").addClass("form-control-error");
                        retorna = false;
                    }else{
                        if($("#operadorPassword").val().trim().length < 5){
                            $("#mensaje-error-password").html("Contraseña debe tener mínimo 5 caracteres");
                            $("#mensaje-error-password").removeClass("d-none");
                            $("#operadorPassword").addClass("form-control-error");
                            retorna = false;
                        }else{
                            $("#mensaje-error-password").html("");
                            $("#mensaje-error-password").addClass("d-none");
                            $("#operadorPassword").removeClass("form-control-error");
                        }
                    }
                }

                return retorna;
            }

            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});

                if(validaForm()){
                    let pass = '';
                    if($("#operadorPassword").length > 0){
                        pass = $("#operadorPassword").val().trim();
                    }

                    $.ajax({
                        url:"{{ route('usuario.create', ['id' => $users->id ?? '']) }}",
                        type: 'POST',
                        data: {
                            _token: $("input[name='_token']").val(),
                            name: $("#operadorNombre").val().trim(),
                            apellido: $("#operadorApellido").val().trim(),
                            rol: $("#rolSelect").val(),
                            correo: $("#operadorCorreo").val().trim(),
                            identificación: $("#operadorIdentificacion").val().trim(),
                            teléfono: $("#operadorTelefono").val().trim(),
                            password: pass
                        },
                        success: function(data) {
                            let myJsonResponse = $.parseJSON(data); //convierte el response string en json

                            if(myJsonResponse.estado >= 400){
                                Swal.fire('¡Error!', myJsonResponse.data.mensaje, 'error');
                            }
                            else{
                                Swal.fire('¡Éxito!', myJsonResponse.data.mensaje, 'success');

                                if($("#operadorPassword").length > 0){
                                    showToastCustom("", myJsonResponse.data.mensaje);
                                    location.href = '/usuario'
                                }
                            }

                            $('#lightbox-loader').css({"visibility":"hidden"});
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                            alert("Hubo un error.");
                            $('#lightbox-loader').css({"visibility":"hidden"});
                        }
                    });
                }else{
                    $('#lightbox-loader').css({"visibility":"hidden"});
                }
            });

            $("#operadorNombre").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $("#mensaje-error-name").addClass("d-none");
                        $(this).removeClass("form-control-error");
                    }
                },
                input: function () {
                    if(! regexpSoloLetra.test($(this).val())){
                        let cadena="";
                        let letter = "";

                        for(let i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(regexpSoloLetra.test(letter)){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                }
            });

            $("#operadorApellido").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $("#mensaje-error-apellido").addClass("d-none");
                        $(this).removeClass("form-control-error");
                    }
                },
                input: function () {
                    if(! regexpSoloLetra.test($(this).val())){
                        let cadena="";
                        let letter = "";

                        for(let i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(regexpSoloLetra.test(letter)){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                }
            });

            $("#operadorCorreo").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $("#mensaje-error-correo").addClass("d-none");
                        $("#mensaje-error-correo").removeClass("d-flex");
                        $(this).removeClass("form-control-error");
                    }
                },
                blur: function () {
                    if(! regexpEmail.test($(this).val())){
                        $("#mensaje-error-correo").addClass("d-flex");
                        $("#mensaje-error-correo").removeClass("d-none");
                        $("#mensaje-error-correo").html("La dirección de correo no tiene un formato válido");
                        $(this).addClass("form-control-error");
                    }
                }
            });

            $("#operadorIdentificacion").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $("#mensaje-error-identificacion").addClass("d-none");
                        $(this).removeClass("form-control-error");
                    }
                },
                input: function () {
                    if(! regexpSoloNumero.test($(this).val())){
                        let cadena="";
                        let letter = "";

                        for(let i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(regexpSoloNumero.test(letter)){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                }
            });

            $("#operadorTelefono").on({
                keyup: function () {
                    if($(this).val().length > 0){
                        $("#mensaje-error-telefono").addClass("d-none");
                        $(this).removeClass("form-control-error");
                    }
                },
                input: function () {
                    if(! regexpSoloNumero.test($(this).val())){
                        let cadena="";
                        let letter = "";

                        for(let i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(regexpSoloNumero.test(letter)){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                }
            });

            $("#operadorPassword").on("keyup", function (e) {
                let cadena = $("#operadorPassword").val();

                if(cadena.length > 0){
                    $("#mensaje-error-password").addClass("d-none");
                    $("#operadorPassword").removeClass("form-control-error");
                }
            });
        });
    </script>
@endsection
