@extends('app')
@section('title', 'Ventas por cobrar')

<?php
$_GET['page'] = "reporte";
$_GET['page-title'] = "Detalle de ventas del cliente.";
$_GET['page-description'] = "Se muestra reporte detalladado de todas las ventas pagadas y no pagadas que se le ha realizado a un cliente.";
?>

@section('content-body')
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h3 class="font-weight-bold">{{ $cliente->nombres }} {{ $cliente->apellidos }}</h3>
                    <div class="row">
                        <div class="col-sm-6 col-md-4"><h6><b>Identificación:</b> {{ $cliente->identificacion }}</h6></div>
                        <div class="col-sm-6 col-md-4"><h6><b>Email:</b> {{ $cliente->email }}</h6></div>
                        <div class="col-sm-6 col-md-4"><h6><b>Teléfono:</b> {{ $cliente->telefono }}</h6></div>
                        <div class="col-sm-6 col-md-4"><h6><b>Celular:</b> {{ $cliente->celular }}</h6></div>
                        <div class="col-sm-8 col-md-4"><h6><b>Dirección domiciliaria:</b> {{ $cliente->direccion }}</h6></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Pagos realizados</h5>
                    <form class="needs-validation" novalidate>
                        @csrf
                        @include('include.buscador')

                        <div class="table-responsive">
                            <table class="mb-0 table table-bordered my-table">
                                <thead>
                                <tr>
                                    <th>Pago</th>
                                    <th>Factura</th>
                                    {{--<th>Valor pendiente de pago</th>--}}
                                    <th>Valor pagado</th>
                                    <th>Saldo por pagar</th>
                                    <th>Fecha que se pagó</th>
                                    <th>Descripción del pago</th>
                                </tr>
                                </thead>
                                <tbody id="myTable">
                                @foreach($ventasPagadas as $key => $ventaPagada)
                                    <tr>
                                        <td>{{ $ventaPagada->id }}</td>
                                        <td style="background: #a5d1f2; color: #000000;">{{ $ventaPagada->salesHeader->num_factura }}</td>
                                        {{--<td>${{ $ventaPagada->total }}</td>--}}
                                        <td>${{ $ventaPagada->valor_pago }}</td>
                                        <td style="background: #a0331e; color: #ffffff;"><b>${{ $ventaPagada->saldo }}</b></td>
                                        <td style="background: #cecece; color: #000000;">{{ \Carbon\Carbon::parse($ventaPagada->fecha_realiza_pago)->isoFormat('dddd, DD MMMM Y') }}</td>
                                        <td>{{ $ventaPagada->estadoPago->descripcion }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Facturas pendientes de pago</h5>
                    <form class="needs-validation" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="search-wrapper active w-100">
                                    <div class="input-holder w-100">
                                        <input type="text" class="search-input" id="input-busqueda1" placeholder="Búsqueda por todos los campos">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="mb-0 table table-bordered my-table">
                                <thead>
                                <tr>
                                    <th>Factura</th>
                                    <th>Observación</th>
                                    <th>Total factura</th>
                                    <th>Saldo por pagar</th>
                                    <th>Fecha estimada de pago</th>
                                </tr>
                                </thead>
                                <tbody id="myTable1">
                                @foreach($ventasNoPagadas as $ventaNoPagada)
                                    <tr>
                                        <td style="background: #a5d1f2; color: #000000;">{{ $ventaNoPagada->salesHeader->num_factura }}</td>
                                        <td>{{ $ventaNoPagada->salesHeader->observacion }}</td>
                                        <td>${{ $ventaNoPagada->totalfac }}</td>
                                        <td style="background: #a0331e; color: #ffffff;"><b>${{ $ventaNoPagada->saldo }}</b></td>
                                        <td style="background: #cecece; color: #000000;">{{ \Carbon\Carbon::parse($ventaNoPagada->fecha_pago_credito)->isoFormat('dddd, DD MMMM Y') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $("#input-busqueda").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $("#input-busqueda1").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTable1 tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        })
    </script>
@endsection
