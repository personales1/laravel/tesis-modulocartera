@php
    use Carbon\Carbon;
    $fechaCurrent = Carbon::now()->startOfDay();
@endphp

<div class="card-body table-responsive">
    <h2 style="text-transform: uppercase; color: black; font-weight: bold;">Reporte de motivos de crédito</h2>
    <h3>Con fecha de {{ $fechaCurrent->format('d-m-Y') }}</h3>

    <table style="width: 100%; border: 1px solid #e9ecef; border-collapse: collapse;">
        <thead style="text-align: center;">
        <tr>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Cod.</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Nombres</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Estado</th>
        </tr>
        </thead>
        <tbody>
        @foreach($credits as $credit)
            <tr>
                <th style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $credit->id }}</th>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $credit->descripcion }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">
                    @if($credit->is_activo)
                        Activo
                    @else
                        Inactivo
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
