@extends('app')
@section('title', 'Parroquia')

<?php
    $_GET['page'] = "ubicacion";
    $_GET['page-title'] = "Parroquia";
    if(isset($parroquias)){
        $_GET['page-description'] = "Actualizar parroquia ". $parroquias->nombre;

    }else{
        $_GET['page-description'] = "Registro de nuevas parroquias.";
    }
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body">
            <h5 class="card-title">Creación de parroquias</h5>
            <form id="myform" method="POST" action="{{ route('parroquia.create', ['id' => $parroquias->id ?? '']) }}" class="needs-validation" novalidate>
                @csrf
                <div class="form-row">
                    <div class="col-md-5 mb-3">
                        <label for="parroquiaName">Nombre</label>
                        <input name="name" id="parroquiaName" type="text" class="form-control" placeholder="Nombre" value="{{ $parroquias->nombre ?? old('name') }}" required>
                        @error('name')
                            <label class="mensaje-error" id="mensaje-error-name">{{ $message }}</label>
                            <script>
                                $("#parroquiaName").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="provinceSelect">Provincia</label>
                        {{ Form::select('provincia', $provincia, $parroquias->province_id ?? null, ['class' => 'form-control', 'id' => 'provinceSelect']) }}
                        @error('provincia')
                            <label class="mensaje-error" id="mensaje-error-provincia">{{ $message }}</label>
                            <script>
                                $("#provinceSelect").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                    <div class="col-md-3 mb-3">
                        <input type="hidden" id="cantonSelected" value="{{ $parroquias->canton_id ?? -1 }}">
                        <label for="cantonSelect">Cantón</label>
                        {{ Form::select('cantón', [''], $parroquias->canton_id ?? null, ['class' => 'form-control', 'id' => 'cantonSelect']) }}
                        @error('cantón')
                            <label class="mensaje-error" id="mensaje-error-canton">{{ $message }}</label>
                            <script>
                                $("#cantonSelect").addClass("form-control-error");
                            </script>
                        @enderror
                    </div>
                </div>

                <button class="btn btn-primary btnGuardar" type="submit">Guardar</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var provincia = $("#provinceSelect").val();
            getCanton(provincia);

            $(".btnGuardar").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
            });

            $("#parroquiaName").on("keyup", function (e) {
                let cadena = $("#parroquiaName").val();

                if(cadena.length > 0){
                    $('#mensaje-error-name').hide();
                    $("#parroquiaName").removeClass("form-control-error");
                }
            });

            $("#provinceSelect").change(function () {
                $('#mensaje-error-canton').hide();
                $('#cantonSelect').removeClass("form-control-error");
                provincia = $(this).val();
                getCanton(provincia);
            });

            function getCanton(idProvincia){
                $('#lightbox-loader').css({"visibility":"visible"});

                $.ajax({
                    url:"{{ route('canton.find.by.province') }}",
                    type: 'POST',
                    data: {
                        _token: $("input[name='_token']").val(),
                        provinciaId: idProvincia,
                        cantonId: $("#cantonSelected").val()
                    },
                    success: function(data) {
                        console.log(data);
                        $('#lightbox-loader').css({"visibility":"hidden"});
                        $('#cantonSelect').html(data);
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        alert("Hubo un error.");
                        $('#lightbox-loader').css({"visibility":"hidden"});
                    }
                });
            }
        });
    </script>
@endsection
