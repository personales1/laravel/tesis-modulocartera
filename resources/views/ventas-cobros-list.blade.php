@extends('app')
@section('title', 'Pago de facturas')

<?php
$_GET['page'] = "cobros";
$_GET['page-title'] = "Pago de facturas";
$_GET['page-description'] = "Listado de las facturas de ventas que están pendientes de pago por parte del cliente.";
?>

@section('content-body')
    @php
        use Carbon\Carbon;
        $fechaCurrent = Carbon::now()->startOfDay();
    @endphp

    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
        <li class="nav-item">
            <a class="nav-link active" href="{{ Request::root() }}/credito/otorgar">
                <span>Nueva venta</span>
            </a>
        </li>
    </ul>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body table-responsive">
                    <h5 class="card-title">Listado de ventas</h5>
                    <form class="needs-validation" novalidate>
                        @csrf
                        @include('include.buscador')

                        <table class="mb-0 table table-bordered my-table">
                            <thead>
                                <tr>
                                    <th>Factura</th>
                                    <th>Cliente</th>
                                    <th>Identificación</th>
                                    <th>Fecha estimada de pago</th>
                                    <th>Estatus</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach($ventas as $venta)
                                    <tr>
                                        @php $fechaPagocredito = \Carbon\Carbon::parse($venta->fecha_pago_credito) @endphp

                                        <td>{{ $venta->salesHeader->num_factura }}</td>
                                        <td>{{ $venta->salesHeader->client->nombres }} {{ $venta->salesHeader->client->apellidos }}</td>
                                        <td>{{ $venta->salesHeader->client->identificacion }}</td>
                                        <td >{{ $fechaPagocredito->isoFormat('dddd, DD MMMM Y') }}</td>
                                        <td>
                                            @if($fechaPagocredito->diffInDays($fechaCurrent) == 0) <!-- es la fecha de cobro -->
                                                <div class="badge badge-success">FECHA DE PAGO</div>
                                            @elseif($fechaPagocredito->gt($fechaCurrent)) <!-- aún no es la fecha de cobro; gt() indica si la fecha que la llama es mayor a la que recibe por parámetro -->
                                                @if($fechaPagocredito->diffInDays($fechaCurrent) <= 3)
                                                    <div class="badge badge-info">FECHA CERCANA</div>
                                                @else
                                                    <div class="badge badge-light">POR ADELANTADO</div>
                                                @endif
                                            @else
                                                @if($fechaPagocredito->diffInDays($fechaCurrent) <= 3) <!-- se pasó de la fecha -->
                                                    <div class="badge badge-warning">FUERA DE TIEMPO</div>
                                                @else
                                                    <div class="badge badge-danger">ATRASADO</div>
                                                @endif
                                            @endif
                                        </td>
                                        <td>${{ $venta->total }}</td>
                                        <td>
                                            <button class="btn-shadow btn btn-primary btnModalCobrar" type="button" data-toggle="modal" data-target="#modalCobrar" data-sales="{{ $venta }}" data-id="{{ $venta->id }}" data-sales-motivo="{{ $venta->salesHeader->creditReason->descripcion }}" data-sales-fecha="{{ $fechaPagocredito->format('d-m-Y') }}" data-total="{{ $venta->total }}">
                                                Cobrar
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var pago;
            var facturaTotal;
            var regexpNumeroDecimal = /^\d+\.?\d{0,2}$/;

            $("#input-busqueda").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $(".btnModalCobrar").on("click", function (e) {
                pago = $(this).data("id");
                let factura = $(this).data("sales");
                let facturaFechaPago = $(this).data("salesFecha");
                let facturaMotivo = $(this).data("salesMotivo");
                facturaTotal = $(this).data("total");

                console.log(factura);

                $("#modalTitle").html("Factura #" + factura['sales_header']['num_factura']);
                $("#facturaCliente").html(factura['sales_header']['client']['nombres'] + " " + factura['sales_header']['client']['apellidos']);
                $("#facturaFechaPago").html(facturaFechaPago);
                $("#facturaIdentificacion").html(factura['sales_header']['client']['identificacion']);
                $("#facturaCorreo").html(factura['sales_header']['client']['email']);
                $("#facturaMotivo").html(facturaMotivo);
                $("#facturaObservacion").html(factura['sales_header']['observacion']);
                $("#valorCobrar").val(facturaTotal);
                $("#facturaTotalPago").html("Total $" + facturaTotal);
            });

            $("#valorCobrar").on({
                input: function () {
                    if(! regexpNumeroDecimal.test($("#valorCobrar").val())){
                        var cadena="";
                        var letterComa = 0;
                        var letter = "";
                        var letterDecimales = 0;

                        for(i=0; i < $(this).val().trim().length; i++){
                            letter = $(this).val().charAt(i);

                            if(letter == ","){
                                letter = ".";
                            }

                            if(letterComa > 0){
                                letterDecimales += 1;
                            }

                            if(letter == "."){
                                letterComa += 1;
                            }

                            if(regexpNumeroDecimal.test(letter)){
                                if(letterDecimales <= 2){
                                    cadena += letter;
                                }
                            }
                            else if(letter == "." && letterComa == 1 && i > 0){
                                cadena += letter;
                            }
                        }

                        $(this).val(cadena);
                    }
                },
                keyup: function () {
                    if($(this).val().length > 0){
                        $("#mensaje-error-valor-factura").html("");
                        $("#valorCobrar").removeClass("form-control-error");
                    }
                }
            });

            $(".btnCobrar").on("click", function (e) {
                if($("#valorCobrar").val().trim() == ""){
                    $("#mensaje-error-valor-factura").html("Campo obligatorio");
                    $("#valorCobrar").addClass("form-control-error");
                }else{
                    $('#lightbox-loader').css({"visibility":"visible"});
                    var valorIngresado = parseFloat($("#valorCobrar").val().trim());

                    $.ajax({
                        url: '{{ route('venta.registro.pago') }}',
                        type: 'POST',
                        data: {
                            _token: $("input[name='_token']").val(),
                            pagoId: pago,
                            pagoValor: valorIngresado
                        },
                        success: function(data) {
                            /*
                            showToastCustom("Registro de pago", data);
                            $('#input-busqueda').val = "";
                            setTimeout(cargarTodo, 2000);
                            */

                            let myJsonResponse = $.parseJSON(data); //convierte el response string en json

                            if(myJsonResponse.estado >= 400){
                                $('#lightbox-loader').css({"visibility":"hidden"});
                                Swal.fire('¡Error!', myJsonResponse.data.mensaje, 'error');
                            }
                            else{
                                //Swal.fire('¡Éxito!', myJsonResponse.data.mensaje, 'success');
                                showToastCustom("Registro de pago", myJsonResponse.data.mensaje);
                                $('#input-busqueda').val = "";
                                setTimeout(cargarTodo, 2000);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                            console.log(JSON.stringify(jqXHR));
                            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                            alert("Hubo un error.");
                            $('#lightbox-loader').css({"visibility":"hidden"});
                        }
                    });

                }
            });

            function cargarTodo(){
                location.reload();
            }
        })
    </script>
@endsection

@include('include.modal-cobrar')
