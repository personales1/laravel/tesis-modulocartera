@php
    use Carbon\Carbon;
    $fechaCurrent = Carbon::now()->startOfDay();
@endphp

<div class="card-body table-responsive">
    <h2 style="text-transform: uppercase; color: black; font-weight: bold;">Reporte de usuarios</h2>
    <h3>Con fecha de {{ $fechaCurrent->format('d-m-Y') }}</h3>

    <table style="width: 100%; border: 1px solid #e9ecef; border-collapse: collapse;">
        <thead style="text-align: center;">
        <tr>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Cod.</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Nombres</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Apellidos</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Email</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Identificación</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Teléfono</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Rol</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Estado</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            @if($user->is_activo)
                @php
                    $colorFondo = "#FFFFFF";
                    $colorTexto = "#000000";
                    $estado = "Activo";
                @endphp
            @else
                @php
                    $colorFondo = "#B81F44";
                    $colorTexto = "#FFFFFF";
                    $estado = "Inactivo";
                @endphp
            @endif
            <tr style="background-color: {{ $colorFondo }}; color: {{ $colorTexto }}">
                <th style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $user->id }}</th>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $user->userProfile->nombres }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $user->userProfile->apellidos }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $user->email }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $user->userProfile->identificacion }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $user->userProfile->telefono }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ \App\Role::where('rol_id', $user->id_rol)->first()->nombre }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $estado }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
