@extends('app')
@section('title', 'Cantón')

<?php
    $_GET['page'] = "ubicacion";
    $_GET['page-title'] = "No existe";
    $_GET['page-description'] = "La página que intentas buscar no existe";
?>

@section('content-body')
    <div class="main-card mb-3 card">
        <div class="card-body" style="text-align: center">
            <div>
                <img src="{{ asset('assets/images/404.png') }}" width="250">
                <p style="color: black; font-size: 20px">La página no existe</p>
            </div>
        </div>
    </div>
@endsection
