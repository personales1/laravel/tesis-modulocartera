@php
    use Carbon\Carbon;
    $fechaCurrent = Carbon::now()->startOfDay();
@endphp

<div class="card-body table-responsive">
    <h2 style="text-transform: uppercase; color: black; font-weight: bold;">Reporte de productos</h2>
    <h3>Con fecha de {{ $fechaCurrent->format('d-m-Y') }}</h3>

    <table style="width: 100%; border: 1px solid #e9ecef; border-collapse: collapse;">
        <thead style="text-align: center;">
        <tr>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Cod.</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Nombres</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Descripción</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Precio</th>
            <th style="border-bottom: 2px solid #e9ecef; border-right: 1px solid #e9ecef; text-align: center; font-size: 12px; color: black; font-weight: bold;">Estado</th>
        </tr>
        </thead>
        <tbody>
        @foreach($productos as $producto)
            @if($producto->is_activo)
                @php
                    $colorFondo = "#FFFFFF";
                    $colorTexto = "#000000";
                    $estado = "Activo";
                @endphp
            @else
                @php
                    $colorFondo = "#B81F44";
                    $colorTexto = "#FFFFFF";
                    $estado = "Inactivo";
                @endphp
            @endif
            <tr>
                <th style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $producto->id }}</th>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $producto->nombre }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $producto->descripcion }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">${{ $producto->precio }}</td>
                <td style="vertical-align: middle; border: 1px solid #e9ecef; padding: .55rem; text-align: center; font-size: 12px;">{{ $estado }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
