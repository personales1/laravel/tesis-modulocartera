@extends('app')
@section('title', 'Historial Crediticio')

<?php
$_GET['page'] = "reporte";
$_GET['page-title'] = "Reporte de historial crediticio";
$_GET['page-description'] = "Reporte que muestra el historial crediticio consolidado por cliente considerando todos los pagos que ha realizado.";
?>

@section('content-body')
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Historial crediticio</h5>
                    <form class="needs-validation" novalidate>
                        @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="font-weight-bold">Búsqueda por calificación crediticia:</div>
                                <div class="row">
                                    @foreach($misEstadosPagos as $estadosPagos)
                                        <label class="mb-0 ml-2 col-auto"><input type="checkbox" class="checkCalificacion" data-id="{{ $estadosPagos->id }}" checked>  {{ $estadosPagos->ponderacion_texto }}</label>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-md-6 mb-3">
                                <div class="search-wrapper active w-100">
                                    <div class="input-holder w-100">
                                        <input type="text" class="search-input" id="input-busqueda" placeholder="Búsqueda por todos los campos">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 mb-3">
                                <div class="float-right">
                                    <a id="busqueda-filtro"><img src="{{asset('assets/images/iconbuscar.png')}}" width="40"></a>
                                    <a href="{{route('export.excel.historial.crediticio')}}"><img src="{{asset('assets/images/iconexcel.png')}}" width="40"></a>
                                    <a href="{{route('export.pdf.historial.crediticio')}}"><img src="{{asset('assets/images/iconpdf.png')}}" width="40"></a>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="mb-0 table table-bordered my-table">
                                <thead>
                                <tr>
                                    <th>Cliente</th>
                                    <th>Identificación</th>
                                    <th>Teléfono</th>
                                    <th>Email</th>
                                    <th>Calidad crediticia</th>
                                    <th>Calificación crediticia</th>
                                    <th>Descripción de la calificación</th>
                                    <th>Ventas pendiente de pago</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="myTableTBody">
                                @foreach($pagos as $pago)
                                    @php $ponderacionSuma = 0 @endphp

                                    @foreach($pago as $p)
                                        @php $ponderacionSuma += $p->estadoPago->ponderacion @endphp
                                    @endforeach

                                    @php
                                        $ponderacionPromedio = $ponderacionSuma/count($pago);
                                        $ponderacionPromedio = round($ponderacionPromedio);

                                        $estadoPago = \App\EstadoPago::where('ponderacion', $ponderacionPromedio)->first();
                                        $numeroPagosPendientes = \App\RegistroPago::where([['client_id', $pago[0]->client_id], ['is_pagado', 0]])->count();
                                    @endphp

                                    <tr class="{{ $estadoPago->ponderacion_clase_css }}">
                                        <td>{{ $pago[0]->client->nombres }} {{ $pago[0]->client->apellidos }}</td>
                                        <td>{{ $pago[0]->client->identificacion }}</td>
                                        <td>@if(empty($pago[0]->client->telefono)) {{ $pago[0]->client->celular }} @else {{ $pago[0]->client->telefono }} / {{ $pago[0]->client->celular }} @endif</td>
                                        <td>{{ $pago[0]->client->email }}</td>
                                        <td>{{ $estadoPago->ponderacion_texto }}</td>
                                        <th>{{ $estadoPago->ponderacion_calificacion }}</th>
                                        <td>{{ $estadoPago->descripcion }}</td>
                                        <td>{{ $numeroPagosPendientes }} ventas</td>
                                        <td class="calificacion" style="display: none;">{{ $estadoPago->id }}</td>
                                        <td>
                                            <a class="nav-link active" href="/ventas/detail/cliente/{{ $pago[0]->client_id }}">
                                                <button class="btn-shadow btn btn-primary btnVerDetalle" title="Ver detalles" type="button">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body" style="padding: 12px">
                    <h5 class="card-title">Descripción de las calificaciones de crédito</h5>

                    <div class="row">
                        <div class="mb-3 col-6 col-md-3">
                            <div class="card card-body {{ $misEstadosPagos[1]->ponderacion_clase_css }}" style="padding: 12px; min-height: 100%">
                                <h5 class="text-white card-title">{{ $misEstadosPagos[1]->ponderacion_texto }} "{{ $misEstadosPagos[1]->ponderacion_calificacion }}"</h5>
                                <span style="text-align: justify">Cliente suele pagar su crédito con muchos días de anticipación.</span>
                            </div>
                        </div>
                        <div class="mb-3 col-6 col-md-3">
                            <div class="card card-body {{ $misEstadosPagos[0]->ponderacion_clase_css }}" style="padding: 12px; min-height: 100%">
                                <h5 class="text-white card-title">{{ $misEstadosPagos[0]->ponderacion_texto }} "{{ $misEstadosPagos[0]->ponderacion_calificacion }}"</h5>
                                <span style="text-align: justify">Cliente suele pagar su crédito con pocos días de anticipación.</span>
                            </div>
                        </div>
                        <div class="mb-3 col-6 col-md-3">
                            <div class="card card-body {{ $misEstadosPagos[2]->ponderacion_clase_css }}" style="padding: 12px; min-height: 100%">
                                <h5 class="text-white card-title">{{ $misEstadosPagos[2]->ponderacion_texto }} "{{ $misEstadosPagos[2]->ponderacion_calificacion }}"</h5>
                                <span style="text-align: justify">Cliente suele pagar su crédito el mismo día de la fecha fijada.</span>
                            </div>
                        </div>
                        <div class="mb-3 col-6 col-md-3">
                            <div class="card card-body {{ $misEstadosPagos[3]->ponderacion_clase_css }}" style="padding: 12px; min-height: 100%">
                                <h5 class="text-white card-title">{{ $misEstadosPagos[3]->ponderacion_texto }} "{{ $misEstadosPagos[3]->ponderacion_calificacion }}"</h5>
                                <span style="text-align: justify">Cliente suele pagar su crédito el mismo día de la fecha fijada, pero hace un pago incompleto.</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="mb-3 col-6 col-md-3">
                            <div class="card card-body {{ $misEstadosPagos[4]->ponderacion_clase_css }}" style="padding: 12px; min-height: 100%">
                                <h5 class="text-white card-title">{{ $misEstadosPagos[4]->ponderacion_texto }} "{{ $misEstadosPagos[4]->ponderacion_calificacion }}"</h5>
                                <span style="text-align: justify">Cliente suele pagar su crédito con pocos días de atraso.</span>
                            </div>
                        </div>
                        <div class="mb-3 col-6 col-md-3">
                            <div class="card card-body {{ $misEstadosPagos[5]->ponderacion_clase_css }}" style="padding: 12px; min-height: 100%">
                                <h5 class="text-white card-title">{{ $misEstadosPagos[5]->ponderacion_texto }} "{{ $misEstadosPagos[5]->ponderacion_calificacion }}"</h5>
                                <span style="text-align: justify">Cliente suele pagar su crédito con pocos días de atraso y paga incompleto.</span>
                            </div>
                        </div>
                        <div class="mb-3 col-6 col-md-3">
                            <div class="card card-body {{ $misEstadosPagos[6]->ponderacion_clase_css }}" style="padding: 12px; min-height: 100%">
                                <h5 class="text-white card-title">{{ $misEstadosPagos[6]->ponderacion_texto }} "{{ $misEstadosPagos[6]->ponderacion_calificacion }}"</h5>
                                <span style="text-align: justify">Cliente suele atrasarse en el pago de su crédito.</span>
                            </div>
                        </div>
                        <div class="mb-3 col-6 col-md-3">
                            <div class="card card-body {{ $misEstadosPagos[7]->ponderacion_clase_css }}" style="padding: 12px; min-height: 100%">
                                <h5 class="text-white card-title">{{ $misEstadosPagos[7]->ponderacion_texto }} "{{ $misEstadosPagos[7]->ponderacion_calificacion }}"</h5>
                                <span style="text-align: justify">Cliente suele atrasarse en el pago de su crédito y paga incompleto.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $("#input-busqueda").on('keyup', function (e) {
                let value = $(this).val().toLowerCase();

                $("#myTableTBody tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });

                let checkboxes = $(this).closest('form').find(':checkbox');
                checkboxes.prop('checked', true);
            });

            $(".checkCalificacion").on('change', function (e) {
                $("#input-busqueda").val("");
            });

            $("#busqueda-filtro").on("click", function (e) {
                $('#lightbox-loader').css({"visibility":"visible"});
                $("#input-busqueda").val("");
                let listClienteId = [];

                $("input:checkbox:checked").each(
                    function () {
                        listClienteId.push($(this).data("id"));
                    }
                );

                if(listClienteId.length > 0){
                    let tr = $("#myTableTBody tr");
                    for(let i = 0; i < tr.length; i++){
                        let mostrar = false;
                        let td = tr[i].getElementsByClassName("calificacion")[0];
                        let txtValor = td.textContent || td.innerText;

                        for(let j = 0; j < listClienteId.length; j++){
                            if(! mostrar){
                                if(listClienteId[j] === parseInt(txtValor)){
                                    mostrar = true;
                                }
                            }
                        }

                        if (mostrar) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }else{
                    alert("Seleccione una opción de búsqueda");
                }

                $('#lightbox-loader').css({"visibility":"hidden"});
            })
        })
    </script>
@endsection
