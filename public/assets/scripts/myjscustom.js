/*
window.setTimeout(function () {
    hideToastCustom();
}, 5000);
*/

$(".toast-top-right").on("click", function (e) {
    hideToastCustom();
});

function hideToastCustom(){
    $(".toast-top-right").fadeTo(3000, 0).slideUp(500, function () {
        $(".toast-top-right").css('display', 'none');
    });
}

function showToastCustom(tituloAlert, mensajeAlert){
    $(".toast-top-right").css('display', 'flex');
    $(".toast-top-right").css('opacity', '1');

    $("#title-success").html(tituloAlert);
    $("#mensaje-success").html(mensajeAlert);

    window.setTimeout(function () {
        hideToastCustom();
    }, 5000);
}

function createToastError(titulo, mensaje){
    $(".mostrarToast").append(
        '' +
        '<div class="toast-top-right" id="toast-container" style="opacity: 1">' +
        '<div class="toast toast-error" aria-live="polite">' +
        '<div class="toast-title">' + titulo + '</div>' +
        '<div class="toast-message">' + mensaje + '</div>' +
        '</div>' +
        '</div>'
    );

    window.setTimeout(function () {
        hideToastCustom();
    }, 2000);
}
