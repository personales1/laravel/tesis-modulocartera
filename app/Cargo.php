<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $primaryKey = 'cargo_id';
    public $incrementing = false;
    protected $keyType = 'string';
}
