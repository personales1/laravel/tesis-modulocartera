<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoPago extends Model
{
    use SoftDeletes;

    public function registroPago(){
        return $this->hasMany(RegistroPago::class);
    }
}
