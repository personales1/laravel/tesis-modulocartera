<?php

namespace App\Exports;

use App\Canton;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CantonExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $cantones = Canton::all();

        return view('pdf-reporte-canton', [
            'cantones' => $cantones
        ]);
    }
}
