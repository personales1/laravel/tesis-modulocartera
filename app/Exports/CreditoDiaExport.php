<?php

namespace App\Exports;

use App\CreditDay;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CreditoDiaExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $credits = CreditDay::all();

        return view('pdf-reporte-credito-dias', [
            'credits' => $credits
        ]);
    }
}
