<?php

namespace App\Exports;

use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsuarioExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $users = User::all();

        return view('pdf-reporte-usuarios', [
            'users' => $users
        ]);
    }
}
