<?php

namespace App\Exports;

use App\Province;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProvinciaExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $provincias = Province::all();

        return view('pdf-reporte-provincias', [
            'provincias' => $provincias
        ]);
    }
}
