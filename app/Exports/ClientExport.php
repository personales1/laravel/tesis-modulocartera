<?php

namespace App\Exports;

use App\Client;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ClientExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $clientes = Client::all();

        return view('pdf-reporte-clientes', [
            'clientes' => $clientes
        ]);
    }
}
