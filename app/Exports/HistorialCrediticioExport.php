<?php

namespace App\Exports;

use App\RegistroPago;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class HistorialCrediticioExport implements FromView
{
    public function view(): View
    {
        $registroPagos = RegistroPago::where('is_pagado', 1)->orderBy('client_id', 'asc')->get();
        $clientesPagos = $registroPagos->groupBy('client_id');

        return view('pdf-reporte-historial-crediticio', [
            'pagos' => $clientesPagos
        ]);
    }
}
