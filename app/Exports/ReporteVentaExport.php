<?php

namespace App\Exports;

use App\SalesHeader;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class ReporteVentaExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $ventas = SalesHeader::all();

        return view('pdf-reporte-ventas', [
            'ventas' => $ventas
        ]);
    }
}
