<?php

namespace App\Exports;

use App\Sector;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SectorExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $sectores = Sector::all();

        return view('pdf-reporte-sector', [
            'sectores' => $sectores
        ]);
    }
}
