<?php

namespace App\Exports;

use App\Parroquia;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ParroquiaExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $parroquias = Parroquia::all();

        return view('pdf-reporte-parroquia', [
            'parroquias' => $parroquias
        ]);
    }
}
