<?php

namespace App\Exports;

use App\Product;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProductExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $productos = Product::all();

        return view('pdf-reporte-productos', [
            'productos' => $productos
        ]);
    }
}
