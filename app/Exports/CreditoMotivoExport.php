<?php

namespace App\Exports;

use App\CreditReason;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CreditoMotivoExport implements FromView
{
    /**
     * @return View
     */
    public function view(): View
    {
        $credits = CreditReason::all();

        return view('pdf-reporte-credito-motivo', [
            'credits' => $credits
        ]);
    }
}
