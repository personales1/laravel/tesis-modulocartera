<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Canton extends Model
{
    use SoftDeletes;

    public function parroquia(){
        return $this->hasMany(Parroquia::class);
    }

    public function cliente(){
        return $this->hasMany(Client::class);
    }

    public function province(){
        return $this->belongsTo(Province::class);
    }
}
