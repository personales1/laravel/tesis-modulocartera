<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesHeader extends Model
{
    use SoftDeletes;

    public function salesDetail(){
        return $this->hasMany(SalesDetail::class);
    }

    //El nombre de la función tiene que ser el mismo nombre del modelo
    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function creditReason(){
        return $this->belongsTo(CreditReason::class);
    }

    public function registroPago(){
        return $this->hasMany(RegistroPago::class);
    }
}
