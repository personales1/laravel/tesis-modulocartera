<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Model
{
    use SoftDeletes;

    public function canton(){
        return $this->hasMany(Canton::class);
    }

    public function parroquia(){
        return $this->hasMany(Parroquia::class);
    }

    public function cliente(){
        return $this->hasMany(Client::class);
    }
}
