<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesDetail extends Model
{
    use SoftDeletes;

    public function salesHeader(){
        return $this->belongsTo(SalesHeader::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
