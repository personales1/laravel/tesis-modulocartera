<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parroquia extends Model
{
    use SoftDeletes;

    public function cliente(){
        return $this->hasMany(Client::class);
    }

    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function canton(){
        return $this->belongsTo(Canton::class);
    }
}
