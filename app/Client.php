<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    public function clientWork(){
        return $this->hasOne(ClientWork::class);
    }

    public function clientDocument(){
        return $this->hasOne(ClientDocument::class);
    }

    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function canton(){
        return $this->belongsTo(Canton::class);
    }

    public function parroquia(){
        return $this->belongsTo(Parroquia::class);
    }

    public function sector(){
        return $this->belongsTo(Sector::class);
    }

    public function salesHeader(){
        return $this->hasMany(SalesHeader::class);
    }

    public function registroPago(){
        return $this->hasMany(RegistroPago::class);
    }
}
