<?php

namespace App\Mail;

use App\RegistroPago;
use App\SalesDetail;
use App\SalesHeader;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientVentaCreditoMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $salesHeader;
    public $salesDetail;
    public $registroPago;
    public $cliente;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SalesHeader $salesHeader, RegistroPago $registroPago)
    {
        $this->salesHeader = $salesHeader;
        $this->registroPago = $registroPago;

        $this->salesDetail = SalesDetail::where('sales_header_id', $this->salesHeader->id)->get();
        $this->cliente = $salesHeader->client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.credit-venta-client')
            ->subject("Venta de crédito");
    }
}
