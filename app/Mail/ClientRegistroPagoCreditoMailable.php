<?php

namespace App\Mail;

use App\RegistroPago;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientRegistroPagoCreditoMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $registroPago;
    public $totalPago;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RegistroPago $registroPago)
    {
        $this->registroPago = $registroPago;
        $this->totalPago = RegistroPago::select('total')->where('sales_header_id', $registroPago->sales_header_id)->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.credit-registro-pago-client')
            ->subject("Registro de pago");
    }
}
