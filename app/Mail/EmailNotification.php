<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $usuario;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->usuario = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');

        return $this
            ->markdown('emails.demo')
            ->subject("es una prueba de creación de usuario")
            ->with(
                [
                    'nombre' => 'José Luis Proaño Acosta',
                    'profesion' => 'Ingeniero en sistemas computacionales.',
                ]);
    }
}
