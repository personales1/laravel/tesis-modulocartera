<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function show(){
        $productos = Product::all();
        return view('producto-list', ['productos'=>$productos]);
    }

    public function showForm($id){
        $productos = Product::find($id);

        if($productos == null){
            return view('error404');
        }

        return view('producto-new', ['productos'=>$productos]);
    }

    public function destroy($id){
        $producto = Product::find($id);
        $producto->delete();
        return config('constant.alert.success.delete');
    }

    public function store(Request $request, $id = null){
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);

        if(isset($id)){
            $producto = Product::find($id);
            $messageSuccess = config('constant.alert.success.update');
        }else{
            $producto = new Product();
            $messageSuccess = "El producto ". $request->name . " se ha creado satisfactoriamente.";
        }

        $producto->nombre = $request->name;
        $producto->descripcion = $request->description;
        $producto->precio = $request->price;
        $producto->save();

        return redirect()->route('producto.list')->with(['success'=> $messageSuccess]);
    }

    public function changeState($id){
        $estado_succes = 200;
        $estado_error_general = 400;

        $producto = Product::find($id);

        if($producto != null){
            if($producto->is_activo == 0){
                $producto->is_activo = 1;
                $estadoRegistro = "Activo";
                $clase = "btn-success";
            }
            else{
                $producto->is_activo = 0;
                $estadoRegistro = "Inactivo";
                $clase = "btn-danger";
            }

            $answer = array(
                "estado" => $estado_succes,
                "data" => array(
                    "mensaje" => config('constant.alert.success.state'),
                    "user_estado" => $estadoRegistro,
                    "user_clase" => $clase,
                ),
            );

            $producto->save();
        }else{
            $answer = array(
                "estado" => $estado_error_general,
                "data" => array(
                    "mensaje" => config('constant.alert.error.not_found'),
                    "user_estado" => "",
                    "user_clase" => "",
                ),
            );
        }

        return json_encode($answer);
    }
}
