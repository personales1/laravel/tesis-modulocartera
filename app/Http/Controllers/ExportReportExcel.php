<?php

namespace App\Http\Controllers;

use App\Exports\CantonExport;
use App\Exports\ClientExport;
use App\Exports\CreditoDiaExport;
use App\Exports\CreditoMotivoExport;
use App\Exports\HistorialCrediticioExport;
use App\Exports\ParroquiaExport;
use App\Exports\ProductExport;
use App\Exports\ProvinciaExport;
use App\Exports\ReporteVentaExport;
use App\Exports\SectorExport;
use App\Exports\UsuarioExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportReportExcel extends Controller
{
    public function printHistorialCrediticio(){
        return Excel::download(new HistorialCrediticioExport(), 'reporte_historial_crediticio.xlsx');
    }

    public function printCliente(){
        return Excel::download(new ClientExport(), 'reporte_clientes.xlsx');
    }

    public function printProvincia(){
        return Excel::download(new ProvinciaExport(), 'reporte_provincias.xlsx');
    }

    public function printCanton(){
        return Excel::download(new CantonExport(), 'reporte_cantones.xlsx');
    }

    public function printParroquia(){
        return Excel::download(new ParroquiaExport(), 'reporte_parroquias.xlsx');
    }

    public function printSector(){
        return Excel::download(new SectorExport(), 'reporte_sectores.xlsx');
    }

    public function printCreditoMotivo(){
        return Excel::download(new CreditoMotivoExport(), 'reporte_motivo_credito.xlsx');
    }

    public function printCreditoDias(){
        return Excel::download(new CreditoDiaExport(), 'reporte_dias_credito.xlsx');
    }

    public function printProductos(){
        return Excel::download(new ProductExport(), 'reporte_productos.xlsx');
    }

    public function printUsuarios(){
        return Excel::download(new UsuarioExport(), 'reporte_usuarios.xlsx');
    }

    public function printReportesVentas(){
        return Excel::download(new ReporteVentaExport(), 'reporte_ventas.xlsx');
    }
}
