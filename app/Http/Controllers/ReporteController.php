<?php

namespace App\Http\Controllers;

use App\Client;
use App\EstadoPago;
use App\RegistroPago;
use App\SalesDetail;
use App\SalesHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReporteController extends Controller
{
    public function showHistorialCrediticio(){
        $pagos = RegistroPago::where('is_pagado', 1)->orderBy('client_id', 'asc')->get();
        $pagos = $pagos->groupBy('client_id');

        $misEstadosPagos = EstadoPago::orderBy('ponderacion', 'desc')->get();

        return view('reporte-historial-crediticio', compact('pagos','misEstadosPagos'));
    }

    public function showVentas(){
        $ventas = SalesHeader::all();
        return view('reporte-ventas', compact('ventas'));
    }

    public function graficoCalidadCrediticiaPagos(){
        $dateCurrent = Carbon::now();
        $dateInicio = Carbon::parse($dateCurrent->year . "-01-01");
        $dateFin = Carbon::parse($dateCurrent->year . "-12-31");

        $misEstadosPagos = EstadoPago::orderBy('ponderacion', 'desc')->get();
        $totalNumeroDePagos = RegistroPago::where('is_pagado', 1)->whereBetween('fecha_realiza_pago', [$dateInicio, $dateFin])->count();
        $pagosConsolidadosPorCalificacion = RegistroPago::selectRaw('count(*) as total, estado_pago_id')
            ->where('is_pagado', 1)
            ->whereBetween('fecha_realiza_pago', [$dateInicio, $dateFin])
            ->groupBy('estado_pago_id')->get();

        foreach ($pagosConsolidadosPorCalificacion as $pagos){
            $porcentaje = (100 * $pagos->total) / $totalNumeroDePagos;
            $pagos->porcentaje = round($porcentaje, 2);
        }

        return view('reporte-grafico-calidad-pagos', compact('misEstadosPagos', 'totalNumeroDePagos', 'pagosConsolidadosPorCalificacion', 'dateInicio', 'dateFin'));
    }

    public function graficoCalidadCrediticiaPagosPorFecha(Request $request){
        $dateInicio = Carbon::createFromFormat('d/m/Y', $request->fechaInicio)->isoFormat('YYYY-MM-DD');
        $dateFin = Carbon::createFromFormat('d/m/Y', $request->fechaFin)->isoFormat('YYYY-MM-DD');

        $misEstadosPagos = EstadoPago::orderBy('ponderacion', 'desc')->get();
        $totalNumeroDePagos = RegistroPago::where('is_pagado', 1)->whereBetween('fecha_realiza_pago', [$dateInicio, $dateFin])->count();
        $pagosConsolidadosPorCalificacion = RegistroPago::selectRaw('count(*) as total, estado_pago_id')
            ->where('is_pagado', 1)
            ->whereBetween('fecha_realiza_pago', [$dateInicio, $dateFin])
            ->groupBy('estado_pago_id')->get();

        foreach ($pagosConsolidadosPorCalificacion as $pagos){
            $porcentaje = (100 * $pagos->total) / $totalNumeroDePagos;
            $pagos->porcentaje = round($porcentaje, 2);
        }

        $answer = array(
            "estadosPagos" => $misEstadosPagos,
            "numerosPago" => $totalNumeroDePagos,
            "pagoConsolidadoPorCalificacion" => $pagosConsolidadosPorCalificacion,
        );

        return json_encode($answer);
    }

    public function graficoVentasPago(){
        $dateCurrent = Carbon::now();
        $dateInicio = Carbon::parse($dateCurrent->year . "-01-01");
        $dateFin = Carbon::parse($dateCurrent->year . "-12-31");

        $ventas = SalesDetail::selectRaw('SUM(total) as total_venta, MONTH(created_at) mes_venta')->groupBy('mes_venta')->get();
        $pagos = RegistroPago::selectRaw('SUM(valor_pago) as total_pago, MONTH(created_at) mes_pago')->groupBy('mes_pago')->get();

        $ventas_pagos = array();

        foreach ($ventas as $v){
            $valor_pago = 0;

            foreach ($pagos as $p){
                if($p->mes_pago == $v->mes_venta){
                    $valor_pago = $p->total_pago;
                }
            }

            $ventas_pagos[] = array('mes' => $v->mes_venta, 'mes_nombre' => $this->getMonthName($v->mes_venta), 'ventas' => $v->total_venta, 'pagos' => $valor_pago);

        }
        $ventas_pagos = json_encode($ventas_pagos);

        return view('reporte-grafico-ventas-pagos', compact('ventas_pagos', 'dateInicio', 'dateFin'));
    }

    public function graficoVentasPagoPorFecha(Request $request){
        $dateInicio = Carbon::createFromFormat('d/m/Y', $request->fechaInicio)->isoFormat('YYYY-MM-DD');
        $dateFin = Carbon::createFromFormat('d/m/Y', $request->fechaFin)->isoFormat('YYYY-MM-DD');

        $ventas = SalesDetail::selectRaw('SUM(total) as total_venta, MONTH(created_at) mes_venta')->whereBetween('created_at', [$dateInicio, $dateFin])->groupBy('mes_venta')->get();
        $pagos = RegistroPago::selectRaw('SUM(valor_pago) as total_pago, MONTH(created_at) mes_pago')->whereBetween('fecha_realiza_pago', [$dateInicio, $dateFin])->groupBy('mes_pago')->get();

        $ventas_pagos = array();

        foreach ($ventas as $v){
            $valor_pago = 0;

            foreach ($pagos as $p){
                if($p->mes_pago == $v->mes_venta){
                    $valor_pago = $p->total_pago;
                }
            }

            $ventas_pagos[] = array('mes' => $v->mes_venta, 'mes_nombre' => $this->getMonthName($v->mes_venta), 'ventas' => $v->total_venta, 'pagos' => $valor_pago);

        }

        $answer = array(
            "ventas_pagos" => $ventas_pagos,
        );

        return json_encode($answer);
    }

    public function getMonthName($monthNumber){
        switch ($monthNumber){
            case 1:
                return "Enero";
                break;
            case 2:
                return "Febrero";
                break;
            case 3:
                return "Marzo";
                break;
            case 4:
                return "Abril";
                break;
            case 5:
                return "Mayo";
                break;
            case 6:
                return "Junio";
                break;
            case 7:
                return "Julio";
                break;
            case 8:
                return "Agosto";
                break;
            case 9:
                return "Septiembre";
                break;
            case 10:
                return "Octubre";
                break;
            case 11:
                return "Noviembre";
                break;
            default:
                return "Diciembre";
                break;
        }
    }
}
