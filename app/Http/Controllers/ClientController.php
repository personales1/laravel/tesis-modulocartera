<?php

namespace App\Http\Controllers;

use App\Area;
use App\Cargo;
use App\Client;
use App\ClientDocument;
use App\ClientWork;
use App\Mail\ClientMailable;
use App\Province;
use App\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ClientController extends Controller
{
    public function show(){
        $clientes = Client::all();
        return view('cliente-list', ['clientes'=>$clientes]);
    }

    public function showForm($id){
        $clientes = Client::find($id);
        $provincia = Province::pluck('nombre','id');
        $sector = Sector::pluck('nombre','id');
        $cargo = Cargo::pluck('nombre','cargo_id');
        $area = Area::pluck('nombre','area_id');
        $imageIdentificacion = ClientDocument::where([['client_id', $id], ['foto_tag', 'IDENTIFICACION']])->get();
        $imageRoles = ClientDocument::where([['client_id', $id], ['foto_tag', 'ROLES_PAGO']])->get();
        $imageOtros = ClientDocument::where([['client_id', $id], ['foto_tag', 'OTROS']])->get();

        if($clientes == null){
            return view('error404');
        }

        return view('cliente-new', compact('clientes','provincia','sector', 'cargo', 'area', 'imageIdentificacion', 'imageRoles', 'imageOtros'));
    }

    public function create(){
        $provincia = Province::pluck('nombre','id');
        $sector = Sector::pluck('nombre','id');
        $cargo = Cargo::pluck('nombre','cargo_id');
        $area = Area::pluck('nombre','area_id');
        return view('cliente-new', compact('provincia','sector', 'cargo', 'area'));
    }

    public function store(Request $request, $id = null){
        $envioEmail = false;

        $request->validate([
            'name' => 'required',
            'apellido' => 'required',
            'fechaNacimiento' => 'required',
            'correo' => 'required',
            'celular' => 'required',
            'dirección' => 'required',
            'referencia' => 'required',
            'provincia' => 'required',
            'canton' => 'required',
            'parroquia' => 'required',
            'sector' => 'required',
        ]);

        if($id == null){ /*VALIDAR CÉDULA SI SE VA CREAR NUEVO CLIENTE*/
            $request->validate([
                'identificación' => 'bail|required|unique:clients,identificacion|max:10',
            ]);
        }else{
            $existCliente = Client::where([['id', $id], ['identificacion', $request->identificación]])->first();

            if($existCliente == null){ /*VALIDAR CÉDULA SI SE MODIFICA CÉDULA DEL CLIENTE*/
                $request->validate([
                    'identificación' => 'bail|required|unique:clients,identificacion|max:10',
                ]);
            }
        }

        /*
         * 'fileIdentificacion1' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
         * 'fileRoles1' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
         */

        if(isset($id)){
            $cliente = Client::find($id);
            $clienteWorks = ClientWork::where('client_id', $id)->first();
            $messageSuccess = config('constant.alert.success.update');
        }else{
            $cliente = new Client();
            $clienteWorks = new ClientWork();
            $messageSuccess = "El cliente ". $request->name . " " . $request->apellido . " se ha creado satisfactoriamente.";
            $envioEmail = true;
        }

        $cliente->nombres = $request->name;
        $cliente->apellidos = $request->apellido;
        $cliente->identificacion = $request->identificación;
        $cliente->fec_nac = $request->fechaNacimiento;
        $cliente->email = $request->correo;
        $cliente->telefono = ($request->telefono == null) ? "" : $request->telefono;
        $cliente->celular = $request->celular;
        $cliente->sexo = $request->sexo;
        $cliente->direccion = $request->dirección;
        $cliente->direccion_referencia = $request->referencia;
        $cliente->province_id = $request->provincia;
        $cliente->canton_id = $request->canton;
        $cliente->parroquia_id = $request->parroquia;
        $cliente->sector_id = $request->sector;
        $cliente->save();



        $clienteWorks->client_id = $cliente->id;
        $clienteWorks->empresa = $request->empresa;
        $clienteWorks->fec_ing = $request->fechaIngreso;
        $clienteWorks->cargo_id = $request->cargo;
        $clienteWorks->cargo_descripcion = ($request->cargoDescripcion == null) ? "" : $request->cargoDescripcion;
        $clienteWorks->area_id = $request->area;
        $clienteWorks->area_descripcion = ($request->areaDescripcion == null) ? "" : $request->areaDescripcion;
        $clienteWorks->telefono = ($request->telefonoTrabajo == null) ? "" : $request->telefonoTrabajo;
        $clienteWorks->extension = ($request->extension == null) ? "" : $request->extension;
        $clienteWorks->direccion = $request->direccionTrabajo;
        $clienteWorks->direccion_referencia = $request->referenciaTrabajo;
        $clienteWorks->ingreso_periodo = $request->periodoIngreso;
        $clienteWorks->ingreso_promedio_mensual = $request->promedioIngreso;
        $clienteWorks->gastos_promedio_mensual = $request->promedioGasto;
        $clienteWorks->save();


        /***
         * Guardar imágenes de documentos de identificación
         */
        for($i=1; $i<=3; $i++){
            $fileIdentificacion = $request->file('fileIdentificacion' . $i);

            if(isset($fileIdentificacion)){
                $destinationPath = 'image_system/clientes/documentos/identificacion/' . $request->identificación . "/";
                $nameImage = $request->identificación . "_" . $i . "_" . date('YmdHis') . "." . $fileIdentificacion->getClientOriginalExtension();
                $fileIdentificacion->move($destinationPath, $nameImage);

                $clienteDocument = new ClientDocument();
                $clienteDocument->client_id = $cliente->id;
                $clienteDocument->foto = $nameImage;
                $clienteDocument->foto_tag = "IDENTIFICACION";
                $clienteDocument->save();
            }
        }

        /***
         * Guardar imágenes de documentos de roles de pago
         */
        for($i=1; $i<=3; $i++){
            $fileRoles = $request->file('fileRoles' . $i);

            if(isset($fileRoles)){
                $destinationPath = 'image_system/clientes/documentos/roles/' . $request->identificación . "/";
                $nameImage = $request->identificación . "_" . $i . "_" . date('YmdHis') . "." . $fileRoles->getClientOriginalExtension();
                $fileRoles->move($destinationPath, $nameImage);

                $clienteDocument = new ClientDocument();
                $clienteDocument->client_id = $cliente->id;
                $clienteDocument->foto = $nameImage;
                $clienteDocument->foto_tag = "ROLES_PAGO";
                $clienteDocument->save();
            }
        }

        /***
         * Guardar imágenes de documentos de otros documentos
         */
        for($i=1; $i<=3; $i++){
            $fileOtros = $request->file('fileOtros' . $i);

            if(isset($fileOtros)){
                $destinationPath = 'image_system/clientes/documentos/otros/' . $request->identificación . "/";
                $nameImage = $request->identificación . "_" . $i . "_" . date('YmdHis') . "." . $fileOtros->getClientOriginalExtension();
                $fileOtros->move($destinationPath, $nameImage);

                $clienteDocument = new ClientDocument();
                $clienteDocument->client_id = $cliente->id;
                $clienteDocument->foto = $nameImage;
                $clienteDocument->foto_tag = "OTROS";
                $clienteDocument->save();
            }
        }

        if($envioEmail){
            Mail::to($cliente->email)->send(new ClientMailable($cliente));
        }

        return redirect()->route('cliente.list')->with(['success'=> $messageSuccess]);
    }

    public function changeState($id){
        $estado_succes = 200;
        $estado_error_general = 400;

        $producto = Client::find($id);

        if($producto != null){
            if($producto->is_activo == 0){
                $producto->is_activo = 1;
                $estadoRegistro = "Activo";
                $clase = "btn-success";
            }
            else{
                $producto->is_activo = 0;
                $estadoRegistro = "Inactivo";
                $clase = "btn-danger";
            }

            $answer = array(
                "estado" => $estado_succes,
                "data" => array(
                    "mensaje" => config('constant.alert.success.state'),
                    "user_estado" => $estadoRegistro,
                    "user_clase" => $clase,
                ),
            );

            $producto->save();
        }else{
            $answer = array(
                "estado" => $estado_error_general,
                "data" => array(
                    "mensaje" => config('constant.alert.error.not_found'),
                    "user_estado" => "",
                    "user_clase" => "",
                ),
            );
        }

        return json_encode($answer);
    }
}
