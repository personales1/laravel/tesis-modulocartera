<?php

namespace App\Http\Controllers;

use App\Client;
use App\CreditDay;
use App\EstadoPago;
use App\Mail\ClientRegistroPagoCreditoMailable;
use App\Mail\ClientVentaCreditoMailable;
use App\Product;
use App\RegistroPago;
use App\SalesDetail;
use App\SalesHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class VentaController extends Controller
{
    public function generaVentaHeader(Request $request){
        $salesHeader = new SalesHeader();

        $salesHeader->num_factura = $request->numFactura;
        $salesHeader->client_id = $request->clientId;
        $salesHeader->credit_reason_id = $request->creditReasonId;
        $salesHeader->observacion = $request->observacion;
        $salesHeader->save();
        return $salesHeader->id;
    }

    public function generaVentaDetail(Request $request){
        $productos = Product::find($request->productId);
        $total = $request->cantidad * $productos->precio;

        $salesDetail = new SalesDetail();

        $salesDetail->sales_header_id = $request->salesHeaderId;
        $salesDetail->product_id = $request->productId;
        $salesDetail->cantidad = $request->cantidad;
        $salesDetail->subtotal = $request->subtotal;
        $salesDetail->iva = $request->iva;
        $salesDetail->total = $total;

        $salesDetail->save();
        return "ok";
    }

    public function generaPrimerRegistroPago(Request $request){
        $ventaHeader = SalesHeader::find($request->salesHeaderId);
        $creditDay = CreditDay::find($request->creditDaysId);

        $ventaDetalle = SalesDetail::where('sales_header_id', $request->salesHeaderId)->get();
        $total = 0;

        foreach ($ventaDetalle as $detalle){
            $total += $detalle->total;
        }

        $fechaCurrent = Carbon::now()->startOfDay();
        $fechaDePago = $fechaCurrent->addDays($creditDay->descripcion);

        $registroPago = new RegistroPago();
        $registroPago->sales_header_id = $request->salesHeaderId;
        $registroPago->client_id = $ventaHeader->client_id;
        $registroPago->total = $total;
        $registroPago->valor_pago = 0;
        $registroPago->saldo = $total;
        $registroPago->is_pagado = 0;
        $registroPago->credito_dia = $creditDay->descripcion;
        $registroPago->credito_dia_periodo = "DIAS";
        $registroPago->fecha_pago_credito = $fechaDePago;

        $registroPago->save();

        Mail::to($ventaHeader->client->email)->send(new ClientVentaCreditoMailable($ventaHeader, $registroPago));
        return "ok";
    }

    public function showPagosPendientes(){
        $ventas = RegistroPago::where('is_pagado', 0)->orderBy('fecha_pago_credito', 'asc')->get();
        return view('ventas-cobros-list', ['ventas'=>$ventas]);
    }

    public function registrarPago(Request $request){
        $estadoOk = 200;
        $estadoError = 400;

        $registroPago = RegistroPago::find($request->pagoId);

        if($request->pagoValor > $registroPago->saldo){
            $estado = $estadoError;
            $mensaje = "El valor a pagar no puede ser mayor al saldo pendiente de pago.";
        }
        else{
            $fechaCurrent = Carbon::now();
            $fechaCurrentSoloDate = $fechaCurrent->startOfDay();

            $fechaDePagoDelCredito = Carbon::parse($registroPago->fecha_pago_credito);
            $valorPagar = $request->pagoValor;
            $saldo = $registroPago->total - $valorPagar;

            if($fechaDePagoDelCredito->diffInDays($fechaCurrentSoloDate) == 0){
                if($saldo == 0){
                    $estadoPago = EstadoPago::where('descripcion','FECHA DE PAGO')->first();
                }else{
                    $estadoPago = EstadoPago::where('descripcion','FECHA DE PAGO - INCOMPLETO')->first();
                }
            }
            else if($fechaDePagoDelCredito->gt($fechaCurrentSoloDate)){
                if($fechaDePagoDelCredito->diffInDays($fechaCurrentSoloDate) <= 3){
                    $estadoPago = EstadoPago::where('descripcion','POR ADELANTADO - FECHA CERCANA')->first();
                }else{
                    $estadoPago = EstadoPago::where('descripcion','POR ADELANTADO')->first();
                }
            }
            else{
                if($fechaDePagoDelCredito->diffInDays($fechaCurrentSoloDate) <= 3){
                    if($saldo == 0){
                        $estadoPago = EstadoPago::where('descripcion','FUERA DE TIEMPO')->first();
                    }else{
                        $estadoPago = EstadoPago::where('descripcion','FUERA DE TIEMPO - INCOMPLETO')->first();
                    }
                }else{
                    if($saldo == 0){
                        $estadoPago = EstadoPago::where('descripcion','ATRASADO')->first();
                    }else{
                        $estadoPago = EstadoPago::where('descripcion','ATRASADO - INCOMPLETO')->first();
                    }
                }
            }

            $registroPago->valor_pago = $valorPagar;
            $registroPago->saldo = $saldo;
            $registroPago->is_pagado = 1;
            $registroPago->fecha_realiza_pago = $fechaCurrent;
            $registroPago->estado_pago_id = $estadoPago->id;
            $registroPago->save();

            //En caso de que no se pagu el valor de la factura, genero otro registrro de pago con valor de is_pagado = 0.
            if($valorPagar < $registroPago->total){
                $nuevoRegistroPago = new RegistroPago();
                $nuevoRegistroPago->sales_header_id = $registroPago->sales_header_id;
                $nuevoRegistroPago->client_id = $registroPago->client_id;
                $nuevoRegistroPago->total = $saldo;
                $nuevoRegistroPago->valor_pago = 0;
                $nuevoRegistroPago->saldo = $saldo;
                $nuevoRegistroPago->is_pagado = 0;
                $nuevoRegistroPago->credito_dia = $registroPago->credito_dia;
                $nuevoRegistroPago->credito_dia_periodo = $registroPago->credito_dia_periodo;
                $nuevoRegistroPago->fecha_pago_credito = $fechaDePagoDelCredito;
                $nuevoRegistroPago->save();
            }

            $estado = $estadoOk;
            $mensaje = "El pago de $$valorPagar ha sido registrado exitosamente";

            Mail::to($registroPago->client->email)->send(new ClientRegistroPagoCreditoMailable($registroPago));
        }

        $answer = array(
            "estado" => $estado,
            "data" => array(
                "mensaje" => $mensaje
            ),
        );

        return json_encode($answer);



        //return "P";
    }

    public function getDetalleVentasPorCliente($id){
        $cliente = Client::find($id);
        $ventasPagadas = RegistroPago::where([['is_pagado', 1], ['client_id', $id]])->orderBy('sales_header_id')->orderBy('fecha_realiza_pago')->get();
        $ventasNoPagadas = RegistroPago::addSelect([
                    'totalfac' => SalesDetail::select(DB::raw("SUM(total)"))->whereColumn('sales_header_id', 'registro_pagos.sales_header_id')
                ])->where([['is_pagado', 0], ['client_id', $id]])->get();

        if($cliente == null){
            return view('error404');
        }

        return view('ventas-cliente-detalle', compact('cliente', 'ventasPagadas','ventasNoPagadas'));
    }

    public function verificaPagoPendientePorCliente($id){
        $pagosPendientes = RegistroPago::where([['client_id', $id], ['is_pagado', 0]])->get();

        if(count($pagosPendientes) > 0){
            return 1;
        }else{
            return 0;
        }
    }
}
