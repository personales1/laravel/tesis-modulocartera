<?php

namespace App\Http\Controllers;

use App\Client;
use App\Sector;
use App\User;
use Illuminate\Http\Request;

class SectorController extends Controller
{
    public function show(){
        $sectores = Sector::all();
        return view('sector-list', ['sectores'=>$sectores]);
    }

    public function showForm($id){
        $sectores = Sector::find($id);

        if($sectores == null){
            return view('error404');
        }

        return view('sector-new', ['sectores'=>$sectores]);
    }

    public function destroy($id){
        $sector = Sector::find($id);
        $sector->delete();
        return config('constant.alert.success.delete');
    }

    public function store(Request $request, $id = null){
        $request->validate([
            'name' => 'required',
        ]);

        if(isset($id)){
            $sector = Sector::find($id);
            $messageSuccess = config('constant.alert.success.update');
        }else{
            $sector = new Sector;
            $messageSuccess = "El sector ". $request->name . " se ha creado satisfactoriamente.";
        }

        $sector->nombre = $request->name;
        $sector->save();

        return redirect()->route('sector.list')->with(['success'=> $messageSuccess]);
    }

    public function changeState($id){
        $estado_succes = 200;
        $estado_error_general = 400;

        $sector = Sector::find($id);

        if($sector != null){
            if($sector->is_activo == 0){

                $sector->is_activo = 1;
                $estadoRegistro = "Activo";
                $clase = "btn-success";
                $sector->save();

                $answer = array(
                    "estado" => $estado_succes,
                    "data" => array(
                        "mensaje" => config('constant.alert.success.state'),
                        "user_estado" => $estadoRegistro,
                        "user_clase" => $clase,
                    ),
                );
            }
            else{
                $user = Client::where('sector_id', $id)->first();

                if($user == null){
                    $sector->is_activo = 0;
                    $estadoRegistro = "Inactivo";
                    $clase = "btn-danger";
                    $sector->save();

                    $answer = array(
                        "estado" => $estado_succes,
                        "data" => array(
                            "mensaje" => config('constant.alert.success.state'),
                            "user_estado" => $estadoRegistro,
                            "user_clase" => $clase,
                        ),
                    );
                }
                else{
                    //No inactivar registro, ya qu está relacionado con otras tablas.
                    $answer = array(
                        "estado" => $estado_error_general,
                        "data" => array(
                            "mensaje" => config('constant.alert.error.change_state_by_join'),
                            "user_estado" => "",
                            "user_clase" => "",
                        ),
                    );
                }
            }
        }else{
            $answer = array(
                "estado" => $estado_error_general,
                "data" => array(
                    "mensaje" => config('constant.alert.error.not_found'),
                    "user_estado" => "",
                    "user_clase" => "",
                ),
            );
        }

        return json_encode($answer);
    }
}
