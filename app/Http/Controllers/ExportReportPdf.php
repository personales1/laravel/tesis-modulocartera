<?php

namespace App\Http\Controllers;

use App\Canton;
use App\Client;
use App\CreditDay;
use App\CreditReason;
use App\EstadoPago;
use App\Parroquia;
use App\Product;
use App\Province;
use App\RegistroPago;
use App\SalesHeader;
use App\Sector;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;

class ExportReportPdf extends Controller
{
    public function printHistorialCrediticio()
    {
        $registroPagos = RegistroPago::where('is_pagado', 1)->orderBy('client_id', 'asc')->get();
        $clientesPagos = $registroPagos->groupBy('client_id');

        view()->share('pagos', $clientesPagos);

        $pdf = PDF::loadView('pdf-reporte-historial-crediticio');
        return $pdf->download('reporte_historial_crediticio.pdf');
    }

    public function printCliente()
    {
        $clientes = Client::all();
        view()->share('clientes', $clientes);

        $pdf = PDF::loadView('pdf-reporte-clientes');
        return $pdf->download('reporte_clientes.pdf');
    }

    public function printProvincia()
    {
        $provincias = Province::all();
        view()->share('provincias', $provincias);

        $pdf = PDF::loadView('pdf-reporte-provincias');
        return $pdf->download('reporte_provincias.pdf');
    }

    /**
     * @return mixed
     */
    public function printCanton()
    {
        $cantones = Canton::all();
        view()->share('cantones', $cantones);

        $pdf = PDF::loadView('pdf-reporte-canton');
        return $pdf->download('reporte_cantones.pdf');
    }

    public function printParroquia()
    {
        $parroquias = Parroquia::all();
        view()->share('parroquias', $parroquias);

        $pdf = PDF::loadView('pdf-reporte-parroquia');
        return $pdf->download('reporte_parroquias.pdf');
    }

    public function printSector()
    {
        $sectores = Sector::all();
        view()->share('sectores', $sectores);

        $pdf = PDF::loadView('pdf-reporte-sector');
        return $pdf->download('reporte_sectores.pdf');
    }

    public function printCreditoMotivo()
    {
        $credits = CreditReason::all();
        view()->share('credits', $credits);

        $pdf = PDF::loadView('pdf-reporte-credito-motivo');
        return $pdf->download('reporte_motivo_credito.pdf');
    }

    public function printCreditoDias()
    {
        $credits = CreditDay::all();
        view()->share('credits', $credits);

        $pdf = PDF::loadView('pdf-reporte-credito-dias');
        return $pdf->download('reporte_dias_credito.pdf');
    }

    public function printProductos()
    {
        $productos = Product::all();
        view()->share('productos', $productos);

        $pdf = PDF::loadView('pdf-reporte-productos');
        return $pdf->download('reporte_productos.pdf');
    }

    public function printUsuarios()
    {
        $users = User::all();
        view()->share('users', $users);

        $pdf = PDF::loadView('pdf-reporte-usuarios');
        return $pdf->download('reporte_usuarios.pdf');
    }

    public function printReportesVentas(){
        $ventas = SalesHeader::all();
        view()->share('ventas', $ventas);

        $pdf = PDF::loadView('pdf-reporte-ventas');
        return $pdf->download('reporte_ventas.pdf');
    }
}
