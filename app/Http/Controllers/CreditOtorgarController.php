<?php

namespace App\Http\Controllers;

use App\Client;
use App\CreditDay;
use App\CreditReason;
use App\Product;
use Illuminate\Http\Request;

class CreditOtorgarController extends Controller
{
    //Mostrar clientes que no tengan crédito aprobado
    public function show(){
        $clientes = Client::where([['is_credito_aprobado', 1], ['is_activo', 1]])->get();
        $productos = Product::where('is_activo', 1)->get();
        $creditDay = CreditDay::where('is_activo', 1)->pluck('descripcion','id');
        $creditReason = CreditReason::where('is_activo', 1)->pluck('descripcion','id');

        return view('credito-otorgar', compact('clientes', 'productos', 'creditDay', 'creditReason'));
    }
}
