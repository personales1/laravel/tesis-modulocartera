<?php

namespace App\Http\Controllers;

use App\Mail\EmailNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DemoEmailController extends Controller
{
    public function send(){
        $user = User::find(1);

        Mail::to("seicarse@gmail.com")->send(new EmailNotification($user));
    }
}
