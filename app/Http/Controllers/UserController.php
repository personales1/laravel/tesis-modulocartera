<?php

namespace App\Http\Controllers;

use App\Mail\UserMailable;
use App\Role;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function show(){
        $users = User::all();
        return view('admin-user-list', ['users'=>$users]);
    }

    public function showForm($id){
        $users = User::find($id);
        $rol = Role::pluck('nombre','rol_id');

        if($users == null){
            return view('error404');
        }

        return view('admin-user-new', compact('users', 'rol'));
    }

    public function showProfile(){
        $user = Auth::user();
        $rol = Role::pluck('nombre','rol_id');
        return view('admin-user-profile', compact('user', 'rol'));
    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();
        return config('constant.alert.success.delete');
    }

    public function changeState($id){
        $user = User::find($id);
        if($user->is_activo){
            $user->is_activo = 0;
            $estadoRegistro = "Inactivo";
            $clase = "btn-danger";
        }else{
            $user->is_activo = 1;
            $estadoRegistro = "Activo";
            $clase = "btn-success";
        }
        $user->save();

        $answer = array(
            "estado" => 200,
            "data" => array(
                "mensaje" => config('constant.alert.success.state'),
                "user_estado" => $estadoRegistro,
                "user_clase" => $clase,
            ),
        );

        return json_encode($answer);
    }

    public function updatePassword(Request $request){
        $estadoOk = 200;
        $estadoError = 400;
        $estadoErrorAuthentication = 401;

        if(Auth::check()){
            $user = Auth::user();
            $userProfile = UserProfile::where('identificacion', $request->identificacion)->first();

            if($userProfile == null || $user->id != $userProfile->user_id){
                $estado = $estadoError;
                $mensaje = "Cédula ingresada no corresponde con el usuario que ha iniciado sesión.";
                $mensajeEstado = "general";
            }
            else if($request->password == ""){
                $estado = $estadoError;
                $mensaje = "Campo obligatorio";
                $mensajeEstado = "password";
            }
            else if($request->confirmPassword == ""){
                $estado = $estadoError;
                $mensaje = "Campo obligatorio";
                $mensajeEstado = "confirmPassword";
            }
            else if($request->password != $request->confirmPassword){
                $estado = $estadoError;
                $mensaje = "Las contraseñas ingresadas no coinciden.";
                $mensajeEstado = "confirmPassword";
            }
            else if(Hash::check($request->password, $user->password)){
                $estado = $estadoError;
                $mensaje = "La nueva contraseña ingresada no puede ser igual a la contraseña temporal.";
                $mensajeEstado = "general";
            }
            else{
                /*** CUANDO LA TRANSACCIÓN ES EXITOSA ***/
                $user = User::find($userProfile->user_id);
                $user->password_update = 1;
                $user->password =  bcrypt($request->password);
                $user->save();

                $estado = $estadoOk;
                $mensaje = "La actualización de su oontraseña fue exitosa.";
                $mensajeEstado = "";
            }
        }
        else{
            $estado = $estadoErrorAuthentication;
            $mensaje = "Tiene problemas con el inicio de sesión.";
            $mensajeEstado = "general";
        }

        $answer = array(
            "estado" => $estado,
            "data" => array(
                "mensaje" => $mensaje,
                "mensaje_estado" => $mensajeEstado,
            ),
        );

        return json_encode($answer);
    }

    public function changePassword(Request $request){
        $estadoOk = 200;
        $estadoError = 400;
        $estadoErrorAuthentication = 401;

        if(Auth::check()){
            $user = Auth::user();

            if(empty($request->passwordCurrent)){
                $estado = $estadoError;
                $mensaje = "Campo obligatorio";
            }
            else if($request->passwordNew == ""){
                $estado = $estadoError;
                $mensaje = "Campo obligatorio";
            }
            else if($request->passwordConfirm == ""){
                $estado = $estadoError;
                $mensaje = "Campo obligatorio";
            }
            else if($request->passwordNew != $request->passwordConfirm){
                $estado = $estadoError;
                $mensaje = "Las contraseñas ingresadas no coinciden.";
            }
            /*
            else if(! Hash::check($request->passwordCurrent, $user->password)){
                $estado = $estadoError;
                $mensaje = "La contraseña actual ingresada es incorrecta, por favor verifique su contraseña y vuelva a intentar.";
            }
            */
            else if(Hash::check($request->passwordNew, $user->password)){
                $estado = $estadoError;
                $mensaje = "La nueva contraseña ingresada no puede ser igual a la contraseña actual, por favor intente ingresar otra contraseña.";
            }
            else if(strlen(trim($request->passwordNew)) < 5){
                $estado = $estadoError;
                $mensaje = "La contraseña es muy corta, al menos debe tener 5 dígitos.";
            }
            else{
                /*** CUANDO LA TRANSACCIÓN ES EXITOSA ***/
                $user->password =  bcrypt($request->passwordNew);
                $user->save();

                $estado = $estadoOk;
                $mensaje = "El cambio de su contraseña fue exitosa.";
            }
        }
        else{
            $estado = $estadoErrorAuthentication;
            $mensaje = "Tiene problemas con el inicio de sesión.";
        }

        $answer = array(
            "estado" => $estado,
            "data" => array(
                "mensaje" => $mensaje
            ),
        );

        return json_encode($answer);
    }

    public function create(){
        $rol = Role::pluck('nombre','rol_id');
        return view('admin-user-new', compact('rol'));
    }

    public function store(Request $request, $id = null){
        /***
         * Si $id es vacio o null es poque es creación de nuevo usuario.
         * Si $id tiene algún valor (es decir es NO VACÏO), se trata de un actualización.
         */
        $estadoOk = 200;
        $estadoError = 400;
        $data = "";
        $envioEmail = false;

        if(empty($request->name)){
            $estado = $estadoError;
            $data = array(
                "mensaje" => "Ingrese al menos un nombre"
            );
        }
        elseif (strlen(trim($request->name)) < 3){
            $estado = $estadoError;
            $data = array(
                "mensaje" => "Nombre tiene pocos caracteres"
            );
        }
        elseif(empty($request->apellido)){
            $estado = $estadoError;
            $data = array(
                "mensaje" => "Ingrese al menos un apellido"
            );
        }
        elseif(strlen(trim($request->apellido)) < 3){
            $estado = $estadoError;
            $data = array(
                "mensaje" => "Apellido tiene pocos caracteres"
            );
        }
        elseif(empty($request->correo)){
            $estado = $estadoError;
            $data = array(
                "mensaje" => "Ingrese un correo electrónico"
            );
        }
        elseif(empty($request->identificación)){
            $estado = $estadoError;
            $data = array(
                "mensaje" => "Ingrese el número de identificación"
            );
        }
        elseif(strlen(trim($request->identificación)) < 10){
            $estado = $estadoError;
            $data = array(
                "mensaje" => "Identificación debe tener mínimo 10 caracteres"
            );
        }
        elseif(empty($request->teléfono)){
            $estado = $estadoError;
            $data = array(
                "mensaje" => "Ingrese el número de teléfono"
            );
        }
        elseif(strlen(trim($request->teléfono)) < 7){
            $estado = $estadoError;
            $data = array(
                "mensaje" => "Teléfono debe tener mínimo 7 caracteres"
            );
        }
        else{
            $hayError = false;

            if(empty($id)){
                if(empty($request->password)){
                    $estado = $estadoError;
                    $data = array(
                        "mensaje" => "Ingrese la contraseña"
                    );
                    $hayError = true;
                }
                elseif(strlen(trim($request->password)) < 5){
                    $estado = $estadoError;
                    $data = array(
                        "mensaje" => "Contraseña debe tener mínimo 5 caracteres"
                    );
                    $hayError = true;
                }
            }

            $userValidation = User::where('email', $request->correo)->first();
            if($userValidation != null){
                //si es creación de usuario O si es modificación y el correo existe en otro usuario
                if(empty($id) || (! empty($id) && $userValidation->id != $id)){
                    $estado = $estadoError;
                    $data = array(
                        "mensaje" => "Ya existe un operador creado con el correo electrónico $request->correo"
                    );
                    $hayError = true;
                }
            }

            $userProfileValidation = UserProfile::where('identificacion', $request->identificación)->first();
            if($userProfileValidation != null){
                if(empty($id) || (! empty($id) && $userProfileValidation->id != $id)){
                    $estado = $estadoError;
                    $data = array(
                        "mensaje" => "Ya existe un operador creado con el número de identificación $request->identificación"
                    );
                    $hayError = true;
                }
            }

            if(! $hayError){
                if(empty($id)){
                    $user = new User();
                    $user->password =  bcrypt($request->password);
                    $userProfile = new UserProfile();
                    $messageSuccess = "El usuario ". $request->name . " " . $request->apellido . " se ha creado satisfactoriamente.";
                    $envioEmail = true;
                }
                else{
                    $user = User::find($id);
                    $userProfile = UserProfile::where('user_id', $id)->first();
                    $messageSuccess = config('constant.alert.success.update');
                }

                $estado = $estadoOk;
                $data = array(
                    "mensaje" => $messageSuccess
                );

                $user->email = $request->correo;
                $user->id_rol = $request->rol;
                $user->save();

                $userProfile->user_id = $user->id;
                $userProfile->nombres = $request->name;
                $userProfile->apellidos = $request->apellido;
                $userProfile->identificacion = $request->identificación;
                $userProfile->telefono = $request->teléfono;
                $userProfile->save();

                if($envioEmail){
                    $user->password =  $request->password;
                    Mail::to($user->email)->send(new UserMailable($user));
                }
            }
        }

        $answer = array(
            "estado" => $estado,
            "data" => $data,
        );

        return json_encode($answer);
    }
}
