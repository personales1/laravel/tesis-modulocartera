<?php

namespace App\Http\Controllers;

use App\Client;
use App\Parroquia;
use App\Province;
use Illuminate\Http\Request;

class ParroquiaController extends Controller
{
    public function show(){
        $parroquias = Parroquia::all();
        return view('parroquia-list', ['parroquias'=>$parroquias]);
    }

    public function showForm($id){
        $parroquias = Parroquia::find($id);
        $provincia = Province::pluck('nombre','id');

        if($parroquias == null){
            return view('error404');
        }

        return view('parroquia-new', compact('parroquias','provincia'));
    }

    public function destroy($id){
        $parroquias = Parroquia::find($id);
        $parroquias->delete();
        return config('constant.alert.success.delete');
    }

    public function create(){
        $provincia = Province::pluck('nombre','id');
        return view('parroquia-new', compact('provincia'));
    }

    public function store(Request $request, $id = null){
        $request->validate([
            'name' => 'required',
            'provincia' => 'required',
            'cantón' => 'required',
        ]);

        if(isset($id)){
            $parroquia = Parroquia::find($id);
            $messageSuccess = config('constant.alert.success.update');
        }
        else{
            $parroquia = new Parroquia();
            $messageSuccess = "La parroquia ". $request->name . " se ha creado satisfactoriamente.";
        }

        $parroquia->nombre = $request->name;
        $parroquia->province_id = $request->provincia;
        $parroquia->canton_id = $request->cantón;
        $parroquia->save();

        return redirect()->route('parroquia.list')->with(['success'=> $messageSuccess]);
    }

    public function getByCanton(Request $request){
        $parroquia = Parroquia::where('canton_id', $request->cantonId)->orderBy('id', 'asc')->get();

        $output = "";
        if($parroquia->isEmpty()){
            $output .= '<option value=""></option>';
        }else{
            foreach($parroquia as $row)
            {
                $output .= '<option value="'.$row->id.'">'.$row->nombre.'</option>';
            }
        }

        return $output;
    }

    public function changeState($id){
        $estado_succes = 200;
        $estado_error_general = 400;

        $parroquia = Parroquia::find($id);

        if($parroquia != null){
            if($parroquia->is_activo == 0){

                $parroquia->is_activo = 1;
                $estadoRegistro = "Activo";
                $clase = "btn-success";
                $parroquia->save();

                $answer = array(
                    "estado" => $estado_succes,
                    "data" => array(
                        "mensaje" => config('constant.alert.success.state'),
                        "user_estado" => $estadoRegistro,
                        "user_clase" => $clase,
                    ),
                );
            }
            else{
                $user = Client::where('parroquia_id', $id)->first();

                if($user == null){
                    $parroquia->is_activo = 0;
                    $estadoRegistro = "Inactivo";
                    $clase = "btn-danger";
                    $parroquia->save();

                    $answer = array(
                        "estado" => $estado_succes,
                        "data" => array(
                            "mensaje" => config('constant.alert.success.state'),
                            "user_estado" => $estadoRegistro,
                            "user_clase" => $clase,
                        ),
                    );
                }
                else{
                    //No inactivar registro, ya qu está relacionado con otras tablas.
                    $answer = array(
                        "estado" => $estado_error_general,
                        "data" => array(
                            "mensaje" => config('constant.alert.error.change_state_by_join'),
                            "user_estado" => "",
                            "user_clase" => "",
                        ),
                    );
                }
            }
        }else{
            $answer = array(
                "estado" => $estado_error_general,
                "data" => array(
                    "mensaje" => config('constant.alert.error.not_found'),
                    "user_estado" => "",
                    "user_clase" => "",
                ),
            );
        }

        return json_encode($answer);
    }
}
