<?php

namespace App\Http\Controllers;

use App\Canton;
use App\Client;
use App\Parroquia;
use App\Province;
use Illuminate\Http\Request;

class CantonController extends Controller
{
    public function show(){
        $cantones = Canton::all();
        return view('canton-list', ['cantones'=>$cantones]);
    }

    public function showForm($id){
        $cantones = Canton::find($id);
        $provincia = Province::pluck('nombre','id');

        if($cantones == null){
            return view('error404');
        }

        return view('canton-new', compact('cantones','provincia'));
    }

    public function destroy($id){
        $cantones = Canton::find($id);
        $cantones->delete();
        return config('constant.alert.success.delete');
    }

    public function create(){
        $provincia = Province::pluck('nombre','id');
        return view('canton-new', compact('provincia'));
    }

    public function store(Request $request, $id = null){
        $request->validate([
            'name' => 'required',
            'listProvince' => 'required',
        ]);

        if(isset($id)){
            $canton = Canton::find($id);
            $messageSuccess = config('constant.alert.success.update');
        }else{
            $canton = new Canton();
            $messageSuccess = "El cantón ". $request->name . " se ha creado satisfactoriamente.";
        }

        $canton->nombre = $request->name;
        $canton->province_id = $request->listProvince;
        $canton->save();

        return redirect()->route('canton.list')->with(['success'=> $messageSuccess]);
    }

    public function getByProvince(Request $request){
        $canton = Canton::where('province_id', $request->provinciaId)->orderBy('id', 'asc')->get();

        $output = "";
        if($canton->isEmpty()){
            $output .= '<option value=""></option>';
        }else{
            foreach($canton as $row)
            {
                if($request->cantonId == $row->id){
                    $output .= '<option value="'.$row->id.'" selected>'.$row->nombre.'</option>';
                }else{
                    $output .= '<option value="'.$row->id.'">'.$row->nombre.'</option>';
                }
            }
        }

        return $output;
    }

    public function changeState($id){
        $estado_succes = 200;
        $estado_error_general = 400;

        $sector = Canton::find($id);

        if($sector != null){
            if($sector->is_activo == 0){

                $sector->is_activo = 1;
                $estadoRegistro = "Activo";
                $clase = "btn-success";
                $sector->save();

                $answer = array(
                    "estado" => $estado_succes,
                    "data" => array(
                        "mensaje" => config('constant.alert.success.state'),
                        "user_estado" => $estadoRegistro,
                        "user_clase" => $clase,
                    ),
                );
            }
            else{
                $user = Client::where('canton_id', $id)->first();
                $parroquia = Parroquia::where('canton_id', $id)->first();

                if($user == null && $parroquia == null){
                    $sector->is_activo = 0;
                    $estadoRegistro = "Inactivo";
                    $clase = "btn-danger";
                    $sector->save();

                    $answer = array(
                        "estado" => $estado_succes,
                        "data" => array(
                            "mensaje" => config('constant.alert.success.state'),
                            "user_estado" => $estadoRegistro,
                            "user_clase" => $clase,
                        ),
                    );
                }
                else{
                    //No inactivar registro, ya qu está relacionado con otras tablas.
                    $answer = array(
                        "estado" => $estado_error_general,
                        "data" => array(
                            "mensaje" => config('constant.alert.error.change_state_by_join'),
                            "user_estado" => "",
                            "user_clase" => "",
                        ),
                    );
                }
            }
        }else{
            $answer = array(
                "estado" => $estado_error_general,
                "data" => array(
                    "mensaje" => config('constant.alert.error.not_found'),
                    "user_estado" => "",
                    "user_clase" => "",
                ),
            );
        }

        return json_encode($answer);
    }
}
