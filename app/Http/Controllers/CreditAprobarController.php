<?php

namespace App\Http\Controllers;

use App\Client;
use App\Mail\ClientCreditAprobarMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CreditAprobarController extends Controller
{
    //Mostrar clientes que no tengan crédito aprobado
    public function show(){
        $clientes = Client::where([['is_credito_aprobado', 0], ['is_activo', 1]])->get();
        return view('credito-aprobar', ['clientes'=>$clientes]);
    }

    public function aprobar(Request $request){
        $cliente = Client::find( $request->clientId);
        $cliente->is_credito_aprobado = 1;
        $cliente->save();

        Mail::to($cliente->email)->send(new ClientCreditAprobarMailable($cliente));
        return "ok";
    }
}
