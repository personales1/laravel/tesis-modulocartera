<?php

namespace App\Http\Controllers;

use App\Canton;
use App\Client;
use App\Parroquia;
use App\Province;
use Illuminate\Http\Request;

class ProvinciaController extends Controller
{
    public function show(){
        $provincias = Province::all();
        return view('provincia-list', ['provincias'=>$provincias]);
    }

    public function showForm($id){
        $provincias = Province::find($id);

        if($provincias == null){
            return view('error404');
        }

        return view('provincia-new', ['provincias'=>$provincias]);
    }

    public function destroy($id){
        $provincia = Province::find($id);
        $provincia->delete();
        return config('constant.alert.success.delete');
    }

    public function store(Request $request, $id = null){
        $request->validate([
            'name' => 'required',
        ]);

        if(isset($id)){
            $provincia = Province::find($id);
            $messageSuccess = config('constant.alert.success.update');
        }else{
            $provincia = new Province();
            $messageSuccess = "La provincia ". $request->name . " se ha creado satisfactoriamente.";
        }

        $provincia->nombre = $request->name;
        $provincia->save();

        return redirect()->route('provincia.list')->with(['success'=> $messageSuccess]);
    }

    public function changeState($id){
        $estado_succes = 200;
        $estado_error_general = 400;

        $provincia = Province::find($id);

        if($provincia != null){
            if($provincia->is_activo == 0){

                $provincia->is_activo = 1;
                $estadoRegistro = "Activo";
                $clase = "btn-success";
                $provincia->save();

                $answer = array(
                    "estado" => $estado_succes,
                    "data" => array(
                        "mensaje" => config('constant.alert.success.state'),
                        "user_estado" => $estadoRegistro,
                        "user_clase" => $clase,
                    ),
                );
            }
            else{
                $user = Client::where('province_id', $id)->first();
                $canton = Canton::where('province_id', $id)->first();
                $parroquia = Parroquia::where('province_id', $id)->first();

                if($user == null && $canton == null && $parroquia == null){
                    $provincia->is_activo = 0;
                    $estadoRegistro = "Inactivo";
                    $clase = "btn-danger";
                    $provincia->save();

                    $answer = array(
                        "estado" => $estado_succes,
                        "data" => array(
                            "mensaje" => config('constant.alert.success.state'),
                            "user_estado" => $estadoRegistro,
                            "user_clase" => $clase,
                        ),
                    );
                }
                else{
                    //No inactivar registro, ya qu está relacionado con otras tablas.
                    $answer = array(
                        "estado" => $estado_error_general,
                        "data" => array(
                            "mensaje" => config('constant.alert.error.change_state_by_join'),
                            "user_estado" => "",
                            "user_clase" => "",
                        ),
                    );
                }
            }
        }else{
            $answer = array(
                "estado" => $estado_error_general,
                "data" => array(
                    "mensaje" => config('constant.alert.error.not_found'),
                    "user_estado" => "",
                    "user_clase" => "",
                ),
            );
        }

        return json_encode($answer);
    }
}
