<?php

namespace App\Http\Controllers;

use App\CreditDay;
use Illuminate\Http\Request;

class CreditDayController extends Controller
{
    public function show(){
        //$credits = CreditDay::all();
        $credits = CreditDay::where('is_activo', 1)->get();
        return view('credito-dias-list', ['credits'=>$credits]);
    }

    public function showForm($id){
        $credits = CreditDay::find($id);

        if($credits == null){
            return view('error404');
        }

        return view('credito-dias-new', ['credits'=>$credits]);
    }

    public function destroy($id){
        $count = CreditDay::where('is_activo', 1)->get()->count();

        if($count > 1){
            $credit = CreditDay::find($id);
            $credit->is_activo = 0;
            $credit->save();

            $estado = 200;
            $data = array(
                "mensaje" => config('constant.alert.success.delete')
            );
        }
        else{
            $estado = 400;
            $data = array(
                "mensaje" => config('constant.alert.error.delete_records_all')
            );
        }

        $answer = array(
            "estado" => $estado,
            "data" => $data,
        );

        return json_encode($answer);
    }

    public function store(Request $request, $id = null){
        $request->validate([
            'description' => 'required',
            'periodo' => 'required',
        ]);

        if(isset($id)){
            $dia_credito = CreditDay::find($id);
            $messageSuccess = config('constant.alert.success.update');
        }else{
            $dia_credito = new CreditDay;
            $messageSuccess = "El registro de día de crédito se ha creado satisfactoriamente.";
        }

        $dia_credito->descripcion = $request->description;
        $dia_credito->periodo = $request->periodo;
        $dia_credito->save();

        return redirect()->route('credito.dias.list')->with(['success'=> $messageSuccess]);
    }
}
