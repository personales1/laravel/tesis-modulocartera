<?php

namespace App\Http\Controllers;

use App\CreditReason;
use Illuminate\Http\Request;

class CreditReasonController extends Controller
{
    public function show(){
        //$credits = CreditReason::all();
        $credits = CreditReason::where('is_activo', 1)->get();
        return view('credito-motivo-list', ['credits'=>$credits]);
    }

    public function showForm($id){
        $credits = CreditReason::find($id);

        if($credits == null){
            return view('error404');
        }

        return view('credito-motivo-new', ['credits'=>$credits]);
    }

    public function destroy($id){
        $count = CreditReason::where('is_activo', 1)->get()->count();

        if($count > 1){
            $credit = CreditReason::find($id);
            $credit->is_activo = 0;
            $credit->save();

            $estado = 200;
            $data = array(
                "mensaje" => config('constant.alert.success.delete')
            );
        }
        else{
            $estado = 400;
            $data = array(
                "mensaje" => config('constant.alert.error.delete_records_all')
            );
        }

        $answer = array(
            "estado" => $estado,
            "data" => $data,
        );

        return json_encode($answer);
    }

    public function store(Request $request, $id = null){
        $request->validate([
            'description' => 'required',
        ]);

        if(isset($id)){
            $motivo_credito = CreditReason::find($id);
            $messageSuccess = config('constant.alert.success.update');
        }else{
            $motivo_credito = new CreditReason;
            $messageSuccess = "El motivo de crédito ". $request->description . " se ha creado satisfactoriamente.";
        }

        $motivo_credito->descripcion = $request->description;
        $motivo_credito->save();

        return redirect()->route('credito.motivo.list')->with(['success'=> $messageSuccess]);
    }
}
