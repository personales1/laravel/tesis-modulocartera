<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditReason extends Model
{
    use SoftDeletes;

    public function salesHeader(){
        return $this->hasMany(SalesHeader::class);
    }
}
