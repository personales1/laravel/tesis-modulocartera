<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistroPago extends Model
{
    use SoftDeletes;

    public function salesHeader(){
        return $this->belongsTo(SalesHeader::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function estadoPago(){
        return $this->belongsTo(EstadoPago::class);
    }
}
