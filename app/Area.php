<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $primaryKey = 'area_id';
    public $incrementing = false;
    protected $keyType = 'string';
}
