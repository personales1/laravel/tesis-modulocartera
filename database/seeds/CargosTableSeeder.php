<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CargosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cargos')->insert([
            'cargo_id' => 'GERENTE',
            'nombre' => 'Gerente General',
        ]);

        DB::table('cargos')->insert([
            'cargo_id' => 'SUPERVISOR',
            'nombre' => 'Supervisor',
        ]);

        DB::table('cargos')->insert([
            'cargo_id' => 'ASISTENTE',
            'nombre' => 'Asistente',
        ]);

        DB::table('cargos')->insert([
            'cargo_id' => 'CHOFER',
            'nombre' => 'Chofer',
        ]);

        DB::table('cargos')->insert([
            'cargo_id' => 'EJECUTIVO',
            'nombre' => 'Ejecutivo',
        ]);

        DB::table('cargos')->insert([
            'cargo_id' => 'PASANTE',
            'nombre' => 'Pasante',
        ]);

        DB::table('cargos')->insert([
            'cargo_id' => 'Z-OTRO',
            'nombre' => 'Otro',
        ]);
    }
}
