<?php

use Illuminate\Database\Seeder;

class EstadoPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado_pagos')->insert([
            'descripcion' => 'ATRASADO - INCOMPLETO',
            'ponderacion' => 1,
            'ponderacion_texto' => 'MUY MALA',
            'ponderacion_calificacion' => 'D2',
            'ponderacion_color_background' => '#d92550',
            'ponderacion_color_texto' => '#ffffff',
            'ponderacion_clase_css' => 'badge-danger',
        ]);
        DB::table('estado_pagos')->insert([
            'descripcion' => 'ATRASADO',
            'ponderacion' => 2,
            'ponderacion_texto' => 'MALA',
            'ponderacion_calificacion' => 'D1',
            'ponderacion_color_background' => '#d92550',
            'ponderacion_color_texto' => '#ffffff',
            'ponderacion_clase_css' => 'badge-danger',
        ]);
        DB::table('estado_pagos')->insert([
            'descripcion' => 'FUERA DE TIEMPO - INCOMPLETO',
            'ponderacion' => 3,
            'ponderacion_texto' => 'MUY BAJA',
            'ponderacion_calificacion' => 'C2',
            'ponderacion_color_background' => '#f7b924;',
            'ponderacion_color_texto' => '#212529',
            'ponderacion_clase_css' => 'badge-warning',
        ]);
        DB::table('estado_pagos')->insert([
            'descripcion' => 'FUERA DE TIEMPO',
            'ponderacion' => 4,
            'ponderacion_texto' => 'BAJA',
            'ponderacion_calificacion' => 'C1',
            'ponderacion_color_background' => '#f7b924;',
            'ponderacion_color_texto' => '#212529',
            'ponderacion_clase_css' => 'badge-warning',
        ]);
        DB::table('estado_pagos')->insert([
            'descripcion' => 'FECHA DE PAGO - INCOMPLETO',
            'ponderacion' => 5,
            'ponderacion_texto' => 'MEDIA',
            'ponderacion_calificacion' => 'B',
            'ponderacion_color_background' => '#6c757d',
            'ponderacion_color_texto' => '#ffffff',
            'ponderacion_clase_css' => 'badge-secondary',
        ]);
        DB::table('estado_pagos')->insert([
            'descripcion' => 'FECHA DE PAGO',
            'ponderacion' => 6,
            'ponderacion_texto' => 'BUENA',
            'ponderacion_calificacion' => 'A3',
            'ponderacion_color_background' => '#3ac47d',
            'ponderacion_color_texto' => '#ffffff',
            'ponderacion_clase_css' => 'badge-success',
        ]);
        DB::table('estado_pagos')->insert([
            'descripcion' => 'POR ADELANTADO - FECHA CERCANA',
            'ponderacion' => 7,
            'ponderacion_texto' => 'ALTA',
            'ponderacion_calificacion' => 'A2',
            'ponderacion_color_background' => '#16aaff',
            'ponderacion_color_texto' => '#ffffff',
            'ponderacion_clase_css' => 'badge-info',
        ]);
        DB::table('estado_pagos')->insert([
            'descripcion' => 'POR ADELANTADO',
            'ponderacion' => 7,
            'ponderacion_texto' => 'SOBRESALIENTE',
            'ponderacion_calificacion' => 'A1',
            'ponderacion_color_background' => '#3f6ad8',
            'ponderacion_color_texto' => '#ffffff',
            'ponderacion_clase_css' => 'badge-primary',
        ]);
    }
}
