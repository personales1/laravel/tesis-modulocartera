<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            ProvincesTableSeeder::class,
            CantonsTableSeeder::class,
            ParroquiasTableSeeder::class,
            SectorsTableSeeder::class,
            GendersTableSeeder::class,
            CargosTableSeeder::class,
            AreasTableSeeder::class,
            CreditReasonsTableSeeder::class,
            CreditDaysTableSeeder::class,
            EstadoPagoSeeder::class,
            UsersProfileTableSeeder::class,
        ]);
    }
}
