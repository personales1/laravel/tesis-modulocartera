<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CantonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cantons')->insert([
            'nombre' => 'Guayaquil',
            'province_id' => '1',
        ]);
    }
}
