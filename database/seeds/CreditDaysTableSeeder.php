<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreditDaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_days')->insert([
            'descripcion' => '15',
            'periodo' => 'días',
        ]);

        DB::table('credit_days')->insert([
            'descripcion' => '30',
            'periodo' => 'días',
        ]);

        DB::table('credit_days')->insert([
            'descripcion' => '45',
            'periodo' => 'días',
        ]);
    }
}
