<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreditReasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('credit_reasons')->insert([
            'descripcion' => 'Cliente frecuente',
        ]);

        DB::table('credit_reasons')->insert([
            'descripcion' => 'Compra al por mayor',
        ]);
    }
}
