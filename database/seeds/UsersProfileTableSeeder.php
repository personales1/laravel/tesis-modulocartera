<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_profiles')->insert([
            'user_id' => 1,
            'nombres' => 'José Luis',
            'apellidos' => 'Proaño Acosta',
            'identificacion' => '0922529904',
            'telefono' => '0984410809',
        ]);
    }
}
