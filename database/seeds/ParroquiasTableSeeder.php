<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParroquiasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parroquias')->insert([
            'nombre' => 'Ximena',
            'province_id' => '1',
            'canton_id' => '1',
        ]);
    }
}
