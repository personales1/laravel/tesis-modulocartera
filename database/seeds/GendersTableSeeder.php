<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genders')->insert([
            'gender_id' => 'HOMBRE',
            'nombre' => 'Hombre',
        ]);

        DB::table('genders')->insert([
            'gender_id' => 'MUJER',
            'nombre' => 'Mujer',
        ]);
    }
}
