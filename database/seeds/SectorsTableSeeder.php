<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sectors')->insert([
            'nombre' => 'Sur',
        ]);

        DB::table('sectors')->insert([
            'nombre' => 'Norte',
        ]);

        DB::table('sectors')->insert([
            'nombre' => 'Este',
        ]);

        DB::table('sectors')->insert([
            'nombre' => 'Oeste',
        ]);

        DB::table('sectors')->insert([
            'nombre' => 'Centro',
        ]);
    }
}
