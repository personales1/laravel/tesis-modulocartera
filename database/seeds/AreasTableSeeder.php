<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->insert([
            'area_id' => 'SISTEMA',
            'nombre' => 'Sistema',
        ]);

        DB::table('areas')->insert([
            'area_id' => 'LIMPIEZA',
            'nombre' => 'Limpieza',
        ]);

        DB::table('areas')->insert([
            'area_id' => 'MKT',
            'nombre' => 'MARKETING',
        ]);

        DB::table('areas')->insert([
            'area_id' => 'OBRAS',
            'nombre' => 'Obras',
        ]);

        DB::table('areas')->insert([
            'area_id' => 'TTHH',
            'nombre' => 'Talento Humano',
        ]);

        DB::table('areas')->insert([
            'area_id' => 'CONTABILIDAD',
            'nombre' => 'Contabilidad',
        ]);

        DB::table('areas')->insert([
            'area_id' => 'Z-OTRO',
            'nombre' => 'Otro',
        ]);
    }
}
