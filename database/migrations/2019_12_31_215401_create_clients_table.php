<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('identificacion');
            $table->string('fec_nac');
            $table->string('email');
            $table->string('telefono')->default('');
            $table->string('celular');
            $table->string('sexo');
            $table->string('direccion');
            $table->string('direccion_referencia');
            $table->string('direccion_mapa')->default('');
            $table->string('direccion_coordenadas')->default('');
            $table->unsignedBigInteger('province_id');
            $table->unsignedBigInteger('canton_id');
            $table->unsignedBigInteger('parroquia_id');
            $table->unsignedBigInteger('sector_id');
            $table->boolean('is_credito_aprobado')->default(false);
            $table->boolean('is_activo')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('canton_id')->references('id')->on('cantons');
            $table->foreign('parroquia_id')->references('id')->on('parroquias');
            $table->foreign('sector_id')->references('id')->on('sectors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
