<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_works', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id');
            $table->string('empresa');
            $table->string('fec_ing');
            $table->string('cargo_id');
            $table->string('cargo_descripcion');
            $table->string('area_id');
            $table->string('area_descripcion');
            $table->string('telefono')->default('');
            $table->string('extension')->default('');
            $table->string('direccion');
            $table->string('direccion_referencia');
            $table->string('direccion_coordenadas')->default('');
            $table->string('direccion_mapa')->default('');
            $table->string('ingreso_periodo');
            $table->string('ingreso_promedio_mensual');
            $table->string('gastos_promedio_mensual');
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('cargo_id')->references('cargo_id')->on('cargos');
            $table->foreign('area_id')->references('area_id')->on('areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_works');
    }
}
