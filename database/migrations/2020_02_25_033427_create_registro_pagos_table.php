<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistroPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_pagos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sales_header_id');
            $table->unsignedBigInteger('client_id');
            $table->decimal('total',8,2);
            $table->decimal('valor_pago',8,2);
            $table->decimal('saldo',8,2);
            $table->boolean('is_pagado')->default(0);
            $table->integer('credito_dia');
            $table->enum('credito_dia_periodo',['DIAS', 'SEMANAS', 'MESES']);
            $table->timestamp('fecha_realiza_pago')->nullable();
            $table->timestamp('fecha_pago_credito')->nullable();
            $table->unsignedBigInteger('estado_pago_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('sales_header_id')->references('id')->on('sales_headers');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('estado_pago_id')->references('id')->on('estado_pagos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_pagos');
    }
}
