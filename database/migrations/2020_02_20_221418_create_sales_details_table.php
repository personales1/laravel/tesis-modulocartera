<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sales_header_id');
            $table->unsignedBigInteger('product_id');
            $table->string('cantidad');
            $table->decimal('subtotal',8,2);
            $table->decimal('iva',8,2);
            $table->decimal('total',8,2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('sales_header_id')->references('id')->on('sales_headers');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_details');
    }
}
