<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('index');
});

Route::get('/cliente/nuevo', 'ClientController@create')->middleware('auth');
Route::get('/clientes', 'ClientController@show')->middleware('auth')->name('cliente.list');
Route::get('/cliente/{id}', 'ClientController@showForm')->middleware('auth')->name('provincia.form');
Route::post('/cliente/create/{id?}', 'ClientController@store')->middleware('auth')->name('cliente.create');
Route::post('/cliente/estado/{id}', 'ClientController@changeState')->middleware('auth')->name('cliente.change.state');

Route::get('/canton/nuevo', 'CantonController@create')->middleware('auth');
Route::get('/canton', 'CantonController@show')->middleware('auth')->name('canton.list');
Route::get('/canton/{id}', 'CantonController@showForm')->middleware('auth')->name('provincia.form');
Route::post('/canton/create/{id?}', 'CantonController@store')->middleware('auth')->name('canton.create');
Route::post('/canton/find', 'CantonController@getByProvince')->middleware('auth')->name('canton.find.by.province');
Route::delete('/canton/{id}', 'CantonController@destroy')->middleware('auth')->name('canton.destroy');
Route::post('/canton/estado/{id}', 'CantonController@changeState')->middleware('auth')->name('canton.change.state');

Route::get('/credito/aprueba', 'CreditAprobarController@show')->middleware('auth')->name('credito.aprueba');
Route::post('/credito/aprueba/cliente', 'CreditAprobarController@aprobar')->middleware('auth')->name('credito.aprueba.cliente');
Route::get('/credito/otorgar', 'CreditOtorgarController@show')->middleware('auth')->name('credito.otorga');

Route::get('/credito/dias/nuevo', function () {
    return view('credito-dias-new');
})->middleware('auth');
Route::get('/credito/dias', 'CreditDayController@show')->middleware('auth')->name('credito.dias.list');
Route::get('/credito/dias/{id}', 'CreditDayController@showForm')->middleware('auth')->name('credito.dias.form');
Route::post('/credito/dias/create/{id?}', 'CreditDayController@store')->middleware('auth')->name('credito.dias.create');
Route::delete('/credito/dias/{id}', 'CreditDayController@destroy')->middleware('auth')->name('credito.dias.destroy');

Route::get('/credito/motivo/nuevo', function () {
    return view('credito-motivo-new');
})->middleware('auth');
Route::get('/credito/motivo', 'CreditReasonController@show')->middleware('auth')->name('credito.motivo.list');
Route::get('/credito/motivo/{id}', 'CreditReasonController@showForm')->middleware('auth')->name('credito.motivo.form');
Route::post('/credito/motivo/create/{id?}', 'CreditReasonController@store')->middleware('auth')->name('credito.motivo.create');
Route::delete('/credito/motivo/{id}', 'CreditReasonController@destroy')->middleware('auth')->name('credito.motivo.destroy');

Route::get('/parroquia/nuevo', 'ParroquiaController@create')->middleware('auth');
Route::get('/parroquia', 'ParroquiaController@show')->middleware('auth')->name('parroquia.list');
Route::get('/parroquia/{id}', 'ParroquiaController@showForm')->middleware('auth')->name('parroquia.form');
Route::post('/parroquia/create/{id?}', 'ParroquiaController@store')->middleware('auth')->name('parroquia.create');
Route::post('/parroquia/find', 'ParroquiaController@getByCanton')->middleware('auth')->name('parroquia.find.by.canton');
Route::delete('/parroquia/{id}', 'ParroquiaController@destroy')->middleware('auth')->name('parroquia.destroy');
Route::post('/parroquia/estado/{id}', 'ParroquiaController@changeState')->middleware('auth')->name('parroquia.change.state');

Route::get('/usuario/nuevo', 'UserController@create')->middleware('auth')->name('usuario.new');
Route::get('/usuario', 'UserController@show')->middleware('auth')->name('usuario.list');
Route::get('/usuario/{id}', 'UserController@showForm')->middleware('auth')->name('usuario.form');
Route::get('/usuario/perfil/datos', 'UserController@showProfile')->middleware('auth')->name('usuario.profile');
Route::get('/usuario/perfil/password/nuevo', function () {
    return view('user-change-password');
})->middleware('auth')->name('usuario.change.password');
Route::post('/usuario/change/password', 'UserController@changePassword')->middleware('auth')->name('usuario-change-password');
Route::post('/usuario/create/{id?}', 'UserController@store')->middleware('auth')->name('usuario.create');
Route::delete('/usuario/{id}', 'UserController@destroy')->middleware('auth')->name('usuario.destroy');
Route::post('/usuario/{id}', 'UserController@changeState')->middleware('auth')->name('usuario.change.state');
Route::post('/usuario/password/update', 'UserController@updatePassword')->middleware('auth')->name('usuario.update.password');

Route::get('/producto/nuevo', function () {
    return view('producto-new');
})->middleware('auth');
Route::get('/producto', 'ProductController@show')->middleware('auth')->name('producto.list');
Route::get('/producto/{id}', 'ProductController@showForm')->middleware('auth')->name('producto.form');
Route::post('/producto/create/{id?}', 'ProductController@store')->middleware('auth')->name('producto.create');
Route::delete('/producto/{id}', 'ProductController@destroy')->middleware('auth')->name('producto.destroy');
Route::post('/producto/estado/{id}', 'ProductController@changeState')->middleware('auth')->name('producto.change.state');

Route::get('/provincia/nuevo', function () {
    return view('provincia-new');
})->middleware('auth');
Route::get('/provincia', 'ProvinciaController@show')->middleware('auth')->name('provincia.list');
Route::get('/provincia/{id}', 'ProvinciaController@showForm')->middleware('auth')->name('provincia.form');
Route::post('/provincia/create/{id?}', 'ProvinciaController@store')->middleware('auth')->name('provincia.create');
Route::delete('/provincia/{id}', 'ProvinciaController@destroy')->middleware('auth')->name('provincia.destroy');
Route::post('/provincia/estado/{id}', 'ProvinciaController@changeState')->middleware('auth')->name('provincia.change.state');

Route::get('/sector/nuevo', function () {
    return view('sector-new');
})->middleware('auth');
Route::get('/sector', 'SectorController@show')->middleware('auth')->name('sector.list');
Route::get('/sector/{id}', 'SectorController@showForm')->middleware('auth')->name('sector.form');
Route::post('/sector/create/{id?}', 'SectorController@store')->middleware('auth')->name('sector.create');
Route::delete('/sector/{id}', 'SectorController@destroy')->middleware('auth')->name('sector.destroy');
Route::post('/sector/estado/{id}', 'SectorController@changeState')->middleware('auth')->name('sector.change.state');

Route::post('/ventas/header', 'VentaController@generaVentaHeader')->middleware('auth')->name('venta.header');
Route::post('/ventas/detail', 'VentaController@generaVentaDetail')->middleware('auth')->name('venta.detail');
Route::post('/ventas/pago', 'VentaController@generaPrimerRegistroPago')->middleware('auth')->name('venta.pago');
Route::post('/ventas/registro/pago', 'VentaController@registrarPago')->middleware('auth')->name('venta.registro.pago');
Route::get('/ventas/cobros', 'VentaController@showPagosPendientes')->middleware('auth')->name('venta.cobro.pendiente');
Route::get('/ventas/detail/cliente/{id}', 'VentaController@getDetalleVentasPorCliente')->middleware('auth')->name('venta.detail.cliente');
Route::get('/ventas/pago/pendiente/cliente/{id}', 'VentaController@verificaPagoPendientePorCliente')->middleware('auth')->name('venta.pago.pendiente.cliente');

Route::get('/reporte/historial/crediticio', 'ReporteController@showHistorialCrediticio')->middleware('auth')->name('reporte.historial.crediticio');
Route::get('/reporte/ventas', 'ReporteController@showVentas')->middleware('auth')->name('reporte.ventas');

Route::get('/export/excel', 'ExportReportExcel@printHistorialCrediticio')->middleware('auth')->name('export.excel.historial.crediticio');
Route::get('/export/pdf', 'ExportReportPdf@printHistorialCrediticio')->middleware('auth')->name('export.pdf.historial.crediticio');
Route::get('/export/excel/clientes', 'ExportReportExcel@printCliente')->middleware('auth')->name('export.excel.clientes');
Route::get('/export/pdf/clientes', 'ExportReportPdf@printCliente')->middleware('auth')->name('export.pdf.clientes');
Route::get('/export/excel/provincia', 'ExportReportExcel@printProvincia')->middleware('auth')->name('export.excel.provincia');
Route::get('/export/pdf/provincia', 'ExportReportPdf@printProvincia')->middleware('auth')->name('export.pdf.provincia');
Route::get('/export/excel/canton', 'ExportReportExcel@printCanton')->middleware('auth')->name('export.excel.canton');
Route::get('/export/pdf/canton', 'ExportReportPdf@printCanton')->middleware('auth')->name('export.pdf.canton');
Route::get('/export/excel/parroquia', 'ExportReportExcel@printParroquia')->middleware('auth')->name('export.excel.parroquia');
Route::get('/export/pdf/parroquia', 'ExportReportPdf@printParroquia')->middleware('auth')->name('export.pdf.parroquia');
Route::get('/export/excel/sector', 'ExportReportExcel@printSector')->middleware('auth')->name('export.excel.sector');
Route::get('/export/pdf/sector', 'ExportReportPdf@printSector')->middleware('auth')->name('export.pdf.sector');
Route::get('/export/excel/motivo/credito', 'ExportReportExcel@printCreditoMotivo')->middleware('auth')->name('export.excel.motivo.credito');
Route::get('/export/pdf/motivo/credito', 'ExportReportPdf@printCreditoMotivo')->middleware('auth')->name('export.pdf.motivo.credito');
Route::get('/export/excel/motivo/dias', 'ExportReportExcel@printCreditoDias')->middleware('auth')->name('export.excel.motivo.dias');
Route::get('/export/pdf/motivo/dias', 'ExportReportPdf@printCreditoDias')->middleware('auth')->name('export.pdf.motivo.dias');
Route::get('/export/excel/productos', 'ExportReportExcel@printProductos')->middleware('auth')->name('export.excel.productos');
Route::get('/export/pdf/productos', 'ExportReportPdf@printProductos')->middleware('auth')->name('export.pdf.productos');
Route::get('/export/excel/usuarios', 'ExportReportExcel@printUsuarios')->middleware('auth')->name('export.excel.usuarios');
Route::get('/export/pdf/usuarios', 'ExportReportPdf@printUsuarios')->middleware('auth')->name('export.pdf.usuarios');
Route::get('/export/excel/reporte/ventas', 'ExportReportExcel@printReportesVentas')->middleware('auth')->name('export.excel.reporte.venta');
Route::get('/export/pdf/reporte/ventas', 'ExportReportPdf@printReportesVentas')->middleware('auth')->name('export.pdf.reporte.venta');

Route::post('/tests', 'ReporteController@test')->middleware('auth')->name('test');
Route::get('/reporte/grafico/calidad-pagos', 'ReporteController@graficoCalidadCrediticiaPagos')->middleware('auth')->name('grafico');
Route::get('/reporte/grafico/ventas-pagos', 'ReporteController@graficoVentasPago')->middleware('auth')->name('grafico.ventas.pago');
Route::post('/reporte/grafico/calidad-pagos/por-fecha', 'ReporteController@graficoCalidadCrediticiaPagosPorFecha')->middleware('auth')->name('grafico.calidad.crediticia.porfecha');
Route::post('/reporte/grafico/ventas-pagos/por-fecha', 'ReporteController@graficoVentasPagoPorFecha')->middleware('auth')->name('grafico.ventas.pagos.porfecha');


//Route::get('/email', 'DemoEmailController@send')->middleware('auth')->name('email.send.demo');
/*
Route::get('/email', function () {
    $invoice = App\User::find(1);
    return new App\Mail\EmailNotification($invoice);
});
*/
